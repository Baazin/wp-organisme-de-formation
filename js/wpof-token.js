jQuery(document).ready(function($)
{
    $(".token-entity").click(function(e)
    {
        e.preventDefault();
        
        session_id = $(".id.session").attr('data-id');
        object_class = $(this).attr('data-objectclass');
        id = $(this).attr('data-id');
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'new_token',
                'session_id': session_id,
                'object_class': object_class,
                'id': id,
            },
            function(response)
            {
                show_message(response);
            }
        );
    });

    $(".token-reset").click(function(e)
    {
        e.preventDefault();
        
        jQuery.post
        ({
            url: ajaxurl,
            data: {
                'action': 'reset_token_with_email',
                'email': $(this).attr('data-email'),
                'session_id': $(".id.session").attr('data-id'),
            },
            success: function(response)
            {
                console.log("succes");
                show_message(response);
            },
            error: function()
            {
                console.log("erreur");
            }
        });
    });
    
    $(".token-delete").click(function(e)
    {
        e.preventDefault();
        bouton = $(this);
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'delete_token',
                'id': $(this).attr('data-id'),
                'object': $(this).attr('data-class'),
                'session_id': $(".id.session").attr('data-id'),
            },
            function(response)
            {
                show_message(response);
                bouton.closest(bouton.attr('data-remove')).remove();
            }
        );
    });
});
