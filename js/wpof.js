jQuery(document).ready(function($)
{

$('.openButton').click(toggle_bloc_hidden);

// Fonction pour ouvrir ou fermer un bloc depuis un bouton radio ou checkbox
// Le bouton doit avoir pour data-id l'id du bloc à gérer
// Pas forcément utile de perdre du temps là-dessus...
$('.openCheck').change(function(e)
{
    id = "#"+$(this).attr("data-id");
    $(id).toggleClass('blocHidden');
});


$('#show-log').click(function(e)
{
    $('#opaga-log').show(200);
});
$('#opaga-message .close').click(close_opaga_message);
function close_opaga_message()
{
    message_box = $(this).parent()
    message_box.hide(400);
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'show_message_box',
        },
        function (response)
        {
            contenu = $(response);
            contenu.on('click', '.close', close_opaga_message);
            message_box.replaceWith(contenu);
        }
    );
}
$('#opaga-log .close').click(function() {$(this).parent().hide(400); });

$('.choix-interactif').click(function()
{
    $(this).children("li").each(function()
    {
        if ($(this).children("label").children("input").is(':checked'))
            $(this).children("label").children("span").show();
        else
            $(this).children("label").children("span").hide();
    });
});

// Gestion du menu du tableau de bord de session
$('.wpof-menu ul .onglet').click(switch_onglet);
function switch_onglet(e)
{
    id = "#" + $(this).attr("data-id");
    famille = $(this).parent().parent().attr("data-famille");
    $('.famille-' + famille + ' .tableau').addClass('blocHidden');
    $('.famille-' + famille + ' ul .onglet').removeClass('highlightButton');
    if (false === $(this).hasClass("fermer-tableau"))
    {
        $(id).removeClass('blocHidden');
        $(this).addClass('highlightButton');
    }
}

$("#main-tabs").tabs({active: $("input[name='default_main_tab']").val()});
$("#options-tabs").tabs({active: $("input[name='default_options_tab']").val()});
$("#user-tabs").tabs({active: $("input[name='default_user_tab']").val()});
var tabs_client = $("#tabs-clients").tabs();
tabs_client.addClass( "ui-tabs-vertical ui-helper-clearfix" );
$("#tabs-clients li").removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
tabs_client.find(".ui-tabs-nav").sortable
({
    axis: "y",
    stop: function()
    {
        list = $(this).children();
        if (list.first().hasClass("tab-stagiaire"))
            $(this).sortable("cancel");
        else
        {
            const list_id = [];
            last_client = null;
            last_client_id = 0;
            cpt = 0;
            list.each(function()
            {
                if ($(this).hasClass("tab-client"))
                    list_id.push({type: "client", id: $(this).attr('data-id')});
                else
                    list_id.push({type: "stagiaire", id: $(this).attr('data-id')});
            });
            jQuery.post
            (
                ajaxurl,
                {
                    'action': 'sort_clients_stagiaires',
                    'session_id': $(".id.session").attr('data-id'),
                    'list_id': list_id,
                },
                function(response)
                {
                    console.log(response);
                }
            )
        }
        tabs_client.tabs( "refresh" );
        console.log($(this));
    },
    cancel: "li.tab-client",
    cursor: "move",
    dropOnEmpty: false,
    connectWith: ".tab-client",
});
$("#profile-box-menu").menu();


$("#main-tabs > ul > li").click(switch_default_tab);
$("#options-tabs > ul > li").click(switch_default_tab);
$("#user-tabs > ul > li").click(switch_default_tab);
function switch_default_tab(e)
{
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'set_default_tab',
            'tab_id': $(this).attr('aria-labelledby').replace('ui-id-', ''),
            'tab_name': $(this).closest('.ui-tabs').attr('id'),
        },
    );

    if ($(this).parent().attr('data-reload') != undefined)
        document.location.reload();
}


/*
$('.wpof-menu ul li.fermer-tableau').click(function(e)
{
    id = $(this).attr("data-id");
    famille = $(this).parent().parent().attr("data-famille");
    $('.tableau' + id).addClass('blocHidden');
    $('.wpof-menu ul li.bouton').removeClass('highlightButton');
});
*/


// Champs relatifs à la TVA dans les options générales
$("input.tva").change(function(e)
{
    e.preventDefault();
    if ($("input[name='wpof_of_hastva']").prop("checked"))
    {
        $("input[name='wpof_of_exotva']").prop("disabled", false);
        $("input[name='wpof_of_tauxtva']").prop("disabled", false);
    }
    else
    {
        $("input[name='wpof_of_tauxtva']").prop("disabled", true);
        $("input[name='wpof_of_exotva']").prop("disabled", true);
    }
});

// 
$('input:checkbox.has-employeur').change(function (e)
{
    id = "#" + $(this).attr("data-id");
    $('table' + id + ' .employeur-oui').toggleClass('blocHidden');
    $('table' + id + ' .employeur-non').toggleClass('blocHidden');
});


$('textarea.autoselect').click(function (e)
{
    $(this).select();
});

/*
$('input.tarif_total_chiffre').click(function (e)
{
    id = $(this).attr("data-id");
    if ($(this).val() == "")
        $(this).val($('#nb-jour' + id).val() * $('#tarif-jour' + id).val());
});
*/

// désinscrire une personne d'une session de formation
// fonction attachée au bouton X dans le menu du tableau de bord
$(".unsubscribe").click(unsubscribe_stagiaire);
function unsubscribe_stagiaire(e)
{
    e.preventDefault();
    session = $(this).attr("data-sessionid");
    user = $(this).attr("data-userid");
    
    $("#user" + user).remove();
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'unsubscribe',
            'user_id': user,
            'session_id': session,
        },
        function(response)
        {
            //console.log(response);
        }
    );
}

$(".sql_select").click(function(e)
{
    e.preventDefault();
    session_id = $(this).attr("data-sessionid");
    stagiaire_id = $(this).attr("data-stagiaireid");
    
    sql_info = $("#pilote_dialog");
    
    if (sql_info.dialog( "instance" ))
        sql_info.dialog("destroy");

    sql_info.dialog
    ({
        autoOpen: true,
        title: "Infos sur la session " + session_id,
        height: 600,
        width: 1200,
        modal: false,
        close: function()
        {
            sql_info.dialog("destroy");
        }
    });
    
    jQuery.post
    (
        ajaxurl,
        {
            'action' : 'sql_session_formation',
            'session_id' : session_id,
            'stagiaire_id' : stagiaire_id,
        },
        function (response)
        {
            sql_info.html(response);
        }
    );
});

$(".button-add-media").click(function(e)
{
    e.preventDefault();
    
    bouton_enregistrer = $(this).closest('.metadata').find('.bouton');
    value_id = $(this).attr("data-valueid");
    link_media = $(this).attr("data-linkmedia");
    img_media = $(this).attr("data-image");
    
    if (!$(this).hasClass("inactif"))
    {
        var uploader = wp.media
        ({
            title: "Envoyer un fichier",
            button: { text: "Téléverser" },
            multiple: false,
        })
        .on('select', function()
        {
            var selection = uploader.state().get('selection');
            var attachment = selection.first().toJSON();
            //console.log(attachment);
            $("#" + link_media).attr("href", attachment.link);
            $("#" + link_media).text(attachment.title);
            $("#" + value_id).val(attachment.id);
            if (img_media != undefined)
                $("#" + img_media).attr("src", attachment.url);
            bouton_enregistrer.addClass('enregistrement-requis');
        })
        .open();
    }
});

// Permuter d'utilisateur
$(".switch-user").click(function(e)
{
    e.preventDefault();
    user = $(this).attr("data-userid");
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'switchuser',
            'user_id': user,
        },
        function(response)
        {
            param = JSON.parse(response);
            if (param.url != undefined)
                document.location.assign(param.url);
        }
    );
});


// Bouton pour ajouter une ligne d'ajout de nouveau stagiaire
var compte_num = 1;
$(".new-compte span#plus").click(function(e)
{
    var markup = "<tr id='" + compte_num +"_new-compte' data-num='" + compte_num +"' class='new-compte new-compte-plus'>";
    if ($(this).attr('data-genre') == 1)
        markup += "<td><select name='" + compte_num +"_genre'><option value='Madame'>Madame</option><option value='Monsieur'>Monsieur</option></select></td>";
    markup += "<td><input type='text' name='" + compte_num +"_firstname' placeholder='Prénom' /></td>";
    markup += "<td><input type='text' name='" + compte_num +"_lastname' placeholder='Nom' /></td>";
    markup += "<td><input type='text' name='" + compte_num +"_email' placeholder='Courriel' /></td>";
    select_role = $(this).attr("data-selectrole");
    if (select_role !== undefined)
    {
        markup += "<td><select name='" + compte_num + "_role'>";
        role_list = JSON.parse(select_role);
        $.each(role_list, function(index, value)
        {
            markup += "<option value='" + index + "'>" + value + "</option>";
        });
        markup += "</select></td>";
    }
    markup += "</tr>";
    $("table.add-compte tbody").append(markup);
    compte_num ++;
});

// ajouter des stagiaires (création de compte + éventuellement inscription à la session session_id)
$("#add-compte-bouton").click(function(e)
{
    bouton = $(this);
    parent_id = "#" + $(this).parent().attr("id");
    var allusers = new Array();
    $(parent_id + " .new-compte").each(function()
    {
        id = "#" + $(this).attr("id");
        num = $(this).attr("data-num");
        genre = $(id + " :input[name$='_genre']").val();
        firstname = $(id + " :input[name$='_firstname']").val();
        lastname = $(id + " :input[name$='_lastname']").val();
        email = $(id + " :input[name$='_email']").val();
        role = $(id + " :input[name$='_role']").val();
        allusers[num] = { "genre" : genre, "firstname" : firstname, "lastname" : lastname, "email" : email, "role" : role };
    });
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'add_compte',
            dataType: 'json',
            contentType: 'application/json',
            'data': JSON.stringify({ 'allusers' : allusers }),
        },
        function(response)
        {
            param = JSON.parse(response);
            $(parent_id + " .message").html(param.message);
            bouton.removeClass('enregistrement-requis');
        }
    );
});


$('.stagiaire-submit').click(function(e)
{
    e.preventDefault();
    
    bouton = $(this);
    message_bloc = $(this).closest('.ui-tabs-panel').children('.message');
    formData = new FormData($(this).closest("form")[0]);
    formData.set('action', 'enregistrer_stagiaire_form');
    jQuery.post
    ({
        url: ajaxurl,
        data: formData,
        processData: false,
        contentType: false,
        success: function (response)
        {
            param = JSON.parse(response);
            bouton.removeClass('enregistrement-requis');
            message_bloc.html(param.message);
        }
    });
    
});

// enregistrer les champs user_meta qui sont dans des input
$(".metadata .enregistrer-user-input").click(function(e)
{
    e.preventDefault();
    bouton = $(this);
    
    user_id = $(this).attr("data-userid");
    parent = "#" + $(this).parent().attr("id");
    var fields = new Object();
    $(parent + " :input").each(function()
    {
        inputid = $(this).attr("name");
        if (editor = tinyMCE.get(inputid))
            fields[inputid] = editor.getContent();
        else
            fields[inputid] = $(this).val();
    });
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'enregistrer_user_input',
            'user_id': user_id,
            'fields': fields,
        },
        function(response)
        {
            $(parent + " .message").html(response);
            bouton.removeClass('enregistrement-requis');
        }
    );
});

// enregistrer les champs user_meta qui sont dans des input
$(".metadata .enregistrer-formation-input").click(function(e)
{
    e.preventDefault();
    bouton = $(this);
    
    entite = "";
    
    if ($(".id.session").attr("data-id") != undefined)
    {
        id = $(".id.session").attr("data-id");
        entite = "SessionFormation";
        console.log($(".id.session"));
    }
    else if ($(".id.formation").attr("data-id") != undefined)
    {
        id = $(".id.formation").attr("data-id");
        entite = "Formation";
    }
    if (entite == "")
    {
        console.log("Entité inconnue : ni une session, ni une formation");
        return;
    }
    
    parent = $(this).closest(".metadata.notif-modif");
    
    parent.find(".modif").each(function()
    {
        var fieldname = $(this).find("h3").text();
        input = $(this).find('.wp-editor-area');
        inputid = input.attr("name");
        if (editor = tinyMCE.get(inputid))
            value = editor.getContent();
        else
            value = input.val();
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'update_jpost_value',
                'session_id': id,
                'formation_id': id,
                'object_class': entite,
                'meta': inputid,
                'value': value,
                'nodeName': 'editor',
            },
            function(response)
            {
                param = JSON.parse(response);
                if (param.erreur !== undefined)
                    append_message("<p class='erreur'>" + param.erreur + "</p>");
                if (param.succes !== undefined)
                    append_message("<p class='succes'>" + fieldname + " mis à jour</p>");
            }
        );
    });
    
    show_message("");
    bouton.removeClass('enregistrement-requis');
    $(".input_jpost.modif").removeClass('modif');
    console.log(log);
});


// enregistrer les images uploadées par le responsable de formation
$("form#uploadcrimgform .enregistrer-resp-img").click(function(e)
{
    parent = "#" + $(this).parent().attr("id");
    
    user_id = $(this).attr("data-userid");
    signature = $("form#uploadcrimgform #signature_img").prop('files')[0];
    //console.log(signature);
    formData = new FormData();
    formData.append('file', signature);
    formData.append('action', 'enregistrer_resp_img');
    formData.append('user_id', user_id);
    //console.log(formData);

    jQuery.post
    ({
        url: ajaxurl,
        data: formData,
        contentType: false,
        processData: false,
        success: function(response)
        {
            //console.log(response);
            $(parent + " .message").html(response);
        }
    });
});

// enregistrer les champs post_meta qui sont dans des input
$(".metadata .enregistrer-session-input").click(function(e)
{
    session_id = $(this).attr("data-sessionid");
    action = $(this).attr("data-action");
    parent = "#" + $(this).parent().attr("id");
    var fields = new Object();
    $(parent + " :input").each(function()
    {
        inputid = $(this).attr("name");
        type = $(this).attr("type");
        if (type == "radio" && !$(this).prop("checked"))
            return;
        fields[inputid] = $(this).val();
    });
    //console.log(fields);
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': action,
            'session_id': session_id,
            'fields': fields,
        },
        function(response)
        {
            $(parent + " .message").html(response);
        }
    );
});


$(".radio-choice input[type='text']").change(radio_choice_one_param);
$(".radio-choice input[type='radio']").change(radio_choice_one_param);
function radio_choice_one_param(e)
{
    e.preventDefault();
    meta_value = $(this).val();
    meta_key = $(this).attr("name");
    session_id = $(this).closest(".radio-choice").attr("data-sessionid");
    var bouton = $(this).closest(".radio-choice").parent().children(".openButton");
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'update_unique_value',
            'session_id': session_id,
            'meta_key': meta_key,
            'meta_value': meta_value,
        },
        function(response)
        {
            if (meta_key.match('_complement') != null)
            {
                base = bouton.text().split(" – ")[0];
                bouton.text(base + " – " + response);
            }
            else
                bouton.text(response);
            bouton.removeClass("erreur");
        }
    );
}

$(".metadata select").change(session_select_change);
function session_select_change(e)
{
    e.preventDefault();
    meta_value = $(this).val();
    meta_key = $(this).attr("name");
    session_id = $(this).closest("fieldset").attr("data-sessionid");
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'update_unique_value',
            'session_id': session_id,
            'meta_key': meta_key,
            'meta_value': meta_value,
        },
        function(response)
        {
        }
    );
}

$("#update-budget-total").click(update_budget_total);
function update_budget_total()
{
    if ($("#budget_total").length)
    {
        var budget_total = 0;
        $(".tarif_total_chiffre").each(function()
        {
            if ("" != $(this).val())
                budget_total += parseFloat($(this).val());
        });
        $("#budget_total").html(budget_total);
    }
    $('#update-budget-total').hide();
}

// enregistrer le changement de tarif pour un stagiaire et mettre à jour les valeurs qui en dépendent
$(".metadata input[name='tarif_heure_presentiel']").change(change_tarif_stagiaire);
$(".metadata input[name='tarif_total_chiffre']").change(change_tarif_stagiaire);
//$(".metadata input[name='tarif_heure_presentiel']").blur(change_tarif_stagiaire);
//$(".metadata input[name='tarif_total_chiffre']").blur(change_tarif_stagiaire);
//$(".metadata input[name='tarif_heure_presentiel']").mouseout(change_tarif_stagiaire);
function change_tarif_stagiaire(e)
{
    e.preventDefault();
    var fieldset = $(this).closest("fieldset");
    
    var formData = new FormData();

    formData.set('session_id', fieldset.attr("data-sessionid"));
    formData.set('action', 'enregistrer_session_tarif');
                 
    if ($(this).attr('name') == 'tarif_heure_presentiel')
    {
        formData.set('tarif_heure', $(this).val());
        formData.set('tarif_base_total', 0);
    }
    else
    {
        formData.set('tarif_total_chiffre', $(this).val());
        formData.set('tarif_base_total', 1);
    }
    
    formData.set('stagiaire_id', fieldset.attr("data-stagiaireid"));
    formData.set('session_id', fieldset.attr("data-sessionid")); // TODO : à finir !!! TODO

    jQuery.post
    ({
        url: ajaxurl,
        data: formData,
        processData: false,
        contentType: false,
        success: function(response)
        {
            param = JSON.parse(response);
            //fieldset.children(".message").html(param.message);
            fieldset.find("input[name='tarif_total_chiffre']").val(param.tarif_total_chiffre);
            fieldset.find("input[name='tarif_heure_presentiel']").val(param.tarif_heure);
            fieldset.find(".tarif_total_lettre").text(param.tarif_total_lettre);
            $('#update-budget-total').show();
        }
    });
}

// enregistrer le changement de tarif pour un groupe et mettre à jour les valeurs qui en dépendent
$(".metadata input[name='tarif_heure_intra']").change(change_tarif_groupe);
$(".metadata input[name='tarif_total_chiffre_intra']").change(change_tarif_groupe);
$(".metadata input[name='tarif_heure_intra']").blur(change_tarif_groupe);
$(".metadata input[name='tarif_total_chiffre_intra']").blur(change_tarif_groupe);
function change_tarif_groupe(e)
{
    e.preventDefault();
    parent = $(this).parent();
    var fieldset = parent.parent();
    var fieldset_id = "#" + fieldset.attr("id");
    
    var formData = new FormData();

    formData.set('session_id', fieldset.attr("data-sessionid"));
    formData.set('action', 'enregistrer_session_tarif');
                 
    if ($(this).attr('name') == 'tarif_heure_intra')
    {
        formData.set('tarif_heure', $(this).val());
        formData.set('tarif_base_total', false);
    }
    else
    {
        formData.set('tarif_total_chiffre', $(this).val());
        formData.set('tarif_base_total', true);
    }
    
    jQuery.post
    ({
        url: ajaxurl,
        data: formData,
        processData: false,
        contentType: false,
        success: function(response)
        {
            $(fieldset_id + " .message").html("");
            param = JSON.parse(response);
            console.log(param);
            if (undefined !== param.erreur)
                $(fieldset_id + " .message").html(param.erreur);
                
            $(fieldset_id + " .tarif_total_chiffre").val(param.tarif_total_chiffre);
            $(fieldset_id + " .tarif_heure").val(param.tarif_heure);
            $(fieldset_id + " .tarif_total_lettre").text(param.tarif_total_lettre);
        }
    });
}
$(".metadata input[name='tarif_total_autres_chiffre']").change(change_tarif_autres);
//$(".metadata input[name='tarif_total_autres_chiffre']").mouseout(change_tarif_autres);
$(".metadata input[name='autres_frais']").change(change_tarif_autres);
//$(".metadata input[name='autres_frais']").mouseout(change_tarif_autres);
function change_tarif_autres(e)
{
    e.preventDefault();
    parent = $(this).parent();
    var fieldset = parent.parent();
    var fieldset_id = "#" + fieldset.attr("id");
    session_id = fieldset.attr("data-sessionid");
    
    autres_frais = $(fieldset_id + " input[name='autres_frais']").val();
    tarif_autres = $(fieldset_id + " input[name='tarif_total_autres_chiffre']").val();
    
    if (tarif_autres == 0)
        $(".tarif-autres").hide();
    else
        $(".tarif-autres").show();
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'enregistrer_session_tarif',
            'session_id': session_id,
            'tarif_total_autres_chiffre': tarif_autres,
            'autres_frais': autres_frais,
        },
        function(response)
        {
            $(fieldset_id + " .message").html("");
            param = JSON.parse(response);
            if (undefined !== param.erreur)
                $(fieldset_id + " .message").html(param.erreur);
                
            $(fieldset_id + " .tarif_total_autres_lettre").text(param.tarif_total_autres_lettre);
        }
    );
    
}

$(".notif-modif input").change(notification_modif);
$(".notif-modif textarea").change(notification_modif);
$(".notif-modif select").change(notification_modif);
function notification_modif(e)
{
    e.preventDefault();
    $(this).closest(".notif-modif").find(".submit").addClass("enregistrement-requis");
}

$(".enregistrement-requis").mouseup(function(e) { $(this).removeClass("enregistrement-requis"); });

// activer ou désactiver les types de feuilles d'émargement
$("#choix-type-emargement .bouton").click(function()
{
    parent = "#" + $(this).parent().attr("id");
    emarge_type = $(this).attr("data-type");
    session_id = $(this).attr("data-sessionid");
    value = $(this).attr("data-value");
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'change_type_emargement',
            'session_id': session_id,
            'emarge_type': emarge_type,
            'value' : value,
        },
        function(response)
        {
            $(parent + " .message").html(response);
        }
    );
    
    $(this).toggleClass("fait");
    $(this).attr("data-value", 1 - value); // inversion de la valeur : soit 0, soit 1
});

$(".unique-value").change(function()
{
    var fields = new Object();
    fields["action"] = "update_unique_value";
    if (undefined !== $(this).attr("data-sessionid"))
        fields["session_id"] = $(this).attr("data-sessionid");
    if (undefined !== $(this).attr("data-stagiaireid"))
        fields["stagiaire_id"] = $(this).attr("data-stagiaireid");
    fields["meta_key"] = $(this).attr("name");
    fields["meta_value"] = $(this).val();
    
    // special checkbox non cochée
    if ($(this).is(":checkbox") && !$(this).is(":checked"))
        fields["meta_value"] = "";
    
    parent = "#" + $(this).parent().attr("id");
    
    jQuery.post
    (
        ajaxurl,
        fields,
        function(response)
        {
            $(parent + " .message").html(response);
            $(parent).addClass("succes");
   //         console.log(response);
        }
    );
});

// Créneaux

// Activer ou désactiver un créneau pour un client
$("div.creneau").click(switch_creneau);
function switch_creneau(e)
{
    creno_id = $(this).attr("data-id");
    session_id = $(this).closest(".tableau-creneau").attr("data-sessionid");
    objet_id = $(this).closest(".tableau-creneau").attr("data-objetid");
    objet = $(this).closest(".tableau-creneau").attr("data-objet");
    
    if (undefined === objet_id) return;
    
    var creno = $(this);
    
    // actif vaut 0 si this a la classe actif (ça veut dire que ce clic le désactive) et 1 dans le cas inverse
    actif = ($(this).hasClass("actif")) ? 0 : 1;
    
    jQuery.post
    (
        ajaxurl,
        {
            'action' : 'active_creneau',
            'creno_id' : creno_id,
            'session_id' : session_id,
            'objet_id' : objet_id,
            'objet' : objet,
            'actif' : actif,
        },
        function(response)
        {
            //console.log(response);
            creno.toggleClass("actif");
            
            // Mise à jour de pour_infos_box
            params = { 'object_class': objet, 'session_id': session_id, 'client_id': objet_id, 'user_id': objet_id };
            postprocess_func['update_pour_infos_client'](params);
        }
    );
}

    var datepicker_options =
    {
        dateFormat: 'dd/mm/yy',
        onClose: datepicker_close,
        numberOfMonths: 3,
    };
    var datepicker_base =
    {
        dateFormat: 'dd/mm/yy',
        numberOfMonths: 1,
    };
    var datepicker_trois_mois =
    {
        dateFormat: 'dd/mm/yy',
        numberOfMonths: 3,
    };
    var datepicker_signature_options = { dateFormat: 'd MM yy' };
    
    $('.liste-creneau .datepicker').datepicker(datepicker_options);
    $('input.input_jpost_value.datepicker').datepicker(datepicker_base);
    $('input[name="decale_date"]').datepicker(datepicker_trois_mois);
    $('.datepicker.input_jpost_value').datepicker(datepicker_signature_options);
    
    /* Plage de date */
    $('.choix_plage_date .datepicker').datepicker(datepicker_trois_mois);
    $('.choix_plage_date input.debut').change(function() { $(this).closest('.choix_plage_date').find('.fin').datepicker("option", "minDate", $(this).val()); });
    $('.choix_plage_date input.fin').change(function() { $(this).closest('.choix_plage_date').find('.debut').datepicker("option", "maxDate", $(this).val()); });
    
    $('input[name="decale_date"]').change(function(e)
    {
        date = $(this).val();
        
        if (date.match(/\d\d\/\d\d\/\d\d\d\d/))
            $(this).parent().children(".valider_decale_date").show();
        else
            $(this).parent().children(".valider_decale_date").hide();
    });
    
    $('.valider_decale_date').click(function(e)
    {
        session_id = $(".id.session").attr('data-id');
        new_date = $(this).parent().children('input[name="decale_date"]').val();
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'session_decale_date',
                'session_id': session_id,
                'new_date': new_date,
            },
            function(response)
            {
                console.log(response);
                document.location.reload(true);
            }
        );
    });
    
    $(".add-date").click(add_new_date);
    function add_new_date(e)
    {
        e.preventDefault();
        parent = $(this).parent(); // TODO : à virer
        tableau = $(this).closest(".tableau-creneau");
        session_id = tableau.attr("data-sessionid");
        
        var fields = new Object();
        decaljour = $(this).attr("data-decaljour");
        if (undefined != decaljour)
        {
            // calcul du décalage en millisecondes
            decaljourMS = 1000 * 60 * 60 * 24 * decaljour;
            
            // récupération du bloc date + liste créneaux sous forme d'un objet jquery'
            dateBaseLine = $(this).closest(".liste-creneau");

            // récupération date actuelle
            dateBaseText = dateBaseLine.find("input.datepicker").val();
            // calcul de la nouvelle date
            dateArray = dateBaseText.split('/');
            newDateObj = new Date(dateArray[2] + '-' + dateArray[1] + '-' + dateArray[0]);
            newDateObj.setTime(newDateObj.getTime() + decaljourMS);
            newDateText = newDateObj.toLocaleDateString('fr-FR');
            
            // vérification si il existe déjà une ligne avec cette date-là
            double_found = false;
            tableau.find(".datepicker").each(function()
            {
                if ($(this).val() == newDateText)
                {
                    double_found = true;
                    $(this).parent().addClass("bg-alerte").delay(700).queue(function(){ $(this).removeClass("bg-alerte").dequeue(); });
                }
            });
            
            // si la nouvelle date n'existe pas encore, on peut la créer'
            if (!double_found)
            {
                crenos = Array();
                dateBaseLine.find('.creneau').each(function() { crenos.push($(this).attr('data-id')); });
                params = 
                {
                    'session_id': $(".id.session").attr('data-id'),
                    'action': 'update_date',
                    'date': newDateText,
                    'creneaux': crenos,
                };
                jQuery.post
                ({
                    url: ajaxurl,
                    data: params,
                    success: function(response)
                    {
                        param = JSON.parse(response);
                        if (param.log != undefined)
                            console.log(param.log);
                        
                        if (param.html != undefined)
                        {
                            newDateLine = $(param.html);
                            postprocess_func['edit_date_add_events'](newDateLine);
                            // si decaljour vaut 1 on ajoute la nouvelle ligne sous la ligne copiée
                            // sinon, on l'ajoute à la fin. TODO : faire un vrai tri des dates'
                            if (decaljour == 1)
                                dateBaseLine.after(newDateLine);
                            else
                                $(".empty-date").before(newDateLine);
                            
                            // on fait clignoter en couleur succes
                            newDateLine.addClass("bg-succes").delay(700).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
                        }
                        //$(document).trigger('cpt.ready');
                    },
                });
            }
            
        }
        else
        {
            bouton = $(".empty-date");
            jQuery.post
            ({
                url: ajaxurl,
                data: { 'action': 'add_new_date_line', 'session_id': session_id, 'fields': fields },
                success: function(response)
                {
                    line = $(response);
                    postprocess_func['edit_date_add_events'](line);
                    bouton.before(line);
                    datepick = line.find(".datepicker");
                    datepick.trigger('focus');
                },
            });
        }
    }
    
    // Enregistrement effectif d'une date
    function datepicker_close(date, datepicker)
    {
        date_line = $(this).closest('.liste-creneau');
        old_date = date_line.attr('data-date');
//        old_date = date_line.find("input[name$='_date']").val();
        
        if (date.match(/\d\d\/\d\d\/\d\d\d\d/))
        {
            date_line.children('.bouton').show();
            if (date != old_date)
            {
                params = 
                {
                    'session_id': $(".id.session").attr('data-id'),
                    'action': 'update_date',
                    'date': date,
                    'old_date': old_date,
                };
                jQuery.post
                (
                    ajaxurl,
                    params,
                    function (response)
                    {
                        param = JSON.parse(response);
                        date_line.attr("data-date", date);
                        date_line.children('.icone-bouton').show();
                        
                        args = Object();
                        args.session_id = $(".id.session").attr('data-id');
                        postprocess_func['update_session_titre'](args);
                        jQuery.post
                        (
                            ajaxurl,
                            {
                                'action': 'get_week_day',
                                'date': date,
                            },
                            function(response)
                            {
                                date_line.find(".week_day").replaceWith($(response));
                            }
                        )
                    },
                );
            }
        }
        else
        {
            if (date != old_date)
            {
                date_line.find('.del-date').trigger('click');
            }
            date_line.remove();
        }
    }
    
    // Suppression d'une date
    $("span.del-date").click(del_date);
    function del_date()
    {
        date_line = $(this).closest('.liste-creneau');
        params =
        {
            'session_id': $(".id.session").attr('data-id'),
            'action': 'del_date_creneau',
            'date': date_line.find('.datepicker').val(),
        };
        
        jQuery.post
        (
            ajaxurl,
            params,
            function (response)
            {
                param = JSON.parse(response);
                //console.log(param.log);
                date_line.remove();
                postprocess_func['tabs_reload'](Array());
                
                // mise à jour pour infos session
                /*
                arg = Object();
                arg.session_id = $(".id.session").attr('data-id');
                postprocess_func['update_pour_infos_session'](arg);
                */
            },
        );
    }
    
    // Suppression effective d'un créneau
    $("span.del-creneau").click(del_creneau);
    function del_creneau()
    {
        creno_div = $(this).closest(".creneau");
        date_line = $(this).closest('.liste-creneau');
        params =
        {
            'session_id': $(".id.session").attr('data-id'),
            'action': 'del_date_creneau',
            'date': date_line.find('.datepicker').val(),
            'creneau': creno_div.attr('data-id'),
        };
        
        jQuery.post
        (
            ajaxurl,
            params,
            function (response)
            {
                param = JSON.parse(response);
                //console.log(param.log);
                creno_div.remove();
                postprocess_func['tabs_reload'](Array());
                // mise à jour pour infos session
                /*
                arg = Object();
                arg.session_id = $(".id.session").attr('data-id');
                postprocess_func['update_pour_infos_session'](arg);
                */
            },
        );
    }
    

$("input.input_jpost_value").focus(function(e)
{
    $(this).removeClass('bord-succes');
});

$(".input_jpost input").change(change_jpost_value);
$(".input_jpost select").change(change_jpost_value);
$(".select_jpost select").change(change_jpost_value);
$(".input_jpost textarea").change(change_jpost_value);

$('.lieu.details input[name="lieu_ville"]').change(function(e)
{
    args = Object();
    args.session_id = $(".id.session").attr('data-id');
    postprocess_func['update_session_titre'](args);
});

$('.icone-bouton.set-acompte').click(function(e)
{
    e.preventDefault();
    
    tarif_total = $(this).closest('.board-client').find('input[name="tarif_total_chiffre"]').val();
    acompte_input = $(this).closest('.input_jpost').find('input[name="acompte"]')
    acompte_input.val((tarif_total * $(this).attr('data-acompte') / 100));
    acompte_input.trigger('change');
});

$('.icone-bouton.move-client').click(function(e)
{
    e.preventDefault();
    data =
    {
        'client_id': $(this).attr('data-clientid'),
        'session_source_id': $(this).attr('data-sessionid'),
        'action': 'copie_ou_deplace_client',
    };
    
    jQuery.post
    ({
        url: ajaxurl,
        data: data,
        success: function(response)
        {
            console.log(response);
        }
    });
});

$("input.input_check_jpost_value").change(change_check_jpost_value);
function change_check_jpost_value(e)
{
    e.preventDefault();
    input = $(this);
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'update_user_value',
            'checked': $(this).is(":checked"),
            'user_id': $(this).parent().children("input[name='user_id']").val(),
            'meta': $(this).attr('name'),
        },
        function (response)
        {
            console.log(response);
        },
    );
}

$("select.select_jpost_formation_value").change(change_jpost_value_formation);
function change_jpost_value_formation(e)
{
    e.preventDefault();
    input = $(this);
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'update_formation_value',
            'formation_id': $(this).parent().children("input[name='formation_id']").val(),
            'meta': $(this).attr('name'),
            'value': $(this).val(),
        },
        function (response)
        {
            console.log(response);
            input.addClass("bg-succes").delay(700).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
        },
    );
}

$(".dynamic-dialog").click(dynamic_dialog);

$('.aide_toggle_edit').click(aide_toggle_edit);
$('.aide_reset').click(aide_reset);

// Supprimer un texte d'aide dans le JSON de référence
$(".delete.aide").click(function(e)
{
    slug = $(this).attr('data-slug');
    parent = $(this).closest('div.aide.objet');
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'delete_aide',
            'slug': slug,
        },
        function(response)
        {
            if (response != undefined)
            {
                parent.remove();
                console.log(response);
            }
            else
                console.log("Rien ne s'est passé");
        }
    );
});

dialog_function = 
{
    'toggle_client_prenom': function(e)
        {
            e.preventDefault();

            if ($(".type_client:checked").val() == 'part')
            {
                $(".new_client.dialog div.prenom").show();
                $(".new_client.dialog span.raison_sociale").hide();
            }
            else
            {
                $(".new_client.dialog div.prenom").hide();
                $(".new_client.dialog span.raison_sociale").show();
            }
        },
};

$('.programme-session').click(function(e)
{
    e.preventDefault();
    bouton = $(this);
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'add_new_session',
            'formation': bouton.attr('data-id'),
            'session_unique_titre': '',
            'formateur': bouton.attr('data-formateur'),
        },
        function(response)
        {
            param = JSON.parse(response);
            
            if (param.url != undefined)
                document.location.assign(param.url);
        }
    );
});

$(".delete-entity").click(delete_entity);
function delete_entity(e)
{
    e.preventDefault();
    
    id = $(this).attr('data-id');
    session_id = $(this).attr('data-sessionid');
    object_class = $(this).attr('data-objectclass')
    parent_to_kill = $(this).attr('data-parent');
    
    yesno = $("<div>La suppression est irréversible !</div>");
    yesno.dialog(
    {
        autoOpen: true,
        title: "Êtes-vous sûr ?",
        height: 200,
        width: 350,
        modal: true,
        buttons:
        {
            "Oui": function()
            {
                jQuery.post
                ({
                    url: ajaxurl,
                    data: { 'action': 'delete_entity', 'object_class': object_class, 'id': id, 'session_id': session_id },
                    success: function (response)
                    {
                        param = JSON.parse(response);
                        
                        if (param.succes != undefined)
                            $(parent_to_kill).remove();
                    },
                });
                yesno.dialog("destroy");
            },
            "Non": function() { supprim = false; yesno.dialog("destroy");},
        },
        close: function()
        {
            supprim = false; yesno.dialog("destroy");
        }
    });
    yesno.dialog("open");
}


/* Quiz fonctions */
$(".quiz-sortable").sortable({ stop: change_quiz_order, });
$(".quiz-sortable").disableSelection();

$(".quiz-sortable li input[type='text']").change(quiz_change_value);
function quiz_change_value(e)
{
    li = $(this).closest("li");
    if (li.attr('data-new') != undefined)
        li.removeAttr('data-new');
    $(this).closest(".quiz").find(".quiz-enregistrer").addClass("enregistrement-requis");
}

function change_quiz_order(e, ui)
{
    $(this).closest(".quiz").find(".quiz-enregistrer").addClass("enregistrement-requis");
}

$(".quiz-enregistrer").click(function (e)
{
    bouton = $(this);
    parent_id = $(".id").attr('data-id');
    quiz_id = $(this).closest('.quiz').attr('data-id');
    sujet = $(this).closest('.quiz').attr('data-sujet');
    questions = Array();
    sortable = $(this).closest('.quiz').find("ul.quiz-sortable");
    sortable.children().each(function()
    {
        q = Object();
        q.type = $(this).attr('data-type');
        q.text = $(this).find("input[type='text']").val();
        questions.push(q);
    });
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'quiz_enregistrer',
            'quiz_id': quiz_id,
            'sujet': sujet,
            'parent_id': parent_id,
            'questions': questions,
        },
        function (response)
        {
            param = JSON.parse(response);
            console.log(param.log);
            bouton.removeClass("enregistrement-requis");
            bouton.closest('.quiz').attr('data-id', param.quiz_id);
        }
    );
});

// Ajouter une ligne
$(".quiz-ajouter").click(function (e)
{
    e.preventDefault();
    
    type = $(this).attr('data-type');
    list = $(this).closest('.quiz').find("ul.quiz-sortable");
    quiz_id = $(this).closest('.quiz').attr('data-id');
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'quiz_add_line',
            'type': type,
            'quiz_id': quiz_id,
        },
        function(response)
        {
            param = JSON.parse(response);
            if (param.log != undefined)
                console.log(param.log);
            if (param.html != undefined)
            {
                list.append(param.html);
                list.on('click', '.quiz-supprimer', quiz_supprimer);
                list.on('change', 'input[type="text"]', quiz_change_value);
            }
        },
    );
});

// Supprimer un ligne
$(".quiz-supprimer").click(quiz_supprimer);
function quiz_supprimer(e)
{
    e.preventDefault();
    
    li = $(this).closest("li");
    if (li.attr('data-new') == undefined)
        $(this).closest(".quiz").find(".quiz-enregistrer").addClass("enregistrement-requis");
    li.remove();
}

$("div.exe_comptable input").change(change_exe_comptable);
function change_exe_comptable(e)
{
    e.preventDefault();
    infosession = $(this).closest(".infosession");
    div_exe_comptable = $(this).parent();
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'update_exe_comptable',
            'session_id': infosession.attr("data-sessionid"),
            'stagiaire_id': infosession.attr("data-stagiaireid"),
            'annee': $(this).attr('name'),
            'tarif': $(this).val(),
        },
        function(response)
        {
            param = JSON.parse(response);
            infosession.find(".message").html(param.message);
            new_div = $(param.inputs);
            new_div.children('input').on('change', '', change_exe_comptable);
            div_exe_comptable.replaceWith(new_div);
        }
    );
}

$("select[name='annee_choix']").change(change_annee_comptable);
function change_annee_comptable(e)
{
    e.preventDefault();
    var fields = new Object();
    fields['annee_comptable'] = $(this).val();
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'enregistrer_user_input',
            'fields': fields,
            'user_id': $(this).attr('data-userid'),
        },
        function(response)
        {
            document.location.reload();
        }
    );
}

// (Dés)Active le mode fullscreen d'un bloc
$(".fullscreen-mode").click(fullscreen_mode);
function fullscreen_mode(e)
{
    bloc = $($(this).attr('data-id'));
    if (bloc.hasClass("fullscreen"))
    {
        bloc.removeClass("fullscreen");
        $(this).removeClass("fait");
        $("#wpadminbar").removeClass("blocHidden");
        $("footer").removeClass("blocHidden");
        bloc.appendTo($("div.entry-content"));
        tmp.remove();
    }
    else
    {
        bloc.addClass("fullscreen");
        $(this).addClass("fait");
        $("#wpadminbar").addClass("blocHidden");
        $("footer").addClass("blocHidden");
        tmp = $("<div id='tmp'></div>");
        bloc.appendTo(tmp);
        tmp.prependTo($('body.pilote'));
        //bloc.html("");
    }
}

// (Dés)Active le mode édition d'un bloc
$(".edition-mode").click(edition_mode);
function edition_mode(e)
{
    bloc = $($(this).attr('data-id'));
    if (bloc.hasClass("edit-data"))
    {
        bloc.removeClass("edit-data");
        $(this).removeClass("fait");
    }
    else
    {
        bloc.addClass("edit-data");
        $(this).addClass("fait");
    }
}

// (Dés)Active le mode édition d'un bloc
$(".filtre .toggle").click(toggle_filtre);
function toggle_filtre(e)
{
    e.preventDefault();
    
    target = $($(this).attr('data-target'));
    toggle_element = target.find($($(this).attr('data-toggle')));
    if ($(this).hasClass('fait'))
    {
        toggle_element.addClass('blocHidden');
        $(this).removeClass('fait');
    }
    else
    {
        toggle_element.removeClass('blocHidden');
        $(this).addClass('fait');
    }
}

$('#profile-box').mouseover(function()
{
    $(this).css('z-index', 20);
});

$('#profile-box').mouseout(function()
{
    $(this).css('z-index', '');
});

/* Fonctions pour le pilote de sessions */
$('.toggle-stagiaires').click(function(e)
{
    $('#' + $(this).attr('data-id')).toggle();
});

$('.affiche_verif_dialog').click(function(e)
{
    e.preventDefault();
    
    verif_dailog = $('.verif_dialog');
    verif_dailog.find('textarea[name="message"]').text(verif_dailog.find('textarea[name="message"]').text().replace(/(<([^>]+)>)/gi, ""));
    verif_dailog.dialog
    ({
        autoOpen: true,
        title: "Rapports d'erreurs",
        height: 600,
        width: 1200,
        modal: false,
        buttons:
        {
            "Fermer": function()
            {
                verif_dailog.dialog("destroy");
            }
        },
        close: function()
        {
            verif_dailog.dialog("destroy");
        }
    });
});

$(".voir_rapport").click(function(e)
{
    e.preventDefault();
    
    if ($(this).attr('data-id') == "all")
        $(".rapport_csv").toggle();
    else
        $("#" + $(this).attr('data-id')).toggle();
});

$(".get_rapport").click(function(e)
{
    e.preventDefault();
    
    force_download($("#csv_global").text(), "rapport_global.csv", "text/csv");
});

$(".envoi_rapport").click(function(e)
{
    e.preventDefault();
    
    parent = $(this).closest('.verif_dialog');
    bouton = $(this);
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'envoi_rapport',
            'sujet': parent.find('input[name="sujet"]').val(),
            'expediteur': parent.find('select[name="expediteur"]').val(),
            'message': parent.find('textarea[name="message"]').val(),
            'csv': parent.find(".rapport_csv").html(),
            'destinataire': bouton.attr('data-dest'),
        },
        function(response)
        {
            $("#message" + bouton.attr('data-dest')).html(response);
        }
    );
});

// Plage de dates
$(".choix_plage_date .appliquer-plage").click(function(e)
{
    e.preventDefault();
    
    parent = $(this).closest(".choix_plage_date");
    plage = {'date_debut': parent.find("input[name='date_debut']").val(), 'date_fin': parent.find("input[name='date_fin']").val() };
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': $(this).attr('data-action'),
            'plage': plage,
        },
        function(response)
        {
            param = JSON.parse(response);
            $("#" + param.div_id).replaceWith(param.html);
        }
    );

});

postprocess_func =
{
    'edit_creneau_add_events': function(o)
        {
            o.off('click', '.dynamic-dialog', dynamic_dialog).on('click', '.dynamic-dialog', dynamic_dialog);
            o.off('click', '.del-creneau', del_creneau).on('click', '.del-creneau', del_creneau);
            postprocess_func['tabs_reload'](Array());
        },
    'edit_date_add_events': function(o)
        {
            o.on('click', '.del-date', del_date);
            o.on('focus', '.datepicker', function() { jQuery(this).datepicker(datepicker_options); });
            o.on('click', '.add-date', add_new_date);
            //o.find(".add-date").hide();
            postprocess_func['edit_creneau_add_events'](o);
        },
    'update_client_nom': function(params)
        {
            if (params.client_id == undefined) return;
            jQuery('a[href="#tab-c' + params.client_id + '"]').html(params.value);
        },
    'update_pour_infos_session': function(params)
        {
            params.object_class = 'SessionFormation';
            postprocess_func['update_pour_infos'](params);
        },
    'update_pour_infos_client': function(params)
        {
            params.object_class = 'Client';
            postprocess_func['update_pour_infos'](params);
        },
    'update_pour_infos_stagiaire': function(params)
        {
            params.object_class = 'SessionStagiaire';
            postprocess_func['update_pour_infos'](params);
        },
    'update_pour_infos': function(params)
        {
            if (params == null) return;
            params.action = 'update_pour_infos';
            
            jQuery.post
            (
                ajaxurl,
                params,
                function (response)
                {
                    param = JSON.parse(response);
                    if (param.html != undefined)
                        jQuery(param.parent).find('.pour-infos').html(param.html);
                    else
                        console.log(param.erreur);
                },
            );
        },
    'update_session_titre': function(params)
        {
            if (params.session_id != undefined)
            {
                params.action = 'update_session_titre';
                jQuery.post
                (
                    ajaxurl,
                    params,
                    function (response)
                    {
                        param = JSON.parse(response);
                        if (param.titre != undefined)
                            jQuery("h1.entry-title a").text(param.titre);
                        if (param.log != undefined)
                            console.log(param.log);
                    }
                );
            }
        },
    'toggle_lieu_details': function(params)
        {
            if (params.meta_value != undefined)
            {
                params.action = 'update_lieu_details';
                params.session_id = jQuery(".id.session").attr('data-id');
                
                jQuery.post
                (
                    ajaxurl,
                    params,
                    function (response)
                    {
                        param = JSON.parse(response);
                        if (param.html != undefined)
                            jQuery("." + params.meta_key + ".details").replaceWith(param.html);
                        if (param.log != undefined)
                            console.log(param.log);
                    },
                );
            }
        },
    'toggle_opco': function(params)
        {
            if (params.meta_key == "financement")
            {
                div = jQuery(".client-" + params.client_id + " .input_jpost.toggle-opco");
                if (["mutu8","mutu7"].indexOf(params.meta_value) != -1)
                    div.show();
                else
                    div.hide();
            }
        },
    'tabs_reload': function(params)
        {
            jQuery(".ui-tabs ul").attr('data-reload', 1);
        },
    'manage_random_stagiaire': function(params)
        {
            console.log(params);
            if (params.client_id != undefined)
            {
                params.action = 'manage_random_stagiaire';
                jQuery.post
                (
                    ajaxurl,
                    params,
                    function (response)
                    {
                        param = JSON.parse(response);
                        if (param.log != undefined)
                            console.log(param.log);
                        if (param.html != undefined)
                        {
                            liste_stagiaires = jQuery(param.html);
                            liste_stagiaires.on('change', '.input_jpost input', change_jpost_value);
                            liste_stagiaires.on('change', '.input_jpost select', change_jpost_value);
                            jQuery('tr#c' + params.client_id + ' td').html(liste_stagiaires);
                        }
                    }
                );
            }
        },
    'page-reload': function(params)
        {
            document.location.reload();
        },
    'update_data': function(params)
        {
            console.log(params);
            if (params.object_class != undefined)
            {
                params.action = 'update_data';
                params.meta_key = params.ppkey;
                params.meta_value = params.ppvalue;
                
                jQuery.post
                (
                    ajaxurl,
                    params,
                    function (response)
                    {
                        param = JSON.parse(response);
                        if (param.log != undefined)
                            console.log(param.log);
                        $(params.ppdest).html(params.ppvalue);
                    }
                )
            }
        },
    'update_tarif': function(params)
        {
            if (params.ppmodif != undefined)
            {
                params.action = 'formation_update_tarif',
                
                jQuery.post
                (
                    ajaxurl,
                    params,
                    function (response)
                    {
                        param = JSON.parse(response);
                        to_update = JSON.parse(param.update_form);
                        for (field in to_update)
                        {
                            $("input[name='" + field + "']").val(to_update[field]);
                            $("input[name='" + field + "']").closest('td').addClass("bg-succes").delay(1400).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
                        }
                    },
                );
            }
        },
    'update_keyword_default_text': function(params)
        {
            if (params.has('key') && params.has('contenu'))
            {
                input = $('input[name="' + params.get('key') + '"]');
                input.val(params.get('contenu'));
                td_parent = input.closest('td.defaut');
                td_parent.find('.dynamic-dialog').attr('data-contenu', params.get('contenu'));
                td_parent.find('.previsu').html(params.get('contenu').substring(0, 150) + " …");
                td_parent.find('.previsu').html(td_parent.find('.previsu').text());
            }
        },
    'check_my_formation': function(params)
        {
            params.action = 'check_my_formation';
            jQuery.post
            (
                ajaxurl,
                params,
                function(response)
                {
                    param = JSON.parse(response);
                    if (param.my_formation == 0)
                        show_message(param.message);
                }
            );
        }
};

});

// Fin du jQuery(document).ready


// Fonction utilisée pour les wp_editor. Liée par callback dans les paramètres de wp_editor
function init_opaga_editor(editor)
{
    var $ = jQuery;
    editor.on("change", function()
    {
        $("#wp-" + this.id + "-wrap").closest(".metadata").find(".bouton").addClass("enregistrement-requis");
        $("#wp-" + this.id + "-wrap").closest(".input_jpost.editor").addClass("modif");
    });
}

// Fonction pour forcer un téléchargement
function force_download(data, filename, mimetype)
{
    let link = document.createElement('a');
    link.download = filename;
    let blob = new Blob([data], {type: mimetype});
    link.href = URL.createObjectURL(blob);
    link.click();
    URL.revokeObjectURL(link.href);
    link.remove();
}

function append_message(html)
{
    jQuery("#opaga-message").append(html);
}

function show_message(response)
{
    param = new Object();
    try
    {  
        param = JSON.parse(response);
    }
    catch (e)
    {  
        if (response instanceof Object)
            param = response;
        else
            jQuery("#opaga-message").append("<div class='message'>" + response + "</div>");
    }
    if (param.erreur !== undefined && param.erreur != "")
        jQuery("#opaga-message").append("<div class='erreur message'>" + param.erreur + "</div>");
    if (param.succes !== undefined && param.succes != "")
        jQuery("#opaga-message").append("<div class='succes message'>" + param.succes + "</div>");
    
    if (jQuery("#opaga-message .message").text().length > 0)
        jQuery("#opaga-message").show(400);
}

function change_jpost_value(e)
{
    e.preventDefault();
    
    input = jQuery(this);
    global_parent = input.closest("tr");
    local_parent = input.closest(".input_jpost");
    nodeName = input.prop("nodeName").toLowerCase();
    object_class = local_parent.find("input[name='object_class']").val();
    session_id = local_parent.find("input[name='session_id']").val();
    formation_id = local_parent.find("input[name='formation_id']").val();
    type = input.attr('type');
    contexte = local_parent.find("input[name='contexte']").val();
    contexte_id = local_parent.find("input[name='contexte_id']").val();
    type_doc = local_parent.find("input[name='type_doc']").val();
    client_id = local_parent.find("input[name='client_id']").val();
    stagiaire_id = local_parent.find("input[name='stagiaire_id']").val();
    valid_icon = local_parent.find(".valid");
    display_valeur = local_parent.find(".valeur");
    postprocess = local_parent.find("input[name='postprocess']").val();
    postprocess_args = Object();
    local_parent.find("input.pparg").each(function()
    {
        postprocess_args[jQuery(this).attr('name')] = jQuery(this).val();
    });
    
    value = input.val();
    
    // special checkbox
    if (type == "checkbox")
    {
        if (input.is(":checked"))
            value = 1;
        else
            value = 0;
    }
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'update_jpost_value',
            'object_class': object_class,
            'session_id': session_id,
            'formation_id': formation_id,
            'client_id': client_id,
            'stagiaire_id': stagiaire_id,
            'type': type,
            'contexte': contexte,
            'contexte_id': contexte_id,
            'type_doc': type_doc,
            'meta': input.attr('name'),
            'value': value,
            'postprocess': postprocess,
            'nodeName': nodeName,
        },
        function(response)
        {
            param = JSON.parse(response);
            valid_icon.show().delay(1000).fadeOut(1000);
            display_valeur.text(param.valeur);
            
            if (postprocess != undefined)
            {
                if (param.postprocess_args != undefined)
                    jQuery.extend(postprocess_args, param.postprocess_args);
                
                postprocess_args.meta_value = input.val();
                postprocess_args.meta_key = input.attr('name');
                
                // parcours de tableau sauce Javascript plutôt que jQuery
                postprocess.split('+').forEach(function(item)
                {
                    postprocess_func[item](postprocess_args);
                });
            }
            
            if (global_parent)
            {
                if (stagiaire_id > 0)
                {
                    session_tr = jQuery("#session" + session_id);
                    jQuery.each(param.session_formation, function(key, value)
                    {
                        session_tr.find("." + key).html(value);
                        session_tr.find("." + key + " div ." + nodeName + "_jpost_value").on('change', '', change_jpost_value);
                        //session_tr.find("." + key + " div input[type!='hidden']").addClass('bord-succes');
                        session_tr.find("." + key).addClass("bg-succes").delay(700).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
                    });
                    jQuery.each(param.session_stagiaire, function(key, value)
                    {
                        global_parent.find("." + key).html(value);
                        global_parent.find("." + key + " div ." + nodeName + "_jpost_value").on('change', '', change_jpost_value);
                        //global_parent.find("." + key + " div input[type!='hidden']").addClass('bord-succes');
                        global_parent.find("." + key).addClass("bg-succes").delay(700).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
                    });
                }
                else
                {
                    jQuery.each(param.session_formation, function(key, value)
                    {
                        global_parent.find("." + key).html(value);
                        // global_parent.find("." + key + " div input[type!='hidden']").addClass('bord-succes');
                        global_parent.find("." + key).addClass("bg-succes").delay(700).queue(function(){ $(this).removeClass("bg-succes").dequeue(); });
                        global_parent.find("." + key + " div ." + nodeName + "_jpost_value").on('change', '', change_jpost_value);
                    });
                }
            }
            
        },
    );
}


function dynamic_dialog(e)
{
    e.preventDefault();
    bouton = jQuery(this);
    
    session_id = bouton.attr('data-sessionid');
    if (session_id == undefined)
        session_id = jQuery(".id.session").attr("data-id");

    fonction = bouton.attr('data-function');
    aide_slug = bouton.attr('data-help');
    
    const [node] = bouton;
    const attrs = {};
    jQuery.each(node.attributes, function(index, attribute)
    {
        if (attribute.name.substring(0, 4) == "data")
            attrs[attribute.name.substring(5)] = attribute.value;
    });
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'the_dynamic_dialog',
            'function': fonction,
            'session_id': session_id,
            'formation_id': bouton.attr('data-formationid'),
            'client_id': bouton.attr('data-clientid'),
            'stagiaire_id': bouton.attr('data-stagiaireid'),
            'creno_id': bouton.attr('data-crenoid'),
            'aide_slug': aide_slug,
            'date': bouton.closest('.liste-creneau').attr('data-date'),
            'data': attrs,
        },
        function(response)
        {
            dialog_box = jQuery(response);
            dialog_box.dialog
            ({
                autoOpen: true,
                minWidth: 400,
                width: 'auto',
                modal: true,
                buttons:
                {
                    "Valider" : {
                        text: "Valider",
                        class: "valider",
                        click: function()
                        {
                            dialog_box = jQuery(this);
                            form = dialog_box.find("form")[0];
                            formData = new FormData(form);
                            dialog_box.find('select[multiple="multiple"]').each(function()
                            {
                                name = jQuery(this).attr('name');
                                values = Array();
                                jQuery(this).children("option:selected").each(function()
                                {
                                    values.push(jQuery(this).val());
                                });
                                formData.set(name, values);
                            });
                            dialog_box.find("textarea.wp-editor-area").each(function()
                            {
                                name = jQuery(this).attr('id');
                                formData.set(name, tinyMCE.get(name).getContent());
                            });
                                        
                            if (formData.has('action'))
                            {
                                jQuery.post
                                ({
                                    url: ajaxurl,
                                    data: formData,
                                    processData: false,
                                    contentType: false,
                                    success: function (response)
                                    {
                                        param = JSON.parse(response);
                                        
                                        if (param.url != undefined)
                                            document.location.assign(param.url);
                                        
                                        if (param.erreur != undefined)
                                            dialog_box.children(".message").html("<span class='erreur'>" + param.erreur + "</span>");
                                        else if (param.succes != undefined)
                                            dialog_box.children(".message").html("<span class='succes'>" + param.succes + "</span>");
                                        
                                        if (param.valid_once !== undefined && param.erreur === undefined)
                                        {
                                            var buttons = dialog_box.dialog("option", "buttons");
                                            delete buttons.Valider;
                                            dialog_box.dialog("option", "buttons", buttons);
                                        }
                                        
                                        if (param.html != undefined && param.html != "")
                                        {
                                            bloc_html = jQuery(param.html);
                                            if (param.eventsfunc != undefined)
                                                postprocess_func[param.eventsfunc](bloc_html);
                                                
                                            if (param.prependto != undefined)
                                            {
                                                prependto = bouton.closest(param.prependto);
                                                bloc_html.prependTo(jQuery(prependto));
                                            }

                                            if (param.appendto != undefined)
                                            {
                                                appendto = bouton.closest(param.appendto);
                                                bloc_html.appendTo(jQuery(appendto));
                                            }
                                            
                                            if (param.replace != undefined)
                                            {
                                                replace = bouton.closest(param.replace);
                                                jQuery(replace).html(bloc_html);
                                            }
                                            
                                            if (param.replacewith != undefined)
                                            {
                                                replace = bouton.closest(param.replacewith);
                                                jQuery(replace).replaceWith(bloc_html);
                                            }
                                        }
                                    },
                                    error: function (response)
                                    {
                                        console.log(response);
                                        console.log("Erreur sur action " + formData.get('action'));
                                    },
                                });
                            }
                            if (formData.has('eventsfunc'))
                            {
                                postprocess_func[formData.get('eventsfunc')](formData);
                            }
                            if (formData.has('close_on_valid'))
                            {
                                dialog_box.dialog("close");
                                if (formData.has('reload_on_close'))
                                    document.location.reload();
                            }
                            else if (formData.has('refresh_on_success'))
                            {
                                dialog_box.find("form input[type!='hidden']").val('');
                            }
                        },
                    },
                    "Fermer" : {
                        text: "Fermer",
                        click: function()
                        {
                            dialog_box.dialog("close");
                        }
                    },
                },
                close:function()
                {
                    formData = new FormData(dialog_box.find("form")[0]);
                    dialog_box.find("textarea.wp-editor-area").each(function()
                    {
                        tinyMCE.execCommand("mceRemoveEditor", false, jQuery(this).attr('id'));
                    });
                    dialog_box.dialog("destroy");
                    if (formData != undefined && formData.has('reload_on_close'))
                        document.location.reload(true);
                },
            });
            
            // pour permettre d'ouvrir une autre fenêtre de dialogue depuis celle-ci
            dialog_box.on('click', '.dynamic-dialog', dynamic_dialog);
            
            // spécial pour les dialogues d'aide en ligne
            if (dialog_box.find('textarea.wp-editor-area') != undefined)
            {
                dialog_box.on('click', '.aide_toggle_edit', aide_toggle_edit);
                dialog_box.on('click', '.aide_reset', aide_reset);
                dialog_box.ready(function()
                {
                    dialog_box.find('textarea.wp-editor-area').each(function()
                    {
                        tinyMCE.execCommand("mceAddEditor", false, jQuery(this).attr('id'));
                    });
                });
            }
            
            // action spéciales, événements à rajouter
            dialog_box.find("input[name='callback']").each(function()
            {
                console.log(jQuery(this).val());
                dialog_box.on(jQuery(this).attr('data-event'), jQuery(this).attr('data-support'), dialog_function[jQuery(this).val()]);
            });
        }
    );
}

function aide_toggle_edit(e)
{
    e.preventDefault();
    
    parent = jQuery(this).closest("form");
    
    parent.find(".aide_edit").toggle();
    parent.find(".aide_show").toggle();
    
    parent.find(".aide_show h2").text(parent.find("input[name='titre']").val());
    parent.find(".aide_show div").html(tinyMCE.get("texte").getContent());
}

function aide_reset(e)
{
    e.preventDefault();
    
    parent = jQuery(this).closest("form");
    
    jQuery.post
    (
        ajaxurl,
        {
            'action': 'get_aide_defaut',
            'slug': parent.find("input[name='slug']").val(),
        },
        function (response)
        {
            param = JSON.parse(response);
            parent.find(".aide_show h2").text(param.titre);
            tinyMCE.get("texte").setContent(param.texte);
        }
    );
}

// Fonction pour ouvrir ou fermer un bloc
// Le bouton doit avoir pour data-id l'id du bloc à gérer
function toggle_bloc_hidden(e)
{
    id = "#"+jQuery(this).attr("data-id");
    jQuery(id).toggleClass('blocHidden');
    jQuery(this).toggleClass('open');
}


