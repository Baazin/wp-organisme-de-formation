jQuery(document).ready(function($)
{
    function parse_checked_cols(col)
    {
        if (col !== undefined)
        {
            parent = col.closest('li');
            o = Object();
            o.db_text = parent.find(".db_text").text();
            o.new_text = parent.find(".new_text input").val();
            o.valeur = parent.find(".valeur input").val();
            o.entite = parent.find(".entite").text();
            return o;
        }
    }
    
    $(".action_export").click(function(e)
    {
        log = $("#log");
        bouton = $(this);
        const sessions = [];
        $("table.selection_export td input:checked").each(function()
        {
            sessions.push($(this).attr('name'));
        });
        
        cols = [];
        //$("ul.colonnes_export .li-sort input:checked").each(parse_checked_cols);
        $("ul.colonnes_export .li-sort input:checked").each(function()
        {
            cols.push(parse_checked_cols($(this)));
        });
        
        jQuery.post
        (
            ajaxurl,
            {
                'action' : 'opaga_export_data',
                'format' : bouton.attr('data-format'),
                'sessions' : sessions,
                'cols' : cols,
            },
            function(response)
            {
                p = JSON.parse(response);
                force_download(p.data, p.filename, p.mimetype);
            },
        );
    });
    
    $(".select-schema").click(select_schema);
    function select_schema(e)
    {
        e.preventDefault();
        
        parent = $(this).closest(".parent");
        schema = $(this).parent();
        destination = $(schema.attr('data-destination'));
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'active_schema',
                'schema': schema.attr('data-schema'),
            },
            function(response)
            {
                destination.replaceWith(response);
                $("#cols-sortable").sortable();
                $("#cols-sortable").disableSelection();
            }
        )
    }
    
    $(".enregistrer-schema").click(function(e)
    {
        e.preventDefault();
        
        input = $(this);
        bloc_parent = $(this).closest(".parent");
        nom_schema = bloc_parent.find("input[name='nom_schema']");
        cols = [];
        if (nom_schema.val() != "")
        {
            $("ul.colonnes_export .li-sort input:checked").each(function()
            {
                cols.push(parse_checked_cols($(this)));
            });
            
            jQuery.post
            (
                ajaxurl,
                {
                    'action': 'enregistrer_schema',
                    'nom_schema': nom_schema.val(),
                    'cols': cols,
                },
                function(response)
                {
                    param = JSON.parse(response);
                    if (param.log != false)
                    {
                        nom_schema.val("");
                        jQuery.post
                        (
                            ajaxurl,
                            { 'action': 'get_schemas' },
                            function(response)
                            {
                                bloc_parent.find(".schemas_dispo").replaceWith(response);
                                bloc_parent.on('click', '.select-schema', select_schema);
                                bloc_parent.on('click', '.delete-schema', delete_schema);
                            }
                        );
                    }
                }
            );
        }
        else
            nom_schema.addClass("bg-alerte").delay(700).queue(function(){ $(this).removeClass("bg-alerte").dequeue(); });
    });
    
    $(".delete-schema").click(delete_schema);
    function delete_schema(e)
    {
        e.preventDefault();
        
        parent = $(this).closest(".parent");
        schema = $(this).parent();
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'supprimer_schema',
                'schema': schema.attr('data-schema'),
                'user_id': schema.attr('data-userid'),
            },
            function(response)
            {
                param = JSON.parse(response);
                console.log(param);
                if (param.log != false)
                    schema.remove();
                else
                    schema.addClass("bord-alerte").delay(700).queue(function(){ $(this).removeClass("bord-alerte").dequeue(); });
            }
        );
    }
    
    $("#cols-sortable").sortable();
    $("#cols-sortable").disableSelection();
    
    $(".cols_action").click(function(e)
    {
        e.preventDefault();
        
        action = $(this).attr('data-action');
        list = $("#" + $(this).attr('data-list'));
        entite = $(this).attr('data-entite');
        
        switch(action)
        {
            case "showhide_uncheck":
                list.find("input:checkbox:not(:checked)").closest("li").toggleClass('visible');
                break;
            case "showhide_entite":
                list.find("input:checkbox:not(:checked)").closest("li." + entite).toggleClass('visible');
                break;
            case "add_col":
                jQuery.post
                (
                    ajaxurl,
                    { 'action': 'add_colonne_export' },
                    function(response)
                    {
                        list.prepend(response);
                    }
                )
            default:
                break;
        }
    });
    
    $(".export .head input[type='checkbox']").change(function(e)
    {
        e.preventDefault();
        
        $("#" + $(this).attr('data-list')).find(".global_check input[type='checkbox']").prop('checked', $(this).is(':checked'));
    });
    
    $(".inter_opaga_export").click(function(e)
    {
        log = $("#log");
        bouton = $(this);
        const donnees = [];
        $(".inter_opaga table.export td input:checked").each(function()
        {
            donnees.push($(this).attr('name'));
        });
        
        jQuery.post
        (
            ajaxurl,
            {
                'action' : 'inter_opaga_export_data',
                'format' : bouton.attr('data-format'),
                'type' : bouton.closest("div.data").attr('id'),
                'donnees' : donnees,
            },
            function(response)
            {
                p = JSON.parse(response);
                force_download(p.data, p.filename, p.mimetype);
            },
        );
    });
    
    
    
});
