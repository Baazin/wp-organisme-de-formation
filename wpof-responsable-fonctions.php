<?php
/*
 * wpof-first-init.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 

/*
 * Renseigne le tableau $SessionFormation avec les sessions concernant l'année comptable passée en argument.
 */
function select_session_by_annee($annee)
{
    global $SessionFormation;
    $SessionFormation = array();
    
    $meta_queries = array();
    $session_posts = get_posts( array('post_type' => 'session', 'meta_query' => $meta_queries, 'posts_per_page' => -1));
    
    foreach($session_posts as $post)
    {
        if ($annee <= 0)
            $SessionFormation[$post->ID] = new SessionFormation($post->ID);
        else
        {
            $creneaux = get_post_meta($post->ID, "creneaux", true);
            if (is_array($creneaux))
            {
                $dates = array_keys($creneaux);
                $annee_min = explode('/', $dates[0]);
                $annee_min = $annee_min[2];
                $annee_max = explode('/', $dates[count($dates)-1]);
                $annee_max = $annee_max[2];
                if ($annee >= $annee_min && $annee <= $annee_max)
                    $SessionFormation[$post->ID] = new SessionFormation($post->ID);
            }
        }
    }
}

/*
 * Renseigne le tableau $SessionFormation avec les sessions concernant la plage de dates passée en argument
 */
function select_session_by_plage($plage)
{
    $user_id = get_current_user_id();
    $role = wpof_get_role($user_id);
    
    global $SessionFormation;
    $SessionFormation = array();
    
    $meta_queries = array();
    $session_posts = get_posts( array('post_type' => 'session', 'meta_query' => $meta_queries, 'posts_per_page' => -1));
    
    $date_min = DateTime::createFromFormat('d/m/Y', $plage['date_debut']);
    $date_max = DateTime::createFromFormat('d/m/Y', $plage['date_fin']);
    
    foreach($session_posts as $post)
    {
        $formateur = get_post_meta($post->ID, "formateur", true);
        if (!is_array($formateur))
            $formateur = array();
        if ($role == "um_formateur-trice" && !in_array($user_id, $formateur))
            continue;
        $creneaux = get_post_meta($post->ID, "creneaux", true);
        if (is_array($creneaux))
        {
            $dates = array_keys($creneaux);
            $debut = DateTime::createFromFormat('d/m/Y', $dates[0]);
            $fin = DateTime::createFromFormat('d/m/Y', $dates[count($dates)-1]);
            if ($fin >= $date_min && $debut <= $date_max)
                $SessionFormation[$post->ID] = new SessionFormation($post->ID);
        }
     
    }
}

function get_choix_annee_comptable($annee = null)
{
    global $wpof;
    
    if ($annee == null)
    {
        $annee = get_user_meta(get_current_user_id(), "annee_comptable", true);
        if (empty($annee))
            $annee = date('Y') - 1;
    }
    
    $annee_actuelle = date('Y');
        
    $liste_annee = array("Toutes");
    for ($a = $wpof->annee1; $a <= $annee_actuelle+1; $a++)
        $liste_annee[$a] = $a;
    
    return select_by_list($liste_annee, "annee_choix", $annee, "data-userid='".get_current_user_id()."'");
}

add_action('wp_ajax_manage_random_stagiaire', 'manage_random_stagiaire');
function manage_random_stagiaire()
{
    $reponse = array('log' => array());
    $session = new SessionFormation($_POST['session_id']);
    $client = new Client($_POST['session_id'], $_POST['client_id']);
//    $client->init_stagiaires();
    
    $nb_stagiaire_actuel = count($client->stagiaires);
    $nb_stagiaire_futur = $_POST['meta_value'];
    
    if ($nb_stagiaire_futur > $nb_stagiaire_actuel)
    {
        require_once(wpof_path . "/wpof-extra-config.php");
        
        for($i = 0; $i < $nb_stagiaire_futur - $nb_stagiaire_actuel; $i++)
        {
            $identite = $faux_nom[rand(0, count($faux_nom) - 1)];
            $userdata = array
            (
                'first_name' => $identite['prenom'],
                'last_name' => $identite['nom'],
                'user_email' => sanitize_user(strtolower($identite['prenom'].$identite['nom']))."@".date_i18n("Ymjis")."fausseadres.se",
                'user_login' => sanitize_user(strtolower($identite['prenom'].$identite['nom'])),
                'user_pass' => NULL
            );
            
            $reponse['log'][] = $userdata;
            
            $user_id = wp_insert_user($userdata);
            $client->stagiaires[] = $user_id;
            $client->update_meta("stagiaires");
            $session->inscrits[] = $user_id;
            $session->update_meta("inscrits");
        }
    }
    
    $reponse['html'] = get_pilote_stagiaires($client);
    
    echo json_encode($reponse);
    die();
}

?>
