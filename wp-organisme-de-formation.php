<?php
/**
 * Plugin Name: OPAGA
 * Plugin URI: https://opaga.fr/
 * Description: Plugin permettant de gérer l'administration de la formation professionnelle continue selon la loi française. Gestion des inscriptions, création automatique des documents obligatoires, suivi des stagiaires, bilan pédagogique et financier.
 * Version: 0.1
 * Author: Dimitri Robert – CAE Coopaname
 * Author URI:  https://formation-logiciel-libre.com/
 * License: AGPL3 license
 * Depends: Ultimate Member
 *
 * Copyright 2018 Dimitri Robert <contact@opaga.fr>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

define( 'debug', get_option("wpof_debug") == 1 );
if (debug)
{
    error_reporting(E_ALL);
    ini_set('display_errors','On');
}

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'WPOF_VERSION', '0.1' );
define( 'WPOF_WP_VERSION', get_bloginfo( 'version' ) );
define( 'wpof_url', plugin_dir_url(__FILE__ ));
define( 'wpof_path', plugin_dir_path(__FILE__ ));
define( 'dompdf_path', wpof_path . "/vendor/dompdf/dompdf" );

setlocale(LC_TIME, 'fr_FR.utf8','fra');

require_once(wpof_path . "/wpof-first-init.php");

function activate_wpof()
{
    update_option('Activated_Plugin', 'OPAGA');
}
register_activation_hook(__FILE__, 'activate_wpof');

function load_plugin()
{
    if (is_admin() && get_option('Activated_Plugin') == 'OPAGA')
    {
        if (is_plugin_active('ultimate-member/ultimate-member.php'))
        {
            delete_option('Activated_Plugin');

            if (get_option("wpof_version") == "")
                first_init();
        }
    }
}
add_action( 'admin_init', 'load_plugin' );

function check_um_active_notice()
{
    if (is_admin() && !is_plugin_active('ultimate-member/ultimate-member.php'))
    {
         echo '<div class="notice notice-error">
             <p>Vous devez installer et activer le plugin <a href="'.get_admin_url().'plugin-install.php?s=ultimate+member&tab=search&type=term">Ultimate Member</a></p>
         </div>';
    }
}
add_action('admin_notices', 'check_um_active_notice');

define( 'WPOF_TABLE_SUFFIX_DOCUMENTS', 'wpof_documents');

setlocale(LC_ALL, 'fr_FR@utf8');

add_action( 'wp_enqueue_scripts', 'wpof_load_scripts', 21 );
add_action( 'admin_enqueue_scripts', 'wpof_load_scripts', 21 );
function wpof_load_scripts()
{
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_script('jquery-ui-dialog');
    wp_enqueue_script('jquery-ui-tabs');
    wp_enqueue_script('jquery-ui-menu');
    wp_enqueue_script('jquery-ui-sortable');
    wp_enqueue_style('jquery-ui', wpof_url."css/jquery-ui.css");
    
    wp_enqueue_script( 'wpof', wpof_url."js/wpof.js", array('jquery') );
    wp_enqueue_script( 'wpof-token', wpof_url."js/wpof-token.js", array('jquery') );
    wp_enqueue_script( 'wpof-doc', wpof_url."js/wpof-doc.js", array('jquery') );
    wp_localize_script('wpof', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    wp_enqueue_style( 'dashicons' );
    
    if (is_admin())
    {
        wp_enqueue_style( 'wpof-admin', wpof_url."css/style-admin.css" );
        wp_enqueue_script( 'wpof-admin', wpof_url."js/wpof-admin.js", array('jquery') );
        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_script( 'iris', admin_url( 'js/iris.min.js' ), array( 'jquery-ui-draggable', 'jquery-ui-slider', 'jquery-touch-punch' ), false, 1 );
        wp_enqueue_script( 'wp-color-picker', admin_url('js/color-picker.min.js'), array('iris'), false, 1 );
    }
    else
    {
        global $post;
        global $wpof;
        
        if ($post && ($post->post_name == $wpof->url_acces || (is_user_logged_in() && ($post->post_type == "session" || ($post->post_type == "formation") && isset($_GET[$wpof->formation_edit_link_suffix]) || in_array($post->post_name, $wpof->no_theme) || get_the_ID() == um_get_option('core_user')))))
        {
            global $wp_styles;
            
            foreach($wp_styles->registered as $style)
            {
                if (preg_match('@wp-content/themes@', $style->src))
                    wp_dequeue_style($style->handle);
            }
            
            if (file_exists(get_stylesheet_directory()."/opaga-no-theme.css"))
                $no_theme_css = get_stylesheet_directory_uri()."/opaga-no-theme.css";
            else
                $no_theme_css = wpof_url."css/opaga-no-theme.css";
            wp_enqueue_style('opaga_no_theme', $no_theme_css);
            wp_enqueue_style('opaga_virtual_pages', wpof_url."css/virtual-pages.css");
            wp_enqueue_style('jquery-datatables-css', wpof_url.'vendor/datatables/datatables/media/css/jquery.dataTables.min.css');
            wp_enqueue_script('jquery-datatables-js-load', wpof_url.'js/datatables.js',array('jquery'));
            wp_enqueue_script('jquery-datatables-js', wpof_url.'vendor/datatables/datatables/media/js/jquery.dataTables.min.js',array('jquery'));
        }
    }
    wp_enqueue_style( 'wpof', wpof_url."css/wpof.css" );
    wp_enqueue_style( 'wpof_public', wpof_url."css/public-pages.css" );
    
    $role = wpof_get_role(get_current_user_id());
    if ($role != "um_stagiaire")
    {
        wp_enqueue_script( 'wpof_cpt', wpof_url."cpt/cpt.js", array('jquery') );
        wp_enqueue_editor();
    }
}

add_action('init', 'init_opaga_page', 10);
function init_opaga_page()
{
    global $wpof;
    require_once(wpof_path . "/class/class-page-virtuelle.php");
    
    $uri_params = trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
    
    // lorsque l'on arrive ici depuis un appel ajax.post, il faut charger le HTTP_REFERER dans uri_params pour recréer les conditions de la page virtuelle
    if (substr_count($uri_params, 'admin-ajax.php') > 0 && !empty($_SERVER['HTTP_REFERER']))
        $uri_params = trim(str_replace(home_url(), '', $_SERVER['HTTP_REFERER']), '/');
    
    $uri_params = preg_split('/[\/?]+/', $uri_params);
    $slug = array_shift($uri_params);
        
    if (get_option("wpof_version") != "" && (is_user_logged_in() || $slug == $wpof->url_acces))
    {
        if (in_array($slug, $wpof->no_theme))
        {
            global $virtual_pagename;
            require_once(wpof_path . "/class/class-page-virtuelle.php");
            require_once(wpof_path . "/wpof-responsable-fonctions.php");
            
            $virtual_pagename = array_search($slug, $wpof->no_theme);
            
            $page_file = wpof_path . '/pages/page-'.$virtual_pagename.'.php';
            if (!file_exists($page_file))
                throw new Exception("$page_file introuvable");
            
            require_once $page_file;
            $page_virtuelle = new PageVirtuelle(array('pagename' => $virtual_pagename, 'params' => $uri_params));
            
            add_filter("page_template", 'get_opaga_page_template');
            add_filter("single_template", 'get_opaga_page_template');
        }
            
        if ($slug == "session" || ($slug == "formation") && isset($_GET[$wpof->formation_edit_link_suffix]))
            add_filter('single_template', 'get_opaga_page_template');
    }
    else
    {
        if ($slug == $wpof->url_user && isset($uri_params[0]))
        {
            $user = get_user_by('login', $uri_params[0]);
            if ($user)
            {
                $formateur = new Formateur($user->ID);
                add_filter('the_content', array($formateur, 'vue_publique'));
                add_filter('archive_template', 'get_default_page_template');
                require_once wpof_path . '/pages/page-user.php';
                $page_virtuelle = new PageVirtuelle(array('pagename' => 'user', 'params' => $uri_params, 'title' => $formateur->get_displayname()));
            }
        }
    }
    
}

function get_opaga_page_template($template)
{
    global $wpof, $post;
    if (is_user_logged_in() || $post->post_name == $wpof->url_acces)
        $template = wpof_path . '/template/no-theme/page.php';
    
    return $template;
}

function get_default_page_template($template)
{
    return get_stylesheet_directory() . '/page.php';
}

function rewrite_opaga_url()
{
    global $wpof;
    foreach($wpof->no_theme as $slug)
        add_rewrite_rule($slug.'/([^/]+)/?$', 'index.php?post_type=page&$matches[1]', 'top');
}
add_action('init', 'rewrite_opaga_url');

function wpof_session_start()
{
    if (!session_id())
        @session_start();
    // le préfixe @ permet d'ignorer les erreurs envoyés par la fonction, par exemple, si la configuration PHP ne supporte pas les sessions
}
add_action( 'init', 'wpof_session_start', 1 );

require_once(wpof_path . "/wpof-config.php");
require_once(wpof_path . "/wpof-fonctions.php");
require_once(wpof_path . "/wpof-custom-post-types.php");
require_once(wpof_path . "/wpof-session-formation.php");
require_once(wpof_path . "/wpof-utilisateur.php");
require_once(wpof_path . "/wpof-formation.php");
require_once(wpof_path . "/wpof-messages.php");
require_once(wpof_path . "/wpof-dialog.php");

// classes
require_once(wpof_path . "/class/class-formation.php");
require_once(wpof_path . "/class/class-aide.php");
require_once(wpof_path . "/class/class-formateur.php");
require_once(wpof_path . "/class/class-message.php");

// TODO : vérifier si on ne pourrait se contenter d'appeler ces deux ressources que dans la page-export.php
            require_once(wpof_path . "/class/class-schema-export.php");
                require_once wpof_path . '/pages/page-export.php';

// Expéditeur des mails
function new_mail_from_name($old)
{
    return get_bloginfo('name');
}
add_filter('wp_mail_from_name', 'new_mail_from_name');
function new_mail_from_email($old)
{
    return get_bloginfo('admin_email');
}
add_filter('wp_mail_from', 'new_mail_from_email');

function opaga_add_board($array, $int)
{
    global $wpof;
    if (is_user_logged_in())
        $array = '<li class="opaga-board menu-item"><a href="'.home_url().'/'.$wpof->url_user.'/">'.__("Tableau de bord").'</a></li>'.$array;
    
    return $array; 
}; 
add_filter('wp_nav_menu_items', 'opaga_add_board', 10, 2);

// globales
$Formation = array();
$SessionFormation = array();
$SessionStagiaire = array();
$Client = array();
$Documents = array();

function add_extra_plugin($plugins)
{
    $plugins['table'] = wpof_url.'js/tinymce-plugins/table/plugin.min.js';
    $plugins['searchreplace'] = wpof_url.'js/tinymce-plugins/searchreplace/plugin.min.js';
    return $plugins;
}
add_filter('mce_external_plugins', 'add_extra_plugin');

if (is_admin())
{
    require_once(wpof_path . "/wpof-admin.php");
    //if (get_option("wpof_version") < WPOF_VERSION)
        require_once(wpof_path . "/wpof-mise-a-jour.php");
}

?>
