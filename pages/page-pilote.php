<?php
/*
 * page-pilote.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

define( 'wpof_path_csv', WP_CONTENT_DIR . "/uploads");


$wpof->session_keys = array
(
    'id' => array('text' => __('ID')),
    'titre_session' => array('text' => __('Intitulé')),
    'specialite' => array('text' => __('Spécialité')),
    'dates_texte' => array('text' => __('Dates')),
    'formateur' => array('text' => __('Formateur⋅trice(s)')),
    'nb_heure_estime_decimal' => array('text' => __('Durée réelle ou estimée (h)')),
    'budget_global' => array('text' => __('Budget global')),
    'nb_stagiaires' => array('text' => __('Stagiaires')),
);

$wpof->client_keys = array
(
    'numero_contrat' => array('text' => __('N° contrat')),
    'nom' => array('text' => __('Client')),
    'exe_comptable' => array('text' => __('Exercice comptable')),
    'financement' => array('text' => __('Financement')),
    'nature_formation' => array('text' => __('Objectif prestation')),
    'nb_heure_estime_decimal' => array('text' => __('Durée (h en décimal)')),
    'tarif_heure' => array('text' => __('Tarif horaire')),
    'tarif_total_chiffre' => array('text' => __('Tarif total')),
    'tarif_total_autres_chiffre' => array('text' => __('Autres frais (montant)')),
    'stagiaires' => array('text' => __("Stagiaires")),
    'erreur' => array('text' => __("Erreur(s)")),
);

if (!champ_additionnel('numero_contrat'))
    unset($wpof->client_keys['numero_contrat']);

$wpof->stagiaire_keys = array
(
    'prenom' => array('text' => __('Prénom')),
    'nom' => array('text' => __('Nom')),
    'statut_stagiaire' => array('text' => __('Statut')),
    'confirme' => array('text' => __('Insc. confirmée')),
    'nb_heure_estime_decimal' => array('text' => __('Durée réelle ou estimée (h)')),
);

$wpof->erreur_ctrl = array
(
    'tarif0' => __('Tarif nul'),
    'duree0' => __('Durée nulle'),
    'stagiaire0' => __('Aucun stagiaire'),
    'heure_stagiaire0' => __('Durée × stagiaire nulle'),
);

// un tableau pour recenser les erreurs par formateurs
$wpof->formateurs_erreur = array();

function get_pilote_content()
{
    $role = wpof_get_role(get_current_user_id());
    $html = "<div id='pilote-content'>";
    
    if ($role == "admin")
        $html .= "<div id='pilote_dialog'></div>";
        
    $annee_defaut = get_user_meta(get_current_user_id(), "annee_comptable", true);
    if (empty($annee_defaut))
        $annee_defaut = -1;
    
    if (in_array($role, array("um_responsable", "admin")))
    {
        $html .= get_top_bloc_pilote();
        $html .= "<div id='pilote' data-annee='$annee_defaut'>";
        $html .= get_pilote($annee_defaut);
        $html .= "</div>";
    }
    $html .= "</div>";
    
    return $html;
}

function get_top_bloc_pilote()
{
    global $wpof;
    ob_start();
    ?>
    <div id="top-bloc-pilote">
        <div id='annee_choix' data-id='pilote'><?php echo get_choix_annee_comptable(); ?></div>
        <?php //echo get_fullscreen_mode("#pilote-content"); ?>
        <?php echo get_edition_mode("#table-editable"); ?>
        <span class="bouton affiche_verif_dialog"><?php _e("Gérer les rapports d'erreur"); ?></span>
        <div class="filtre">
        <p><?php _e("Cochez pour voir, décochez pour cacher"); ?></p>
        <span class="bouton toggle fait" data-target="#table-editable" data-toggle="tr.session"><?php _e("En-tête de session"); ?>
        </span> | <span class="bouton toggle fait" data-target="#table-editable" data-toggle="tr.client:not(.opac)"><?php _e("Clients directs"); ?>
        </span><span class="bouton toggle fait" data-target="#table-editable" data-toggle="tr.client.opac"><?php _e("En sous-traitance"); ?>
        </span> | <?php
        foreach($wpof->erreur_ctrl as $key => $text) :
        ?><span class="bouton toggle" data-target="#table-editable" data-toggle="tr.client.<?php echo $key; ?>"><?php echo $text; ?></span><?php
        endforeach;
        ?></div>
    </div>
    <?php
    return ob_get_clean();
}

function get_pilote($annee)
{
    global $wpof;
    global $SessionFormation;
    $role = wpof_get_role(get_current_user_id());
    
    select_session_by_annee($annee);
    
    $liste = array();
    
    foreach($SessionFormation as $s)
    {
        if ($s->numero != "")
            $liste[$s->numero] = $s;
        else
            $liste[$s->id] = $s;
    }
    ksort($liste);
    
    ob_start();
    ?>
    <table id="table-editable" class="pilote_clients pilote">
    <thead>
    <tr><?php array_walk($wpof->client_keys, "pilote_array_map_th", "client_"); ?></tr>
    </thead>
    
    
    <tbody>
    <?php foreach($liste as $s) : ?>
    <?php $s->init_clients(); $s->init_stagiaires(); $s->calcule_budget_global(); ?>
    <tr data-sessionid="<?php echo $s->id; ?>" class="session session<?php echo $s->id; ?>" id="session<?php echo $s->id; ?>">
    <td colspan="<?php echo count($wpof->client_keys); ?>">
    <div>
        <p class="delete-entity icone-bouton" data-objectclass="<?php echo get_class($s); ?>" data-id="<?php echo $s->id; ?>" data-parent=".session<?php echo $s->id; ?>">
        <span class="dashicons dashicons-dismiss" ></span> <?php _e("Supprimer"); ?>
        </p>
        <p><?php echo get_edition_mode(".session{$s->id}"); ?></p>
        <?php foreach($wpof->session_keys as $k => $val) : ?>
        <div class="<?php echo $k; ?>"><span class='legende'><?php echo $val['text']; ?> : </span> <?php
            if (isset($s->$k))
            {
                switch($k)
                {
                    case 'titre_session':
                        echo "<a href='{$s->permalien}'>".$s->$k."</a>";
                        break;
                    case 'nb_stagiaires':
                        $erreur = false;
                        echo "<p>".$s->$k."</p>";
                        break;
                    case 'formateur':
                        if (!isset($wpof->formateur))
                            init_term_list("formateur");
                        echo get_input_jpost($s, $k, array('select' => 'multiple', 'aide'));
                        break;
                    case 'dates_texte':
                    case 'budget_global':
                    case 'id':
                        echo $s->$k;
                        break;
                    case 'nb_heure_estime_decimal':
                        echo get_input_jpost($s, $k, array('input' => 'number', 'step' => 0.01, 'min' => 0));
                        break;
                    case 'specialite':
                        echo get_input_jpost($s, $k, array('select' => '', 'first' => __("Choisir")));
                        break;
                    default:
                        echo get_input_jpost($s, $k, array('input' => 'text'));
                        break;
                }
            }
        ?>
        </div>
        <?php endforeach; ?>
    </div>
    </td>
    </tr>
    <?php echo get_pilote_clients($s); ?>
    <?php endforeach; ?>
    </tbody>
    </table>
    <?php
    if (!empty($wpof->formateurs_erreur))
        echo get_verif_dialog();
        
    return ob_get_clean();
}

function get_pilote_clients($session)
{
    if (count($session->clients) == 0) return "";
    
    global $wpof;
    $session_id = $session->id;
    $role = wpof_get_role(get_current_user_id());
    
    ob_start();
    ?>
    <?php foreach($session->clients as $cid) : ?>
    <?php
        $client = get_client_by_id($session_id, $cid);
        $ctrl_class = array();
        if ($client->tarif_total_chiffre == 0) $ctrl_class[] = "tarif0";
        if ($client->nb_stagiaires == 0) $ctrl_class[] = "stagiaire0";
        if ($client->nb_heure_estime_decimal == 0) $ctrl_class[] = "duree0";
        if ($client->financement == "opac" && $client->nb_heures_stagiaires == 0) $ctrl_class[] = "heure_stagiaire0";
        if(count($ctrl_class) > 0)
        {
            foreach($session->formateur as $fid)
            {
                if (!isset($wpof->formateurs_erreur[$fid]))
                    $wpof->formateurs_erreur[$fid] = array();
                $row = get_displayname($fid).";{$client->numero_contrat};{$client->nom};{$session->titre_session};{$client->tarif_total_chiffre};{$client->nb_stagiaires};{$client->nb_heure_estime_decimal}";
                if ($client->financement == "opac")
                    $row .= ";{$client->nb_heures_stagiaires}";
                $wpof->formateurs_erreur[$fid][] = $row;
            }
        }
    ?>
    <tr id="client-<?php echo $client->id; ?>" class="client <?php echo (count($ctrl_class) > 0) ? join(" ", $ctrl_class)." erreur" : ""; ?> <?php echo $client->financement; ?> session<?php echo $session->id; ?>" data-sessionid="<?php echo $session_id; ?>" data-clientid="<?php echo $cid; ?>">
        <?php foreach($wpof->client_keys as $k => $val) : ?>
        <td class="<?php echo $k; ?>">
        <?php
            if (isset($client->$k))
            {
                switch($k)
                {
                    case 'numero_contrat':
                        echo get_input_jpost($client, $k, array('input' => 'text', 'aide' => false));
                        break;
                    case 'nom':
                        echo get_input_jpost($client, $k, array('input' => 'text', 'aide' => false)); ?>
                        <p class="delete-entity icone-bouton" data-objectclass="<?php echo get_class($client); ?>" data-id="<?php echo $client->id; ?>" data-sessionid="<?php echo $client->session_formation_id; ?>" data-parent="#client-<?php echo $client->id; ?>">
                        <span class="dashicons dashicons-dismiss" > </span> <?php _e("Supprimer client"); ?> </p>
                        <?php
                        break;
                    case 'exe_comptable':
                        echo get_pilote_exe_comptable($session);
                        break;
                    case 'nature_formation':
                        echo get_input_jpost($client, $k, array('select' => '', 'first' => __("Choisir"), 'aide' => false));
                        break;
                    case 'financement':
                        if ($client->$k == "opac")
                            echo $wpof->financement->get_term("opac");
                        else
                            echo get_input_jpost($client, $k, array('select' => '', 'first' => __("Choisir"), 'aide' => false));
                        break;
                    case 'stagiaires':
                        $erreur = false;
                        if ($client->financement == "opac")
                        {
                            echo get_input_jpost($client, "nb_stagiaires", array('input' => 'number', 'min' => 0, 'label' => 'Stagiaires', 'aide' => false));
                            echo get_input_jpost($client, "nb_heures_stagiaires", array('input' => 'number', 'min' => 0, 'label' => 'Heures stagiaires', 'aide' => false));
                        }
                        else
                        {
                            if (champ_additionnel('random_stagiaire'))
                                echo get_input_jpost($client, "nb_stagiaires", array('input' => 'text', 'label' => 'Stagiaires', 'postprocess' => 'manage_random_stagiaire'));
                            else
                                echo "<p>{$client->nb_stagiaires}</p>";
                            echo "<p><span class='toggle-stagiaires bouton' data-id='c{$client->id}'>".__("Stagiaires")."</span></p>";
                        }
                        break;
                    case 'tarif_total_chiffre':
                    case 'tarif_total_autres_chiffre':
                    case 'nb_heure_estime_decimal':
                        echo get_input_jpost($client, $k, array('input' => 'number', 'step' => '0.01', 'min' => '0', 'aide' => false));
                        break;
                    case 'tarif_heure':
                    default:
                        echo $client->$k;
                        break;
                }
            }
            elseif ($k == 'erreur')
            {
                foreach($ctrl_class as $key) : ?>
                <p class="erreur"><?php echo $wpof->erreur_ctrl[$key]; ?></p>
                <?php endforeach;
            }
        ?>
        </td>
        
        <?php endforeach; ?>
    </tr>
    <tr class='pilote-tableau-stagiaires' id='c<?php echo $client->id;?>'>
    <td colspan="<?php echo count($wpof->client_keys); ?>" class="session<?php echo $session->id; ?> client<?php echo $client->id;?>">
    <?php echo get_pilote_stagiaires($client); ?>
    </td>
    </tr>
    <?php endforeach; ?>
    <?php 
    return ob_get_clean();
}

function get_pilote_stagiaires($client)
{
    if (count($client->stagiaires) == 0) return "";
    
    global $wpof;
    $session_id = $client->session_formation_id;
    $session = get_session_by_id($session_id);
    $stagiaires = $client->stagiaires;
    $role = wpof_get_role(get_current_user_id());
    
    ob_start();
    ?>
    <table class="pilote_stagiaires">
    <thead>
    <tr><?php array_walk($wpof->stagiaire_keys, "pilote_array_map_th", "sessionstagiaire_"); ?></tr>
    </thead>
    
    <?php foreach($stagiaires as $sid) : ?>
    <?php $s = get_stagiaire_by_id($session_id, $sid); ?>
    <tr data-clientid="<?php echo $client->id; ?>" data-stagiaireid="<?php echo $s->id; ?>">
        <?php foreach($wpof->stagiaire_keys as $k => $val) : ?>
        <td class="<?php echo $k; ?>">
        <?php
            if (isset($s->$k))
            {
                switch($k)
                {
                    case 'prenom':
                    case 'nom':
                        echo get_input_jpost($s, $k, array('input' => 'text', 'aide' => false));
                        /*if ($role == 'admin')
                            echo "<br />".$session->get_sql_select_button($s->id);*/
                        break;
                    case 'confirme':
                        echo get_input_jpost($s, $k, array('input' => 'checkbox', 'aide' => false));
                        break;
                    case 'statut_stagiaire':
                        echo get_input_jpost($s, $k, array('select' => '', 'first' => __("Choisir"), 'aide' => false));
                        break;
                    case 'nb_heure_estime_decimal':
                        if (empty($s->$k))
                            $s->$k = $s->nb_heure_decimal;
                        echo get_input_jpost($s, $k, array('input' => 'number', 'step' => '0.01', 'min' => '0', 'aide' => false));
                        break;
                    default:
                        echo $s->$k;
                        break;
                }
            }
        ?>
        </td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    
    </table>
    <?php
    return ob_get_clean();
}

function get_verif_dialog()
{
    global $tinymce_wpof_settings, $wpof;
    $message = "Bonjour {prenom},
Il nous manque quelques informations sur tes sessions de formation de l’an dernier. Peux-tu vérifier le tableau ci-joint et remplacer les zéros par les bonnes informations. La dernière colonne (nb heures x stagiaires) n’est à remplir que si tu étais sous-traitant.

Merci d’avance de nous renvoyer ce tableau avant mercredi 12 mai.

Bonne journée";
    
    $rapport_global = array();
    ob_start();
    ?>
    
    <div class="verif_dialog_wrapper">
        <div class="verif_dialog">
        <p><?php echo __('Expéditeur⋅trice')." ".select_user(array('role__in' => array('um_responsable')), "expediteur"); ?></p>
        <p><?php _e('Sujet du message'); ?> <input type="text" name="sujet" value="Informations manquantes sur tes formations 2020" /></p>
        <p><?php _e('Message générique. Les mots-clé {prenom} et {nom} sont respectivement remplacés par le prénom et le nom du destinataire'); ?></p>
        <textarea name="message" cols="60" rows="10">MESSAGE</textarea>
        <p> <span class="bouton get_rapport" data-id="all"><?php _e("Télécharger le rapport global"); ?></span> <span class="bouton voir_rapport" data-id="all"><?php _e("Voir tous les rapports en CSV"); ?></span> </p>
        <?php
        foreach($wpof->formateurs_erreur as $fid => $rapport)
        {
            $formateur = get_user_by("ID", $fid);
            $dest_nom = get_user_meta($fid, "last_name", true);
            $dest_prenom = get_user_meta($fid, "first_name", true);
            $nb_client_erreur = count($rapport);
            $rapport_global = array_merge($rapport_global, $rapport);
            array_unshift($rapport, "Formateur-trice;Numéro de contrat;Client;Session de formation;Tarif total HT;Nombre de stagiaires;Durée (heures en décimal);Nb heures x stagiaires (sous-traitance)");
            ?>
            <p><strong><?php echo get_displayname($fid, false); ?></strong> (<?php echo $formateur->user_email; ?>) <span class="bouton envoi_rapport" data-dest="<?php echo $fid; ?>"><?php _e("Envoi du rapport"); ?></span> <span class="bouton voir_rapport" data-id="csv<?php echo $fid; ?>"><?php _e("Voir rapport en CSV"); ?></span> <strong><?php $s = ($nb_client_erreur > 1) ? "s" : ""; echo $nb_client_erreur." ".__("erreur").$s; ?></strong> <?php
            $file_name = wpof_path_csv."/".sanitize_title($dest_prenom.$dest_nom).".csv";
            if (file_exists($file_name))
                echo __("Envoyé le ").date_i18n("d/m/Y à H:i", filemtime($file_name));
            ?>
            </p>
            <p class="message" id="message<?php echo $fid; ?>"></p>
            <pre class="rapport_csv" id="csv<?php echo $fid; ?>"><?php echo implode(PHP_EOL, $rapport); ?></pre>
            <?php
        }
        array_unshift($rapport_global, "Formateur-trice;Numéro de contrat;Client;Session de formation;Tarif total HT;Nombre de stagiaires;Durée (heures en décimal);Nb heures x stagiaires (sous-traitance)");
        ?>
        <pre class="rapport_csv" id="csv_global"><?php echo implode(PHP_EOL, $rapport_global); ?></pre>
        </div>
    </div>
    
    <?php
    $html = ob_get_clean();
    $html = str_replace("MESSAGE", $message, $html);
    return $html;
}

add_action('wp_ajax_envoi_rapport', 'envoi_rapport');
function envoi_rapport()
{
    $destinataire = get_userdata($_POST['destinataire']);
    $dest_nom = get_user_meta($_POST['destinataire'], "last_name", true);
    $dest_prenom = get_user_meta($_POST['destinataire'], "first_name", true);
    $dest_email = $destinataire->user_email;
    $expediteur = get_userdata($_POST['expediteur']);
    $exp_nom = get_displayname($expediteur->ID);
    $exp_email = $expediteur->user_email;
    $message = str_replace("{nom}", $dest_nom, $_POST['message']);
    $message = str_replace("{prenom}", $dest_prenom, $_POST['message']);
    $sujet = $_POST['sujet'];
    
    $file_name = wpof_path_csv."/".sanitize_title($dest_prenom.$dest_nom).".csv";
    
    $csv_handle = fopen($file_name, "w");
    fwrite($csv_handle, $_POST['csv']);
    fclose($csv_handle);
    
    $mail = new Message(array('to' => $dest_email, 'subject' => $sujet, 'content' => $message, 'replyto' => $exp_email, 'attachment' => $file_name));
    $res = $mail->sendmail();
    if ($res)
        printf(__("Message envoyé à %s avec %s"), "$dest_prenom $dest_nom ($dest_email)", $file_name);
    else
        _e("Échec de l'envoi");
    
    die();
}

function get_pilote_exe_comptable($session)
{
    $exe_array = array();
    foreach($session->exe_comptable as $y => $b)
        $exe_array[] = "$y → $b €";
    
    return join('<br />', $exe_array);
}

?>
