<?php
/*
 * wpof-bpf.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

function get_bpf_content()
{
    $role = wpof_get_role(get_current_user_id());
    
    if (!in_array($role, array("um_responsable", "admin")))
        return "";
    
    $annee_defaut = get_user_meta(get_current_user_id(), "annee_comptable", true);
    if (empty($annee_defaut))
        $annee_defaut = date('Y') - 1;
    
    $html = "";
    $html .= "<div id='annee_choix' data-id='bpf'>";
    $html .= get_choix_annee_comptable($annee_defaut);
    $html .= "</div>";
    
    $html .= "<div id='bpf' data-annee='$annee_defaut'>";
    $html .= get_bpf($annee_defaut);
    $html .= "</div>";
    return $html;
}

function get_bpf($annee)
{
    global $SessionFormation;
    global $Client;
    global $wpof;
    select_session_by_annee($annee);
    
    $html = "";
    
    $session_ctl = array();
    $session_ctl[0] = array();
    
    $nb_stagiaires = $nb_stagiaires_st = $nb_clients_st = $nb_clients = 0;
    foreach($SessionFormation as $s)
    {
        $s->init_clients();
        $s->init_stagiaires();
        
        foreach($s->clients as $cid)
        {
            if ($Client[$cid]->financement == "opac")
            {
                $nb_stagiaires_st += $Client[$cid]->nb_stagiaires;
                $nb_clients_st ++;
            }
            else
            {
                $nb_stagiaires += count($Client[$cid]->stagiaires);
                $nb_clients ++;
            }
        }
        
        if (!in_array($s->type_index, array_keys($wpof->type_session)))
            $session_ctl[0][] = get_session_numero($s);
        else
        {
            if (empty($session_ctl[$s->type_index]))
                $session_ctl[$s->type_index] = array();
            $session_ctl[$s->type_index][] = get_session_numero($s);
        }
    }
    $html .= "<ul>";
    $html .= "<li>".__("Nombre de sessions pour")." $annee : ".count($SessionFormation)."</li>";
    $html .= "<li>Nombre de clients en direct : $nb_clients</li>";
    $html .= "<li>Nombre de clients en sous-traitance : $nb_clients_st</li>";
    $html .= "<li>Nombre de stagiaires en direct : $nb_stagiaires</li>";
    $html .= "<li>Nombre de stagiaires en sous-traitance : $nb_stagiaires_st</li>";
    $html .= "</ul>";
    
    $html .= get_bpf_c($annee);
    $html .= get_bpf_d();
    $html .= get_bpf_e();
    $html .= get_bpf_f1();
    $html .= get_bpf_f2();
    $html .= get_bpf_f3();
    $html .= get_bpf_f4();
    $html .= get_bpf_g();
    
    return $html;
}

// Bilan financier HT : origine des produits de l'OF
function get_bpf_c($annee)
{
    global $wpof;
    global $SessionFormation;
    global $Client;
    
    $total_produits = 0;
    $financement = array();
    foreach(array_keys($wpof->financement->term) as $k)
        $financement[$k] = 0;
    $fin_erreur = array();
    $fin_ctl = array();
    $fin_total = 0;
    
    foreach($Client as $client)
    {
        if ($wpof->financement->is_term($client->financement))
        {
            $financement[$client->financement] += ($client->tarif_total_chiffre - $client->tarif_total_autres_chiffre);
            $fin_ctl[$client->session_formation_id][$client->id] = $client->financement;
        }
        else
            $fin_erreur[$client->session_formation_id][$client->id] = $client->financement;
        
        $financement['autres'] += $client->tarif_total_autres_chiffre;
        $fin_total += $client->tarif_total_chiffre;
    }
    
    ob_start();
    ?>
    <h2><?php _e("C. Bilan financier hors taxes : origine des produits de l'organisme"); ?></h2>
    
    <?php
        $sous_total = $financement["prive"];
        $total_produits += $sous_total;
    ?>
    <p class="value"><?php echo $wpof->financement->get_term("prive"); ?><span class="bpf_right">1. </span><span class="bpf_value"><?php echo $sous_total; ?></span></p>
    
    <div class="bpf_group">
        <p class="groupe"><?php echo $wpof->financement->group["g1"]; ?></p>
        
        <?php
            $lettre = array('', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
            $total_mutu = 0;
        ?>
        <?php for($i = 1; $i <= 8; $i++) : ?>
            <?php
                $sous_total_mutu = $financement["mutu".$i];
                $total_mutu += $sous_total_mutu;
            ?>
            <p class="sous_value"><?php echo $wpof->financement->get_term("mutu".$i); ?><span class="bpf_right"><?php echo $lettre[$i]; ?>. </span><span class="bpf_value"><?php echo $sous_total_mutu; ?></span></p>
        <?php endfor; ?>
        
        <?php
            $total_produits += $total_mutu;
        ?>
        <p class="value"><?php echo __("Total")." ".$wpof->financement->group["g1"]." ".__("(total des lignes a à h)"); ?> <span class="bpf_right">2. </span><span class="bpf_value"><?php echo $total_mutu; ?></span></p>
    </div>
    
    <?php
        $sous_total = $financement["public"];
        $total_produits += $sous_total;
    ?>
    <p class="value"><?php echo $wpof->financement->get_term("public"); ?><span class="bpf_right">3. </span><span class="bpf_value"><?php echo $sous_total; ?></span></p>
    
    <div class="bpf_group">
        <p class="groupe"><?php echo $wpof->financement->group["g2"]; ?></p>
        
        <?php
            $total_pubspec = 0;
        ?>
        <?php for($i = 1; $i <= 5; $i++) : ?>
            <?php
                $sous_total_pubspec = $financement["pubspec".$i];
                $total_pubspec += $sous_total_pubspec;
            ?>
            <p class="sous_value"><?php echo $wpof->financement->get_term("pubspec".$i); ?><span class="bpf_right"><?php echo $i+3; ?>. </span><span class="bpf_value"><?php echo $sous_total_pubspec; ?></span></p>
        <?php endfor; ?>
        
        <?php
            $total_produits += $total_pubspec;
        ?>
    </div>
    
    <?php
        $sous_total = $financement["part"];
        $total_produits += $sous_total;
    ?>
    <p class="value"><?php echo $wpof->financement->get_term("part"); ?><span class="bpf_right">9. </span><span class="bpf_value"><?php echo $sous_total; ?></span></p>
    
    <?php
        $sous_total = $financement["opac"];
        $total_produits += $sous_total;
    ?>
    <p class="value"><?php echo $wpof->financement->get_term("opac"); ?><span class="bpf_right">10. </span><span class="bpf_value"><?php echo $sous_total; ?></span></p>
    
    <?php
        $sous_total = $financement["autres"];
        $total_produits += $sous_total;
    ?>
    <p class="value"><?php echo $wpof->financement->get_term("autres"); ?><span class="bpf_right">11. </span><span class="bpf_value"><?php echo $sous_total; ?></span></p>
    
    <p class="total"><?php _e("Total des produits réalisés au titre de la formation professionnelle (total des lignes 1 à 11)"); ?>
    <span class="bpf_value"><?php 
        echo $total_produits;
        if ($fin_total != array_sum($financement))
            echo "<br /><span class='erreur'>(+ ".($fin_total - $total_produits)." ".__("à réaffecter").")</span>";
    ?></span>
    </p>
    <?php
    
    // contrôle : liste par type de financement réellement indiqués dans les sessions (permet de vérifier les infos erronées)
    ?>
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-financement"><?php _e("Voir les erreurs d'affectation"); ?></span></p>
    <div class="blocHidden edit-data" id="liste-financement">
    <p><?php _e("Erreurs, manques à corriger"); ?></p>
    <table>
    <tr><td><?php _e("Mode de financement actuel"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Montant total"); ?></td></tr>
    <?php
        foreach($fin_erreur as $session => $fin) :
            foreach($fin as $client_id => $f)
            {
                if (!$wpof->financement->is_term($f)) : ?>
                    <tr>
                    <td><?php echo get_input_jpost($Client[$client_id], "financement", array('select' => 1)); ?></td>
                    <td><a href="<?php echo $SessionFormation[$session]->permalien; ?>"> <?php echo $session; ?></a>
                    </td>
                    <td><?php echo $Client[$client_id]->get_nom(); ?></td>
                    <td><?php echo get_tarif_formation($Client[$client_id]->tarif_total_chiffre); ?></td>
                    </tr>
                <?php endif;
            }
        endforeach; ?>
    <tr class="total"><td colspan="3"><?php _e("Total mauvaise affectation"); ?></td><td><?php echo $fin_total - array_sum($financement); ?></td></tr>
    <tr class="total"><td colspan="3"><?php _e("Total tout"); ?></td><td><?php echo $fin_total; ?></td></tr>
    </table>
    <p><?php _e("Modes de financement à priori corrects, pour vérification"); ?></p>
    <table>
    <tr><td><?php _e("Mode de financement actuel"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Montant total"); ?></td></tr>
    <?php
        foreach($fin_ctl as $session => $fin) :
            foreach($fin as $client_id => $f)
            {
                ?>
                <tr>
                <td><?php echo get_input_jpost($Client[$client_id], "financement", array('select' => 1)); ?></td>
                <td><a href="<?php echo $SessionFormation[$session]->permalien; ?>"> <?php echo $session; ?></a>
                </td>
                <td><?php echo $Client[$client_id]->get_nom(); ?></td>
                <td><?php echo get_tarif_formation($Client[$client_id]->tarif_total_chiffre); ?></td>
                </tr>
                <?php
            }
        endforeach; ?>
    </table>
    
    </div>
    
    </div>
    <?php
    return ob_get_clean();
}

// Bilan financier HT : charges de l'OF
function get_bpf_d()
{
    ob_start();
    return ob_get_clean();
}

// Personnes dispensant des heures de formation
function get_bpf_e()
{
    global $wpof;
    global $SessionFormation;
    
    $formateur_interne = array();
    $formateur_st = array();
    
    $session_ctl = array();
    
    foreach($SessionFormation as $session)
    {
        foreach($session->formateur as $f)
        {
            if (empty(get_user_meta($f, "sous_traitant", true)))
            {
                if (isset($formateur_interne[$f]))
                    $formateur_interne[$f] += $session->nb_heure_decimal;
                else
                    $formateur_interne[$f] = $session->nb_heure_decimal;
            }
            else
            {
                if (!isset($formateur_st[$f]))
                    $formateur_st[$f] = 0;
                    
                $formateur_st[$f] += $session->nb_heure_decimal;
            }
                
            if (empty($session_ctl[$f]))
                $session_ctl[$f] = array();
            $session_ctl[$f][$session->id] = $session->nb_heure_decimal;
        }
    }
    
    ob_start();
    ?>
    <h2><?php _e("E. Personnes dispensant des heures de formation"); ?></h2>
    <p><strong>Attention</strong> lorsque qu'une session est animée par plus d'un formateur, la durée de la formation est attribuée à chacun, pouvant entraîner des comptes multiples de temps ! À corriger à la main pour l'instant</p>
    <table>
    <tr><td></td><td><?php _e("Nombre de formateurs"); ?></td><td><?php _e("Nombre d'heures de formation"); ?></td></tr>
    <tr>
    <td><?php _e("Personnes de votre organisme dispensant des heures de formation"); ?></td>
    <td><?php echo count($formateur_interne); ?></td>
    <td><?php echo array_sum($formateur_interne); ?></td>
    </tr>
    <tr>
    <td><?php _e("Personnes extérieures à votre organisme dispensant des heures de formation dans le cadre de contrats de sous-traitance"); ?></td>
    <td><?php echo count($formateur_st); ?></td>
    <td><?php echo array_sum($formateur_st); ?></td>
    </tr>
    </table>
    
    <?php
    
    // contrôle : liste des formateurs interne et externe
    ?>
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-formateur"><?php _e("Voir les formateurs"); ?></span></p>
    <div class="blocHidden" id="liste-formateur">
    <p><?php _e("Définissez les formateurs sous-traitants depuis la page"); ?> <a href="/equipe-pedagogique/"><?php _e("Équipe pédagogique"); ?></a></p>
    <table>
    <tr><td><?php _e("Formateur⋅trice"); ?></td><td><?php _e("Session (nb h)"); ?></td><td><?php _e("Nombre d'heures"); ?></td></tr>
    <?php foreach($formateur_interne as $user_id => $nb_heures) : ?>
    <tr><td><?php echo get_displayname($user_id, false); ?> <strong>(<?php _e("interne"); ?>)</strong></td>
    <td>
        <?php
        $liste_session_heure = array();
        foreach ($session_ctl[$user_id] as $sid => $nbh)
        {
            $link = "<a href=".$SessionFormation[$sid]->permalien.">";
            $link .= ($SessionFormation[$sid]->numero != "") ? $SessionFormation[$sid]->numero : $sid;
            $link .= "</a> ($nbh)";
            $liste_session_heure[] = $link;
        }
        echo join("<br />", $liste_session_heure);
        ?>
    </td>
    <td><?php echo $nb_heures; ?></td></tr>
    <?php endforeach; ?>
    <?php foreach($formateur_st as $user_id => $nb_heures) : ?>
    <tr><td><?php echo get_displayname($user_id, false); ?> <em>(<?php _e("externe"); ?>)</em></td>
    <td>
        <?php
        $liste_session_heure = array();
        foreach ($session_ctl[$user_id] as $sid => $nbh)
        {
            $link = "<a href=".$SessionFormation[$sid]->permalien.">";
            $link .= ($SessionFormation[$sid]->numero != "") ? $SessionFormation[$sid]->numero : $sid;
            $link .= "</a> ($nbh)";
            $liste_session_heure[] = $link;
        }
        echo join("<br />", $liste_session_heure);
        ?>
    </td>
    <td><?php echo $nb_heures; ?></td></tr>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

// Type de stagiaire de l'OF
function get_bpf_f1()
{
    global $wpof;
    global $SessionFormation, $Client, $SessionStagiaire;
    
    // tableau pour remonter les durées nulles
    $erreur_duree = array();
    // tableau pour les erreurs de statut
    $erreur_statut = array();
    $ctl_statut = array(); // pour lister précisément les statuts à priori bien renseignés
    $total_heure_erreur = 0;
    $total_stagiaire_erreur = 0;
    
    $statut_stagiaire = array();
    foreach(array_keys($wpof->statut_stagiaire->term) as $k)
        $statut_stagiaire[$k] = array();
    $total_stagiaires = $total_heures = 0;
    
    foreach($Client as $client)
    {
        if ($client->financement != "opac")
        {
            $session = $SessionFormation[$client->session_formation_id];
            $session->init_stagiaires();
            
            foreach($client->stagiaires as $user_id)
            {
                $stagiaire = new SessionStagiaire($session->id, $user_id);
                if (!empty($stagiaire->statut_stagiaire) && $wpof->statut_stagiaire->is_term($stagiaire->statut_stagiaire))
                {
                    $statut_stagiaire[$stagiaire->statut_stagiaire][] = (float) $stagiaire->nb_heure_estime_decimal;
                    $ctl_statut[] = array
                        (
                            'statut' => get_input_jpost($stagiaire, "statut_stagiaire", array('select' => 1, 'class' => 'flexrow')),
                            'session_id' => $session->get_id_link(),
                            'client' => $client->get_nom(),
                            'stagiaire' => $stagiaire->get_displayname(),
                            'duree' => $stagiaire->nb_heure_estime_decimal
                        );
                }
                else
                {
                    $erreur_statut[] = array
                        (
                            'statut' => get_input_jpost($stagiaire, "statut_stagiaire", array('select' => 1, 'class' => 'flexrow')),
                            'session_id' => $session->get_id_link(),
                            'client' => $client->get_nom(),
                            'stagiaire' => $stagiaire->get_displayname(),
                            'duree' => $stagiaire->nb_heure_estime_decimal
                        );
                    $total_heure_erreur += (float) $stagiaire->nb_heure_estime_decimal;
                    $total_stagiaire_erreur ++;
                }
                $total_stagiaires++;
                $total_heures += $stagiaire->nb_heure_estime_decimal;
                
                if ($stagiaire->nb_heure_estime_decimal == 0)
                {
                    if (empty($erreur_duree[$session->id]))
                        $erreur_duree[$session->id] = array();
                    $erreur_duree[$session->id][] = $stagiaire->get_displayname();
                }
            }
        }
    }
    
    $total_stagiaires_affiche = $total_heures_affiche = 0;
    ob_start();
    ?>
    <h2><?php _e("F1. Type de stagiaires de l'organisme"); ?></h2>
    
    <table>
    <tr><td></td><td><?php _e("Nombre de stagiaires ou apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <?php foreach(array_keys($wpof->statut_stagiaire->term) as $k) : ?>
    <tr>
    <td><?php echo $wpof->statut_stagiaire->get_term($k); ?></td>
    <td><?php $total_stagiaires_affiche += count($statut_stagiaire[$k]); echo count($statut_stagiaire[$k]); ?></td>
    <td><?php $total_heures_affiche += array_sum($statut_stagiaire[$k]); echo array_sum($statut_stagiaire[$k]); ?></td>
    </tr>
    <?php endforeach; ?>
    <tr class="total">
    <td><?php _e("Total"); ?></td><td><?php echo $total_stagiaires; ?></td><td><?php echo $total_heures; ?></td></tr>
    <?php if ($total_heures != $total_heures_affiche) : ?>
    <tr class="erreur"><td><?php _e("Dont total des mauvaises affectations (à corriger)"); ?></td><td><?php echo $total_stagiaires - $total_stagiaires_affiche; ?></td><td><?php echo $total_heures - $total_heures_affiche; ?></td></tr>
    <?php endif; ?>
    </table>
    
    <div class="bpf_ctl">
    <?php if (!empty($erreur_duree)) : ?>
    <p class="bg-erreur"><span class="openButton" data-id="erreur-duree-stagiaire"><?php _e("Attention ! certains stagiaires ont un nombre d'heures égal à 0"); ?></span></p>
    <div class="blocHidden" id="erreur-duree-stagiaire">
    <table>
    <?php foreach ($erreur_duree as $sid => $stag_zero) : ?>
    <tr><td><?php echo join(", ", $stag_zero); ?></td>
    <td><a href="<?php echo $SessionFormation[$sid]->permalien; ?>"><?php echo ($SessionFormation[$sid]->numero != "") ? $SessionFormation[$sid]->numero : $sid; ?></td>
    </tr>
    <?php endforeach; ?>
    </table>
    </div>
    <?php endif; ?>
    <p><span class="openButton" data-id="liste-type-stagiaire"><?php _e("Voir les types de stagiaire mal affectés"); ?></span></p>
    <div class="blocHidden edit-data" id="liste-type-stagiaire">
    <?php if (count($erreur_statut) > 0) : ?>
    <p><?php _e("Définissez les types de stagiaire ici"); ?>.</p>
    <table>
    <tr><td><?php _e("Statut invalide, ancien ou non défini"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Stagiaire"); ?></td><td><?php _e("Durée de formation"); ?></td></tr>
    <?php foreach($erreur_statut as $row) : ?>
    <tr>
        <?php foreach($row as $val) : ?>
        <td><?php echo $val; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </table>
    <?php endif; ?>
    <p><?php _e("Statuts des stagiaires déjà affectés (pour vérification)"); ?></p>
    <table>
    <tr><td><?php _e("Statut"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Stagiaire"); ?></td><td><?php _e("Durée de formation"); ?></td></tr>
    <?php foreach($ctl_statut as $row) : ?>
    <tr>
        <?php foreach($row as $val) : ?>
        <td><?php echo $val; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

// dont activité est sous-traitée de l'OF (formateur sous-traitant)
function get_bpf_f2()
{
    global $wpof;
    global $SessionFormation, $Client;
    
    $session_st = array();
    $total_stagiaires = $total_heures = 0;
    
    $session_st_ctl = array();
    
    foreach($SessionFormation as $session)
    {
        foreach($session->formateur as $f)
        {
            if (!empty(get_user_meta($f, "sous_traitant", true)))
            {
                // on fait un classement par session pour une vérif ultérieure
                if (!isset($session_st[$session->id]))
                    $session_st[$session->id] = array("stagiaire" => 0, "heure" => 0);
                foreach($session->clients as $cid)
                {
                    if ($Client[$cid]->financement == "opac") continue;
                    
                    foreach($Client[$cid]->stagiaires as $sid)
                    {
                        $stagiaire = new SessionStagiaire($session->id, $sid);
                        $session_st[$session->id]["heure"] += $stagiaire->nb_heure_estime_decimal;
                        $session_st[$session->id]["stagiaire"] ++;
                        
                        $session_st_ctl[] = array('session_id' => $session->get_id_link(), 'formateur' => get_displayname($f, false), 'client' => $Client[$cid]->get_nom(), 'stagiaire' => $stagiaire->get_displayname(), 'duree' => $stagiaire->nb_heure_estime_decimal);
                    }
                }
                $total_stagiaires += $session_st[$session->id]["stagiaire"];
                $total_heures += $session_st[$session->id]["heure"];
            }
        }
    }

    ob_start();
    ?>
    <h2><?php _e("F2. Dont activité sous-traitée"); ?></h2>
    <table>
    <tr>
    <td></td><td><?php _e("Nombre de stagiaires ou d'apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <tr>
    <td><?php _e("Stagiaire ou apprentis dont l'action a été confiée par votre organisme à un autre organisme"); ?></td>
    <td><?php echo $total_stagiaires; ?></td>
    <td><?php echo $total_heures; ?></td>
    </tr>
    </table>
    
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-sous-traitant"><?php _e("Voir par session"); ?></span></p>
    <div class="blocHidden" id="liste-sous-traitant">
    <table>
    <tr><td><?php _e("Session ID"); ?></td><td><?php _e("Formateur⋅trice"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Stagiaire"); ?></td><td><?php _e("Durée"); ?></td></tr>
    <?php foreach($session_st_ctl as $row) : ?>
    <tr>
        <?php foreach($row as $val) : ?>
        <td><?php echo $val; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

// Objectif général des prestations dispensées
function get_bpf_f3()
{
    global $wpof;
    global $SessionFormation, $Client;
    
    $total_stagiaires = $total_heures = 0;
    $nature_formation = array();
    $ctl_nature = array();
    $erreur_nature = array();
    foreach(array_keys($wpof->nature_formation->term) as $k)
        $nature_formation[$k] = array('duree' => 0, 'stagiaires' => 0);
    
    foreach($Client as $client)
    {
        if ($client->financement != "opac")
        {
            $session = $SessionFormation[$client->session_formation_id];
            $client_nb_stagiaires = count($client->stagiaires);
            $client_nb_heure_stagiaire = $client->get_nb_heure_stagiaire();
                
            $total_stagiaires += $client_nb_stagiaires;
            $total_heures += $client_nb_heure_stagiaire;
            
            if (!empty($client->nature_formation) && $wpof->nature_formation->is_term($client->nature_formation))
            {
                $ctl_nature[] = array
                    (
                        'nature' => get_input_jpost($client, "nature_formation", array('select' => 1, 'class' => 'flexrow')),
                        'session_id' => $session->get_id_link(),
                        'client' => $client->get_nom(),
                        'stagiaires' => $client_nb_stagiaires,
                        'duree' => $client_nb_heure_stagiaire
                    );
                $nature_formation[$client->nature_formation]["stagiaires"] += $client_nb_stagiaires;
                $nature_formation[$client->nature_formation]["duree"] += $client_nb_heure_stagiaire;
            }
            else
            {
                $erreur_nature[] = array
                    (
                        'nature' => get_input_jpost($client, "nature_formation", array('select' => 1, 'class' => 'flexrow')),
                        'session_id' => $session->get_id_link(),
                        'client' => $client->get_nom(),
                        'stagiaires' => count($client->stagiaires),
                        'duree' => $client->get_nb_heure_stagiaire()
                    );
            }
        }
    }
  
    $total_stagiaires_affiche = $total_heures_affiche = 0;
    ob_start();
    ?>
    <h2><?php _e("F3. Objectif général des prestations dispensées"); ?></h2>
    <table>
    <tr>
    <td></td><td><?php _e("Nombre de stagiaires ou d'apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <?php foreach(array_keys($wpof->nature_formation->term) as $k) : ?>
    <tr>
    <td><?php echo $wpof->nature_formation->get_term($k); ?></td>
    <td><?php $total_stagiaires_affiche += $nature_formation[$k]["stagiaires"]; echo $nature_formation[$k]["stagiaires"]; ?></td>
    <td><?php $total_heures_affiche += $nature_formation[$k]["duree"]; echo $nature_formation[$k]["duree"]; ?></td>
    </tr>
    <?php endforeach; ?>
    <tr class="total">
    <td><?php _e("Total"); ?></td><td><?php echo $total_stagiaires; ?></td><td><?php echo $total_heures; ?></td></tr>
    <?php if (!empty($erreur_nature)) : ?>
    <tr class="erreur">
    <td><?php _e("Dont total des mauvaises affectations (à corriger)"); ?></td><td><?php echo $total_stagiaires - $total_stagiaires_affiche; ?></td><td><?php echo $total_heures - $total_heures_affiche; ?></td></tr>
    <?php endif; ?>
    </table>
    
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-nature"><?php _e("Voir également les mauvaises affectations"); ?></span></p>
    <div class="blocHidden edit-data" id="liste-nature">
    <p><?php _e("Corrigez les objectifs de prestation (nature de formation) ici"); ?>.</p>
    <table>
    <tr><td><?php _e("Objectif de prestation invalide, ancien ou non défini"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Nb stagiaires"); ?></td><td><?php _e("Nb heures × stagiaires"); ?></td></tr>
    <?php foreach($erreur_nature as $row) : ?>
    <tr>
        <?php foreach($row as $val) : ?>
        <td><?php echo $val; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </table>
    <p><?php _e("Pour information les objectifs de prestation (nature de formation) à priori correctement renseignés"); ?>.</p>
    <table>
    <tr><td><?php _e("Objectif de prestation"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Nb stagiaires"); ?></td><td><?php _e("Nb heures × stagiaires"); ?></td></tr>
    <?php foreach($ctl_nature as $row) : ?>
    <tr>
        <?php foreach($row as $val) : ?>
        <td><?php echo $val; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

// Spécialités de formation
function get_bpf_f4($tri = "heures")
{
    global $wpof;
    global $SessionFormation, $Client;
    
    $total_stagiaires = $total_heures = 0;
    $specialites = array();
    
    // pour vérification
    $ctl_spec = array();
    $erreur_spec = array();
    
    foreach($Client as $client)
    {
        if ($client->financement != "opac")
        {
            $session = $SessionFormation[$client->session_formation_id];
            $client_nb_stagiaires = count($client->stagiaires);
            $client_nb_heure_stagiaire = $client->get_nb_heure_stagiaire();
                
            $total_stagiaires += $client_nb_stagiaires;
            $total_heures += $client_nb_heure_stagiaire;
            
            if (!empty($session->specialite) && $wpof->specialite->is_term($session->specialite))
            {
                $ctl_spec[] = array
                    (
                        'specialite' => get_input_jpost($session, "specialite", array('select' => 1, 'class' => 'flexrow')),
                        'session_id' => $session->get_id_link(),
                        'client' => $client->get_nom(),
                        'stagiaires' => $client_nb_stagiaires,
                        'duree' => $client_nb_heure_stagiaire
                    );
                if (!isset($specialites[$session->specialite]))
                    $specialites[$session->specialite] = array('duree' => 0, 'stagiaires' => 0);
                $specialites[$session->specialite]["stagiaires"] += $client_nb_stagiaires;
                $specialites[$session->specialite]["duree"] += $client_nb_heure_stagiaire;
            }
            else
            {
                $erreur_spec[] = array
                    (
                        'specialite' => get_input_jpost($session, "specialite", array('select' => 1, 'class' => 'flexrow')),
                        'session_id' => $session->get_id_link(),
                        'client' => $client->get_nom(),
                        'stagiaires' => count($client->stagiaires),
                        'duree' => $client->get_nb_heure_stagiaire()
                    );
            }
        }
    }
    
    // on trie ensuite le tableau $specialites par stagiaires et par heures
    $spec_stagiaires = array();
    $spec_heures = array();
    
    $nb_spec = 5;
    
    foreach($specialites as $k => $val)
    {
        $spec_stagiaires[$k] = $val['stagiaires'];
        $spec_heures[$k] = $val['duree'];
    }
    arsort($spec_stagiaires);
    arsort($spec_heures);
    //ksort($spec_ctl);
    
    $total_stagiaires_affiche = array_sum($spec_stagiaires);
    $total_heures_affiche = array_sum($spec_heures);
    
    $spec_ref = ($tri == "stagiaires") ? $spec_stagiaires : $spec_heures;
    
    ob_start();
    ?>
    <h2><?php _e("F4. Spécialités de formation"); ?></h2>
    <p class="value"><?php _e("Cinq principales spécialités de formation"); ?></p>
    <table>
    <tr>
    <td></td><td><?php _e("Nombre de stagiaires ou d'apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <?php foreach(array_keys(array_slice($spec_ref, 0, $nb_spec, true)) as $k) : ?>
    <tr>
    <td><?php echo $wpof->specialite->get_term($k); ?></td>
    <td><?php echo $spec_stagiaires[$k]; ?></td>
    <td><?php echo $spec_heures[$k]; ?></td>
    </tr>
    <?php endforeach; ?>
    <tr>
    <td><?php _e("Autres spécialités"); ?></td>
    <td><?php echo array_sum(array_intersect_key($spec_stagiaires, array_slice($spec_ref, $nb_spec, NULL, true))); ?></td>
    <td><?php echo array_sum(array_intersect_key($spec_heures, array_slice($spec_ref, $nb_spec, NULL, true))); ?></td>
    </tr>
    <tr class="total">
    <td><?php _e("Total"); ?></td><td><?php echo $total_stagiaires; ?></td><td><?php echo $total_heures; ?></td></tr>
    <?php if (!empty($erreur_spec)) : ?>
    <tr class="erreur">
    <td><?php _e("Dont total des mauvaises affectations (à corriger)"); ?></td><td><?php echo $total_stagiaires - $total_stagiaires_affiche; ?></td><td><?php echo $total_heures - $total_heures_affiche; ?></td></tr>
    <?php endif; ?>
    </table>
    
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-specialite"><?php _e("Voir les autres spécialités et le détail par session"); ?></span>
    <?php if (!empty($erreur_spec)) : ?>
        <span class="erreur"><?php _e("Des formations et/ou des sessions n'ont pas de spécialité définie ou inconnue !"); ?></span>
    <?php endif; ?>
    </p>
    <div class="blocHidden edit-data" id="liste-specialite">
    <p><?php echo __("Pour les formations du catalogue, les spécialités peuvent être affectées depuis la page")." <a href='/formations/'>".__("Formations")."</a>"; ?>.</p>
    <h3><?php _e("Détails des autres spécialités"); ?></h3>
    <table>
    <tr>
    <td></td><td><?php _e("Nombre de stagiaires ou d'apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <?php foreach(array_keys(array_slice($spec_ref, $nb_spec, NULL, true)) as $k) : ?>
    <tr>
    <td><?php echo $wpof->specialite->get_term($k); ?></td>
    <td><?php echo $spec_stagiaires[$k]; ?></td>
    <td><?php echo $spec_heures[$k]; ?></td>
    </tr>
    <?php endforeach; ?>
    </table>
    
    <h3><?php _e("Détail par session"); ?></h3>
    <p><?php _e("Corrigez les spécialités inconnues ou manquantes ici. <strong>Attention</strong> la spécialité est affectée à la session, inutile de la momdifier pour chaque client d'une même session !"); ?>.</p>
    <table>
    <tr><td><?php _e("Spécialité"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Nb stagiaires"); ?></td><td><?php _e("Nb heures × stagiaires"); ?></td></tr>
    <?php foreach($erreur_spec as $row) : ?>
    <tr>
        <?php foreach($row as $val) : ?>
        <td><?php echo $val; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </table>
    <p><?php _e("Pour information les spécialités à priori correctement définies. <strong>Attention</strong> la spécialité est affectée à la session, inutile de la momdifier pour chaque client d'une même session !"); ?>.</p>
    <table>
    <tr><td><?php _e("Spécialité"); ?></td><td><?php _e("Session ID"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Nb stagiaires"); ?></td><td><?php _e("Nb heures × stagiaires"); ?></td></tr>
    <?php foreach($ctl_spec as $row) : ?>
    <tr>
        <?php foreach($row as $val) : ?>
        <td><?php echo $val; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

// Stagiaires dont la formation a été confiée à votre OF par un autre OF (votre OF est sous-traitant d'un autre OF)
function get_bpf_g()
{
    global $SessionFormation, $Client;
    
    $total_stagiaires = $total_heures = 0;
    $ctl_sous_traitance = array();
    
    foreach($Client as $client)
    {
        $session = $SessionFormation[$client->session_formation_id];
        if ($client->financement == "opac")
        {
            $total_stagiaires += $client->nb_stagiaires;
            $total_heures += $client->nb_heures_stagiaires;
            $ctl_sous_traitance[] = array
                (
                    'session_id' => $session->get_id_link(),
                    'client' => $client->get_nom(),
                    'stagiaires' => $client->nb_stagiaires,
                    'duree' => $client->nb_heures_stagiaires
                );
        }
    }
    
    /*
    foreach($SessionFormation as $s)
    {
        $nb_stagiaires = $nb_heures = 0;
        if ($s->type_index == "sous_traitance")
        {
            if (count($s->inscrits) > 0)
            {
                $s->init_stagiaires();
                $nb_stagiaires += count($s->stagiaires);
                foreach($s->stagiaires as $stagiaire)
                    $nb_heures += $stagiaire->nb_heure_decimal;
            }
            else
            {
                $nb_stagiaires = $s->st_nb_stagiaire;
                $nb_heures = $s->st_nb_stagiaire * $s->nb_heure_decimal;
            }
            
            $sous_traitance[$s->id] = array("stagiaire" => $nb_stagiaires, "heure" => $nb_heures);
        }
        $total_heures += $nb_heures;
        $total_stagiaires += $nb_stagiaires;
    }*/
    
    ob_start();
    ?>
    <h2><?php _e("G. Stagiaires dont la formation a été confiée à votre organisme par un autre OF"); ?></h2>
    <table>
    <tr>
    <td></td><td><?php _e("Nombre de stagiaires ou d'apprentis"); ?></td><td><?php _e("Nombre d'heures de formation suivies par les stagiaires et apprentis"); ?></td></tr>
    <tr>
    <td><?php _e("Formations confiées à votre organisme par un autre organisme de formation"); ?></td>
    <td><?php echo $total_stagiaires; ?></td>
    <td><?php echo $total_heures; ?></td>
    </tr>
    </table>
    
    <div class="bpf_ctl">
    <p><span class="openButton" data-id="liste-sous-traite"><?php _e("Voir par session"); ?></span></p>
    <div class="blocHidden" id="liste-sous-traite">
    <table>
    <tr><td><?php _e("Session ID"); ?></td><td><?php _e("Client"); ?></td><td><?php _e("Nb stagiaires"); ?></td><td><?php _e("Nb heures × stagiaires"); ?></td></tr>
    <?php foreach($ctl_sous_traitance as $row) : ?>
    <tr>
        <?php foreach($row as $val) : ?>
        <td><?php echo $val; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </table>
    </div>
    </div>
    <?php
    return ob_get_clean();
}

?>
