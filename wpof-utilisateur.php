<?php
/*
 * wpof-utilisateur.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

$role_nom = array
(
    'um_stagiaire' => __('Stagiaire'),
    'um_formateur-trice' => __('Formateur⋅trice'),
    'um_responsable' => __('Responsable de formation'),
    'admin' => __('Administrateur⋅trice'),
);

function shortcode_show_liste_formateur( $atts )
{
    ob_start();
    // Attributes
    $atts = shortcode_atts(
        array
            (
            'cat' => 'all',
            'tag' => 'all',
            ),
	$atts
	);
	
    the_liste_formateur();
    
    return ob_get_clean();
    //debug_info($formateurs, "formateurs");
}
add_shortcode( 'liste_formateurs', 'shortcode_show_liste_formateur' );


function the_liste_formateur($atts = array(), $echo = true)
{
    $formateurs = get_formateurs($atts);
    $role = wpof_get_role(get_current_user_id());
    
    ob_start();
    ?>
    <ul class="list list_formateur">
    <?php foreach($formateurs as $f) : ?>
        <li><?php echo $f->get_displayname($f->profil_public); ?><?php if ($f->marque != "") echo " (".$f->marque.")"; ?></li>
    <?php endforeach; ?>
    </ul>
    <?php
    
    if ($echo)
        echo ob_get_clean();
    else
        return ob_get_clean();
}

function get_user_check_jpost($name, $label = "", $user_id)
{
    $html = "<div class='input_check_jpost'>";
    $input_id = $name.rand();
    
    if ($label != "")
        $html .= "<label class='input_check_jpost_label' for='$input_id'>$label</label>";
    $html .= "<input class='input_check_jpost_value' type='checkbox' id='$input_id' name='$name' value='1' ".checked(get_user_meta($user_id, $name, true), 1, false)." />";
    $html .= "<input type='hidden' name='user_id' value='{$user_id}' />";
    $html .= "</div>";
    
    return $html;
}

add_action( 'wp_ajax_update_user_value', 'update_user_value' );
function update_user_value()
{
    $value = ($_POST['checked'] == "true") ? 1 : 0;
    update_user_meta($_POST['user_id'], $_POST['meta'], $value);
    
    echo $_POST['checked'];
    echo $value;
    
    die();
}

function get_formateurs($atts = array(), $single = false)
{
    global $wpof;
    
    $roles = array('um_formateur-trice', 'um_responsable');
    if (isset($atts['role']))
        $roles = (is_array($atts['role'])) ? $atts['role'] : explode(',', str_replace(' ', '', $atts['role']));
    
    if (in_array('only', array_keys($atts)))
    {
        foreach($atts['only'] as $id)
            $formateur_users[] = get_user_by("id", $id);
    }
    else
    {
        if (!$single)
            $formateur_users = get_users( array($atts, 'role__in' => $roles));
        else
            $formateur_users[0] = get_user( $atts['p'] );
    }
    
    $formateurs = array();
    
    foreach ($formateur_users as $fu)
        $formateurs[$fu->ID] = new Formateur($fu->ID);
    return $formateurs;
}


/*
 * Méta-fonction gérant l'affichage des tableaux de bord dans les profils utilisateurs
 */
function board_profil($user_id = null)
{
    $current_user_id = get_current_user_id();
    $user_id = ($user_id) ? $user_id : $current_user_id;

    $formateur = new Formateur($user_id);
    $role = $formateur->role;
    
    if ($user_id == $current_user_id)
        $formateur->board_profil();
    else
        $formateur->vue_publique();
}

/*
 * La méthode get_role d'Ultimate member renvoie le rôle WP s'il existe et pas le rôle UM
 * Cette fonction vise à palier à ce manque en ne renvoyant QUE les rôles utilisés dans le plugin
 */
function wpof_get_role($user_id)
{
    if ($user_id == 0)
        return false;

    if (user_can($user_id, "um_formateur-trice"))
        return "um_formateur-trice";

    if (user_can($user_id, "um_responsable"))
        return "um_responsable";
    
    if (user_can($user_id, 'manage_options'))
        return "admin";
}

/*
 * Ajouter un utilisateur
 * Analyse des données du formulaire
 */
add_action( 'wp_ajax_add_compte', 'add_compte' );
function add_compte()
{
    global $ultimatemember;
    
    $reponse = array('message' => '', 'html' => '');
    
    $user = array();
    $session = null;
    $session_id = 0;
    
    // Analyse des données
    if (isset($_POST['data']))
    {
        $data = json_decode(html_entity_decode(stripslashes($_POST['data'])));
        $user = $data->allusers;
    }
    else
    {
        $user[0] = (object) array();
        foreach(array('genre', 'firstname', 'lastname', 'email', 'role') as $field)
            $user[0]->$field = (isset($_POST[$field])) ? $_POST[$field] : "";
    }
        
    if ( isset($_FILES["file"]) && $_FILES["file"]["type"] == 'text/csv')
    {
        if ($_FILES["file"]["error"] > 0)
        {
            $reponse['message'] .= "<span class='erreur'>".__("Erreur de fichier CSV")." ".$_FILES["file"]["error"]."</span>";
        }
        else
        {
            $fhandle = fopen($_FILES["file"]["tmp_name"], "r");
            
            // première ligne ignorée
            $line = fgetcsv($fhandle, 0, ';');
            
            $i = count($user);
            while ($line = fgetcsv($fhandle, 0, ';'))
            {
                $line = array_map("trim", $line);
                $line = array_pad($line, 5, "");
                $line[0] = strtoupper($line[0]);
                $line[4] = strtoupper($line[4]);
                $line[1] = ucfirst($line[1]);
                $line[2] = ucfirst($line[2]);
                
                if (count($line) < 3 || $line[0] == "" || $line[1] == "" || $line[2] == "" || !in_array($line[0], array("F", "H")))
                {
                    $reponse['message'] .= "<p><span class='erreur'>".__("Informations manquantes ou erronées")." ".join(";", $line)."</span></p>";
                    continue;
                }
                $user[$i] = new stdClass();
                $user[$i]->genre = ($line[0] == "H") ? "Monsieur" : "Madame";
                $user[$i]->firstname = $line[1];
                $user[$i]->lastname = $line[2];
                $user[$i]->email = $line[3];
                switch ($line[4])
                {
                    case "R":
                        $user[$i]->role = "um_responsable";
                        break;
                    case "F":
                        $user[$i]->role = "um_formateur-trice";
                        break;
                    case "S":
                    default:
                        $user[$i]->role = "um_stagiaire";
                        break;
                }
                $i++;
            }
            fclose($fhandle);
        }
    }
    foreach ($user as $u)
    {
        if ($u->firstname != "" && $u->lastname != "" && $u->email != "" )
        {
            $reponse['message'] .= "<p>{$u->firstname} {$u->lastname} ({$u->email}) ";
            $userdata = array
            (
                'first_name' => $u->firstname,
                'last_name' => $u->lastname,
                'user_email' => $u->email,
                'user_login' => sanitize_user( strtolower( str_replace( " ", ".", $u->firstname . " " . $u->lastname ) ) ),
                'user_pass' => NULL
            );
            $user_id = wp_insert_user($userdata);
            if (is_wp_error($user_id))
            {
                $reponse['message'] .= "<span class='erreur'>".$user_id->get_error_message()."</span>";
            }
            else
            {
                if (champ_additionnel("genre_stagiaire"))
                    add_user_meta($user_id, 'genre', $u->genre);
                $user_obj = new WP_User($user_id);
                $user_obj->set_role($u->role);
                    
                //UM()->mail()->send($u->email, 'welcome_email'); // cette action est déjà effectuée par UM
                $reponse['message'] .= "<span class='succes'>".__("compte créé")."</span>";
            }
            $reponse['message'] .= "</p>";
        }
    }
    if ($reponse['message'] == "")
    {
        $reponse['message'] .= "<span class='erreur'>".__("Saisissez un prénom et un nom pour inscrire un⋅e stagiaire")."</span>";
        $reponse['erreur'] = $reponse['message'];
    }
    else
    {
        $reponse['succes'] = $reponse['message'];
        if ($session)
            $reponse['html'] = $session->get_tabs_clients();
    }
    

    echo json_encode($reponse);
    
    die();
}

/*
 * Ajouter un utilisateur
 * Affichage du formulaire
 * $session_id : si > 0 alors les stagiaires sont également inscrits à cette session
 */
function get_add_compte_form($session_id = 0, $client_id = 0)
{
    $data_genre = "";
    $data_selectrole = "";
    
    ob_start();
    ?>
    <div id="add_compte">
        <?php echo get_icone_aide("utilisateur_ajout_compte", __("Créer des comptes")); ?>
        <div id="add_compte_form" class="dynamic_form notif-modif">
        
        <table class="add-compte">
        <tbody>
        
        <tr id='0_new-compte' data-num='0' class='new-compte'>
        <?php if (champ_additionnel("genre_stagiaire")) : 
        $data_genre = "data-genre='1'"; ?>
        <td><select name="0_genre"><option value="Madame">Madame</option><option value="Monsieur">Monsieur</option></select></td>
        <?php endif; ?>
        <td><input type="text" name="0_firstname" placeholder="<?php _e("Prénom"); ?>" /></td>
        <td><input type="text" name="0_lastname" placeholder="<?php _e("Nom"); ?>" /></td>
        <td><input type="text" name="0_email" placeholder="<?php _e("Courriel"); ?>" /></td>
        <?php
        if ($session_id == 0) :
            global $role_nom;
            $role_list = $role_nom;
            unset($role_list['admin']);
            unset($role_list['um_stagiaire']);
            $select_role = select_by_list($role_list, "NUM_role", "um_formateur-trice");
        ?>
        <td><?php echo str_replace("NUM", "0", $select_role); ?></td>
        <?php
            // on doit passer la liste des rôles seulement dans le cas où $session_id vaut 0 (c'est-à-dire si cette boîte est affichée hors d'une session)
            $data_selectrole = "data-selectrole='".json_encode($role_list)."'";
            endif;
        ?>
        <td><span class="dashicons dashicons-plus-alt icone-bouton" <?php echo $data_selectrole; ?> id="plus"></span></td>
        </tr>
        
        </tbody>
        </table>
        
        <input type='hidden' name='action' value='add_compte' />
        
        <p class="bouton submit" id="add-compte-bouton"><?php _e("Ajouter"); ?></p>
        <p class="message"></p>
        </div>
        
        <?php //if (in_array(wpof_get_role(get_current_user_id()), array("um_responsable", "admin"))) : ?>
        <?php //if (in_array(wpof_get_role(get_current_user_id()), array("admin"))) : ?>
        <?php if (0) : ?>
            <div id="add_compte_csv">
            <form id="add-stagiaire-csv-form" method="POST" name="add-stagiaire-csv-form" enctype="multipart/form-data">
                <label for="add-stagiaire-csv"><?php _e("Listing au format CSV avec comme colonnes : genre (F ou H) ; prenom ; nom ; email ; role (R ou F ou S). La première ligne (souvent utilisée pour caractériser les colonnes) est <strong>ignorée</strong>"); ?></label><br />
                <input id="add-stagiaire-csv" name="add-stagiaire-csv" type="file" accept=".csv" />
                <input class="ajax-save-file bouton" data-action="add_stagiaire" data-sessionid="<?php echo $session_id; ?>" data-message-parent="add_compte_form" type="button submit" value="<?php _e("Traiter le CSV"); ?>" />
            </form>
            <p class="message"></p>
            </div>
        <?php endif; ?>
        
    </div>
    <?php
    return ob_get_clean();
}

// Initialise un tableau global classé par utilisateur avec la liste de ses formations
$anime_formation = array();
function init_anime_formation()
{
    global $anime_formation;
    if (!empty($anime_formation))
        return;
    $formations = get_formations();
    if (!empty($formations))
    {
        foreach($formations as $f)
            foreach($f->formateur as $user_id)
                $anime_formation[$user_id][] = '<a title="'.$f->titre.'" href="'.$f->permalien.'">'.$f->id.'</a>';
    }
}

// Renvoie un tableau classé par utilisateur avec la liste de ses sessions
$anime_session = array();
function init_anime_session()
{
    global $anime_session;
    if (!empty($anime_session))
        return;
    $sessions = get_formation_sessions();
    if (!empty($sessions))
    {
        foreach($sessions as $s)
            foreach($s->formateur as $user_id)
                // strip_tags nettoie les balises <sup> lorsque la première date est le 1er du mois
                $anime_session[$user_id][] = '<a title="'.strip_tags($s->titre_session).'" href="'.$s->permalien.'">'.$s->id.'</a>';
    }
}

function get_gestion_comptes()
{
    $role = wpof_get_role(get_current_user_id());
    $formateurs = get_formateurs();
    
    ob_start();
    ?>
    <table class="gestion_formateurs opaga opaga2 datatable">
    <thead>
    <?php echo $formateurs[0]->get_formateur_control_head(); ?>
    </thead>
    
    <?php
    foreach($formateurs as $f)
        echo $f->get_formateur_control();
    ?>
    </table>
    <?php
    return ob_get_clean();
}

add_filter('um_profile_tabs', 'add_of_profile_tab', 1000 );
function add_of_profile_tab( $tabs )
{
    $tabs['oftab'] = array
    (
        'name' => 'Formation',
        'icon' => 'um-faicon-graduation-cap',
    );
    return $tabs;
}

add_filter('um_login_redirect_url', 'opaga_login_redirect_url', 10, 2);
function opaga_login_redirect_url($url, $id)
{
    global $wpof;
    $user = get_user_by('id', $id);
    return home_url()."/".$wpof->url_user."/".$user->user_login;
}

add_action('um_on_login_before_redirect', 'opaga_user_login', 10, 1);
function opaga_user_login($args)
{
    global $wpof;
    $args['redirect_to'] = home_url()."/".$wpof->url_user."/";
    $args['after_login'] = 'redirect_url';
}

/* Then we just have to add content to that tab using this action */

add_action('um_profile_content_oftab_default', 'um_profile_content_oftab_default');
function um_profile_content_oftab_default( $args )
{
    board_profil(um_profile_id());
}


//if (get_current_user_id() > 0)
add_action('wp_body_open', 'show_profile_box');

function show_profile_box()
{
    $user_id = get_current_user_id();
    $role = wpof_get_role($user_id);
    global $role_nom;
    global $wpof;
    global $post;
    
    ?>
    <div id="profile-box">
    <?php if ($user_id > 0): ?>
    
    <ul id="profile-box-menu">
    <li class="sous-titre nom"><?php echo get_displayname($user_id); ?><br />
    <?php echo $role_nom[$role]; ?></li>
    <li class="sous-titre"><?php _e("Gestion"); ?></li>
    <li class="gestion-link" title="<?php _e('Gérez vos formations, sessions, profil, veille'); ?>"><a href="<?php echo home_url()."/".$wpof->url_user; ?>/"><strong><?php _e("Tableau de bord"); ?></strong></a></li>
    <?php if (in_array($role, array("um_responsable", "admin"))) : ?>
        <li title="<?php _e('Gestion de l’organisme de formation'); ?>" class="bpf-link"><a href="<?php echo home_url().'/'.$wpof->url_gestion; ?>"><?php echo $wpof->title_gestion; ?></a></li>
        <li title="<?php _e('Retrouvez vos bilans pédagogiques et financiers'); ?>" class="bpf-link"><a href="<?php echo home_url().'/'.$wpof->url_bpf; ?>"><?php echo $wpof->title_bpf; ?></a></li>
        <li title="<?php _e('Vue d’ensemble des sessions de l’année'); ?>"  class="pilote-link"><a href="<?php echo home_url().'/'.$wpof->url_pilote; ?>"><?php echo $wpof->title_pilote; ?></a></li>
        <li title="<?php _e('Entrez dans les coulisses d’OPAGA et de WordPress'); ?>"  class="backend"><a href="<?php echo site_url(); ?>/wp-admin/"><?php _e("Options d'OPAGA"); ?></a></li>
    <?php endif; ?>
    <li title="<?php _e('Exportez les informations de vos sessions'); ?>"  class="export-link"><a href="<?php echo home_url().'/'.$wpof->url_export; ?>"><?php echo $wpof->title_export; ?></a></li>
    <li class="sous-titre"><?php _e("Pages publiques"); ?></li>
    <li title="<?php _e('Catalogue de formations de votre organisme de formation'); ?>"  class="page"><a href="<?php echo home_url(); ?>/formations/"><?php _e("Catalogue de formations"); ?></a></li>
    <li title="<?php _e('Calendrier des sessions programmées de votre organisme de formation'); ?>"  class="page"><a href="<?php echo home_url(); ?>/calendrier/"><?php _e("Calendrier"); ?></a></li>
    <li title="<?php _e('Équipe pédagogique de votre organisme de formation'); ?>"  class="page"><a href="<?php echo home_url(); ?>/equipe-pedagogique/"><?php _e("Équipe pédagogique"); ?></a></li>
    <li title="<?php _e('À bientôt !'); ?>"  class="logout"><a href="<?php echo home_url(); ?>/logout/"><?php _e("Déconnexion"); ?></a></li>
    <li title="<?php _e('Documentation utilisateur'); ?>"><a href="https://doc.opaga.fr/"><?php _e("Aide"); ?></a></li>
    <?php if (debug && $role == "admin" || (strstr(home_url(), "make-formations.fr"))) : ?>
        <li id="show-log"><?php _e("Log"); ?></li>
        <li class="dynamic-dialog" data-function="debug_SESSION">$_SESSION</li>
        <?php if (get_post_type() == "session") : ?>
                <li class="dynamic-dialog" data-function="sql_session_formation"><?php echo "SQL Session ".get_the_ID(); ?></li>
        <?php endif; ?>
        
    </ul>
    <?php endif; ?>
    
    <?php elseif ($wpof->allways_menu && (!$post || $post->post_name != $wpof->url_acces)): ?>
    
    <p class="login"><a href="<?php echo home_url(); ?>/login/"><?php _e("Connexion"); ?></a></p>
    
    <?php endif; ?>
    </div>
    
    <?php
}

add_action( 'wp_ajax_switchuser', 'switchuser' );
function switchuser()
{
    $reponse = array();
    $role = wpof_get_role(get_current_user_id());
    if ($role == "admin")
    {
        $user_id = $_POST['user_id'];
        $role_dest = wpof_get_role($user_id);
        if ($role_dest && $role_dest != "admin")
        {
            global $wpof;
            UM()->user()->auto_login($user_id);
            $reponse['url'] = home_url().'/'.$wpof->url_user;
        }
    }
    echo json_encode($reponse);
    
    die();
}

add_action( 'wp_ajax_enregistrer_user_input', 'enregistrer_user_input' );
function enregistrer_user_input()
{
    global $wpof;
    $role = wpof_get_role(get_current_user_id());
    
    $user_id = $_POST['user_id'];
    if ($user_id == get_current_user_id() || in_array($role, $wpof->super_roles))
    {
        $data = array_intersect_key($_POST['fields'], $wpof->desc_formateur->term);
        
        foreach($data as $key => $value)
        {
            $result = update_user_meta($user_id, $key, $value);
            if ($result)
                echo "<span class='succes'>[ ".$key." ] ".__("mise à jour")."</span><br />";
            else
                echo "<span class='alerte'>[ ".$key." ] ".__("inchangée")."</span><br />";
        }
    }
    
    die();
}


?>
