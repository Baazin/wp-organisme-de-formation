<?php
/*
 * class-formateur.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

//require ABSPATH . WPINC . "/class-wp-user.php";

class Formateur extends WP_User
{
    public $nom = "";
    public $prenom = "";
    public $id = "";
    public $permalink;
    
    public $role = "";
        
    public $presentation = "";
    public $photo = null;
    public $marque = null;
    public $statut = null;
    public $realisations = null;
    public $cv_url = null;
    public $cv = null;
    public $profil_public = 0;
    public $genre = null;
    
    public $veille_bloc_notes = "";
    
    public function __construct($user_id = -1)
    {
        if ($user_id > 0)
        {
            parent::__construct($user_id);
            if (isset($this->data))
            {
                global $wpof;
                $this->id = $user_id;
                
                $this->role = wpof_get_role($user_id);
                
                $meta = get_user_meta($user_id);
                
                // Recherche du permalien de l'utilisateur
                $this->permalink = home_url()."/".$wpof->url_user."/".$this->data->user_login;
                
                ?>
                <?php
                // patch TODO
                if (isset($meta['formateur_marque']))
                {
                    $this->update_meta("marque", $meta['formateur_marque']);
                    delete_user_meta($this->id, "formateur_marque");
                }
                
                foreach(array('presentation', 'photo', 'marque', 'statut', 'realisations', 'cv_url', 'cv', 'profil_public', 'genre', 'veille_bloc_notes') as $field)
                    if (isset($meta[$field][0]))
                        $this->$field = $meta[$field][0];
                
                $this->nom = (isset($meta['last_name'][0])) ? $meta['last_name'][0] : "";
                $this->prenom = (isset($meta['first_name'][0])) ? $meta['first_name'][0] : "";
            }
            else
                return null;
        }
    }
    
    /*
    * Retourne le nom (display_name) d'un utilisateur par son ID
    * Si $link vaut true, alors le nom est entouré d'une balise a qui pointe vers sa page de profil
    */
    public function get_displayname($link = false)
    {
        $display_name = "{$this->prenom} {$this->nom}";
        
        if ($link)
        {
            // Recherche du permalien de l'utilisateur
            return "<a href='$this->permalink'>$display_name</a>";
        }
        else
            return $display_name;
    }
    
    /*
     * Retourne l'URL du CV, qu'il soit local ou distant
     */ 
    public function get_cv_url()
    {
        $cv_url = $this->cv_url;
        if (empty($cv_url) && !empty($this->cv))
            $cv_url = wp_get_attachment_url($this->cv);

        return $cv_url;
    }
    
    /*
     * Retourne le taux de complétion du profil formateur sous la forme :
     * nb_champs_remplis / nb_champs_a_repmlir
     */
    public function get_profil_completion($html = true)
    {
        global $wpof, $anime_formation;
        
        if (!isset($wpof->desc_formateur_necessaire))
            $wpof->desc_formateur_necessaire = $wpof->desc_formateur->get_group("necessaire");
        
        // profil complet = desc_formateur_necessaire + au moins une formaiton au catalogue
        $total = count($wpof->desc_formateur_necessaire->term) + 1;
        $rempli = 0;
        foreach(array_keys($wpof->desc_formateur_necessaire->term) as $k)
        {
            $func = "get_doc_".$k;
            if (trim($this->$func()) != "")
                $rempli++;
        }
        if (!empty($anime_formation[$this->id]))
            $rempli++;
        
        if ($html)
        {
            $class = ($rempli/$total == 1) ? "fait" : "attention";
            return "<span class='$class'>$rempli/$total</span>";
        }
        else
            return $rempli/$total;
    }
    
    /*
     * Renvoi le tableau des sessions du formateur
     */
    public function get_sessions($quand)
    {
        $sessions = get_formation_sessions(array('formateur' => $this->id, 'quand' => $quand));
        if ($sessions)
        {
            if ($quand == 'futur') : ?>
            <p><em><?php _e("Contient également les sessions sans date"); ?></em></p>
            <?php endif; ?>
            <table class="opaga opaga2 edit-data">
            <?php
                echo reset($sessions)->get_session_control_head($quand);
                foreach($sessions as $s)
                    echo $s->get_session_control($quand);
            ?>
            </table>
            <?php
        }
        else
            echo "<p>".__("Aucune session programmée")."</p>";
    }
    
    /*
     * Renvoi le tableau des formations du formateur
     */
    public function get_formations()
    {
        $formations = get_formations(array('formateur' => $this->id));
        if ($formations) : ?>
            <table class="opaga opaga2 edit-data">
            <?php
                echo reset($formations)->get_formation_control_head();
                foreach($formations as $f)
                    echo $f->get_formation_control();
            ?>
            </table>
        <?php endif;
    }
    
    
    /*
    * Vue publique d'un utilisateur par quelqu'un d'autre que lui-même
    */
    public function vue_publique()
    {
        global $wpof;
        $role = wpof_get_role(get_current_user_id());
        
        ?>
        <div class="presentation-profil">
        <?php
        echo "<div id='description'>";
        if ($this->photo)
            echo "<img id='photo' class='photo-profil' src='".wp_get_attachment_url($this->photo)."' />";
        echo $this->presentation;
        echo "</div>";
        
        if (champ_additionnel('formateur_marque') && $this->marque)
            echo "<h3>".__("Marque")."</h3><p id='marque'>$this->marque</p>";
        
        if ($this->realisations)
            echo "<div id='realisations'><h3>".__("Ses réalisations")."</h3>".$this->realisations."</div>";
        
        $cv_url = $this->cv_url;
        if (empty($this->cv_url))
        {
            if (!empty($this->cv))
                $cv_url = wp_get_attachment_url($this->cv);
            // Donne la priorité au CV avec une URL externe sur le CV ajouté dans la bibliothèque de médias WP
        }
        if ($cv_url)
            echo "<div id='cv'><a target='_blank' href='$cv_url'><h3>".__("Consulter son CV")."</h3></a></div>";
            
        echo "<div id='formations'>";
        if (in_array($role, array("um_responsable", "admin")))
        {
            echo "<h2>".__("Sessions futures")."</h2>";
            echo $this->get_sessions("futur");
            echo "<h2>".__("Sessions passées")."</h2>";
            echo $this->get_sessions("passe");
            echo "<h2>".__("Catalogue de formations")."</h2>";
            echo $this->get_formations();
        }
        else
        {
            $formations = get_formations(array('formateur' => $this->id, 'format' => 'html'));
            if ($formations)
                echo "<h2>".__("Son catalogue de formations")."</h2>".$formations;
            echo "</div>";
            
            echo "<div id='sessions'>";
            $sessions = get_formation_sessions(array('formateur' => $this->id, 'quand' => 'futur', 'format' => 'html'));
            if ($sessions)
                echo "<h2>".__("Ses prochaines sessions")."</h2>".$sessions;
                
            $sessions = get_formation_sessions(array('formateur' => $this->id, 'quand' => 'passe', 'format' => 'html'));
            if ($sessions)
                echo "<h2>".__("Ses sessions passées")."</h2>".$sessions;
            echo "</div>";
        }
        
        ?> </div> <?php
    }
    
    public function board_profil()
    {
        global $SessionFormation;
        global $tinymce_wpof_settings;
        global $wpof;
        
        ?>
        <div class="board-formateur">
            <?php echo hidden_input("default_user_tab", (isset($_SESSION['user-tabs'])) ? $_SESSION['user-tabs'] : 0); ?>
            <div class="wpof-menu" id="user-tabs">
            <ul>
            <li><a href="#taches-form"><?php _e("Activité"); ?></a></li>
            <li><a href="#metadata-form"><?php _e("Profil de formateur-trice"); ?></a></li>
            <li><a href="#veille-form"><?php _e("Veille"); ?></a></li>
            <?php if (in_array($this->role, array("um_responsable", "admin"))) : ?>
                <li><a href="#comptes"><?php _e("Comptes formateurs/responsables"); ?></a></li>
            <?php endif; ?>
            </ul>
            
            <div id="taches-form" class="tableau">
                <?php
                $signature = is_signataire();
                if ($signature !== false) :
                    $nb_docs = 0;
                    $docs_a_valider = get_docs_to_validated(Document::VALID_RESPONSABLE_REQUEST);
                    $nb_docs = count($docs_a_valider);
                    ?>
                    <?php if ($nb_docs > 0): 
                    $signature_pouvoir = __("Vous êtes habilité⋅e à signer.");
                    if ($signature == "signature_po")
                    {
                        if (signataire_pour_id() === null)
                            $signature_pouvoir = '<span class="erreur">'.__("Aucun⋅e responsable n'a la signature officielle, veuillez corriger cela au plus vite !").'</span>';
                        else
                            $signature_pouvoir = sprintf(__("Vous signez sur ordre de %s."), signataire_pour_nom());
                    }
                    ?>
                    <p><?php echo __("Voici la liste des documents à vérifier et, éventuellement, signer.")." ".$signature_pouvoir; ?></p>
                    
                    <table class='gestion-docs-admin edit-data'><tbody>
                    <tr class="tr-titre">
                        <th><?php _e("Document"); ?></th>
                        <th><?php _e("Finaliser"); ?></th>
                        <th><?php _e("Déposer le doc signé"); ?></th>
                        <th><?php _e("Rejeter le document"); ?></th>
                    </tr>
                    
                    <?php
                    $last_session = 0;
                    
                    foreach ($docs_a_valider as $doc_line)
                    {
                        if ($last_session != $doc_line->session_id)
                        {
                            $formateurs_nom = array();
                            $last_session = $doc_line->session_id;
                            $type_session = get_post_meta($last_session, 'type_formation', true);
                            $formateurs_id = get_post_meta($last_session, 'formateur', true);
                            foreach($formateurs_id as $f_id)
                                $formateurs_nom[] = get_displayname($f_id, false);
                            
                            echo "<tr><td colspan='4' class='td-intertitre'><a href='".get_the_permalink($doc_line->session_id)."'>".get_the_title($doc_line->session_id)."</a> ".__("animée par")." ".join(", ", $formateurs_nom)."</td></tr>";
                        }
                        $doc = new Document($doc_line->document, $doc_line->session_id, $doc_line->contexte, $doc_line->contexte_id);
                        $doc->signataire = true;
                        echo $doc->get_html_ligne(Document::COL_DOCUMENT | Document::COL_FINAL | Document::COL_SCAN | Document::COL_NOM_ENTITE | Document::COL_REJETER);
                    }
                    ?>
                    </tbody></table>
                    <?php else: ?>
                    <p><?php _e("Aucun document à signer. Passez une bonne journée."); ?></p>
                    <hr />
                    <?php
                    endif; // if ($nb_docs > 0):
                endif; // if (is_signataire) :
                
                // Formateur
                ?>
                <h2><?php _e("Sessions futures"); ?></h2>
                <?php echo $this->get_sessions("futur"); ?>
                <h2 class="openButton" data-id="sessions-passees"><?php _e("Sessions passées"); ?></h2>
                <div id="sessions-passees" class="blocHidden">
                <?php echo $this->get_sessions("passe"); ?>
                </div>
                
                <h2><?php _e("Mon catalogue de formations"); ?></h2>
                <?php
                echo $this->get_formations();
                ?>
                <p class="icone-bouton dynamic-dialog" data-function="new_session"><span class="dashicons dashicons-plus-alt"></span> <?php _e("Nouvelle session"); ?></p>
                <p class="icone-bouton dynamic-dialog" data-function="new_formation"><span class="dashicons dashicons-plus-alt"></span> <?php _e("Nouvelle formation"); ?></p>
                <?php echo get_icone_aide("session_definition", __("Qu'est-ce qu'une session ?")); ?>
                <?php echo get_icone_aide("formation_definition", __("Qu'est-ce qu'une formation ?")); ?>
            </div>
            
            <div id="metadata-form" class="tableau metadata notif-modif">
                <?php echo get_icone_aide("utilisateur_profil", __("Renseignez votre profil")); ?>
                <div>
                <h3><?php _e("Photo"); echo get_icone_aide("utilisateur_photo"); ?></h3>
                <a href="#" class="clickButton button-add-media" data-valueid="photo" data-linkmedia="photo_link" data-image="photo_img"><?php _e("Téléversez une image"); ?></a>
                <span title="<?php _e("La photo mesure 35mm de large dans le PDF, pour une résolution de 200 dpi. Pas besoin d'une grande photo qui va alourdir vos PDF inutilement !") ;?>"><?php _e("Largeur conseillée : 280 pixels"); ?></span>
                <input type="hidden" id="photo" name="photo" value="<?php echo $this->photo; ?>" />
                <img id="photo_img" class="photo-profil" src="<?php echo wp_get_attachment_url($this->photo); ?>" />
                </div>
                <div>
                
                <h3><?php _e("CV"); echo get_icone_aide("utilisateur_cv"); ?></h3>
                <p>
                <a href="#" class="clickButton button-add-media" data-valueid="cv" data-linkmedia="cv_link" data-image="cv_img"><?php _e("Téléversez un document PDF"); ?></a>
                <input type="hidden" id="cv" name="cv" value="<?php echo $this->cv; ?>" />
                <a id="cv_link" target="_blank" href="<?php if ($this->cv != "") echo wp_get_attachment_url($this->cv); ?>"><?php if ($this->cv != "") echo get_the_title($this->cv); ?></a>
                </p>
                <p>
                <label for="cv_url"><?php _e("Ou via un lien externe"); ?>
                <input type="text" size="60" id="cv_url" name="cv_url" value="<?php echo $this->cv_url; ?>" /></label>
                </p>
                
                <?php if (champ_additionnel('formateur_marque')) : ?>
                <h3><?php _e("Marque"); echo get_icone_aide("utilisateur_marque"); ?></h3>
                <label for="marque"><?php _e("Votre marque ou nom commercial"); ?>
                <input type="text" size="60" id="marque" name="marque" value="<?php echo $this->marque; ?>" /></label>
                <?php endif; ?>
                
                <?php if (champ_additionnel('formateur_statut')) : ?>
                <h3><?php _e("Statut"); echo get_icone_aide("utilisateur_statut"); ?></h3>
                <label for="statut"><?php _e("Votre statut"); ?>
                <input type="text" size="60" id="statut" name="statut" value="<?php echo $this->statut; ?>" /></label>
                <?php endif; ?>
                
                <h3><?php _e("Présentation"); echo get_icone_aide("utilisateur_presentation"); ?></h3>
                <?php
                    wp_editor($this->presentation, 'presentation', array_merge($tinymce_wpof_settings, array('textarea_rows' => 10, 'textarea_name' => 'presentation')));
                ?>
                <h3><?php _e("Réalisations"); echo get_icone_aide("utilisateur_realisations"); ?></h3>
                <?php
                    wp_editor($this->realisations, 'realisations', array_merge($tinymce_wpof_settings, array('textarea_rows' => 10, 'textarea_name' => 'realisations')));
                ?>
                </div>
                <p data-userid="<?php echo $this->id; ?>" class="bouton submit enregistrer-user-input"><?php _e("Enregistrer les informations"); ?></p>
                <p class="message"></p>
            </div>
            
            <div id="veille-form" class="metadata notif-modif">
                <?php echo get_icone_aide("utilisateur_veille", __("Veille sur la formation")); ?>
                <p><?php _e('Notez ici toutes vos actions de veille : lecture de sites, livres, réseaux, lettres d’info, participations à des conférences, événements, etc. Pensez à dater !'); ?></p>
                <?php
                wp_editor($this->veille_bloc_notes, 'veille_bloc_notes', array_merge($tinymce_wpof_settings, array('textarea_rows' => 20, 'textarea_name' => 'veille_bloc_notes')));
                ?>
                <p data-userid="<?php echo $this->id; ?>" class="bouton submit enregistrer-user-input"><?php _e("Enregistrer les informations"); ?></p>
                <p class="message"></p>
            </div>
            
            <?php if (in_array($this->role, array("um_responsable", "admin"))) : ?>
            <div id="comptes" class="tableau">
            <p><?php echo 'La gestion des comptes se fait désormais via <a href="'.home_url().'/'.$wpof->url_gestion.'/formateur/">Gestion générale → Équipe pédagogique</a>'; ?></p>
            </div>
            <?php endif; ?>
            
            </div> <!-- user-tabs -->
        
        
        </div> <!-- board-formateur -->
        <?php
    }
    
    public function get_formateur_control_head()
    {
        global $wpof;
        $role = wpof_get_role(get_current_user_id());
        ob_start();
        ?>
        <tr>
        <th><?php _e("Nom"); ?></th>
        <th><?php _e("Prénom"); ?></th>
        <th><?php _e("Dernière connexion"); ?></th>
        <th><?php _e("Rôle"); echo get_icone_aide("user_role"); ?></th>
        <th><?php _e("Completion profil"); echo get_icone_aide("user_completion_profil"); ?></th>
        <th><?php _e("Visible"); echo get_icone_aide("user_profil_public"); ?></th>
        <th><?php _e("Sous-traitant"); echo get_icone_aide("user_sous_traitant"); ?></th>
        <th><?php _e("Mentionné dans"); echo get_icone_aide("user_mention"); ?></th>
        <th><?php _e("Modifier"); echo get_icone_aide("user_modifier"); ?></th>
        <th><?php _e("Voir profil"); echo get_icone_aide("user_voir_profil"); ?></th>
        <th><?php _e("Supprimer"); echo get_icone_aide("user_supprimer"); ?></th>
        <?php if ($role == "admin") : ?>
            <th><?php _e("Usurper"); echo get_icone_aide("user_usurper"); ?></th>
        <?php endif; ?>
        </tr>
        <?php
        return ob_get_clean();
    }
    
    /*
     * Renvoie une ligne de tableau pour contrôler la formation
     * 
     */
    public function get_formateur_control()
    {
        global $wpof, $anime_formation, $anime_session, $role_nom;
        $role = wpof_get_role(get_current_user_id());
        ob_start();
        ?>
        <tr id="user<?php echo $this->id; ?>">
        <td><?php echo $this->last_name; ?></td>
        <td><?php echo $this->first_name; ?></td>
        <td><span class="hidden"><?php echo get_last_login_ts($this->id); ?></span><?php echo get_last_login($this->id); ?></td>
        <td><?php echo $role_nom[$this->role]; ?></td>
        <td class="center"><?php echo $this->get_profil_completion(); ?></td>
        <td class="center"><?php echo get_user_check_jpost("profil_public", "", $this->id); ?></td>
        <td class="center"><?php echo get_user_check_jpost("sous_traitant", "", $this->id); ?></td>
        <td>
        <?php
            if (isset($anime_formation[$this->id]))
                echo "<p>".__("Formations")." : ".join(', ', $anime_formation[$this->id])."</p>";
            if (isset($anime_session[$this->id]))
                echo "<p>".__("Sessions")." : ".join(', ', $anime_session[$this->id])."</p>";
        ?></td>
        <td class="center"><a href="<?php echo home_url(); ?>/wp-admin/user-edit.php?user_id=<?php echo $this->id; ?>"><span class="icone-bouton dashicons dashicons-edit-large"></span></a></td>
        <td class="center"><a href="<?php echo $this->permalink; ?>"><span class="icone-bouton dashicons dashicons-visibility"></span></a></td>
        <td class="center"><span class="icone-bouton dashicons dashicons-dismiss delete-entity" data-objectclass="Utilisateur" data-id="<?php echo $this->id; ?>" data-parent="#user<?php echo $this->id; ?>"></span></td>
        <?php if ($role == "admin"): ?>
            <td class="center"><span data-userid='<?php echo $this->id; ?>' class='icone-bouton switch-user dashicons dashicons-controls-repeat'></span></td>
        <?php endif; ?>
        </tr>
        <?php
        return ob_get_clean();
    }
    
    
    public function get_doc_prenom_nom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $identite = "{$this->prenom} {$this->nom}";
        $result = array('valeur' => $identite, 'tag' => "formateur:$function_name", 'desc' => __("Identité complète de la personne : prénom nom. Adapté lorsque ce marqueur doit faire apparaître les infos de plusieurs personnes."));
        return $result[$arg];
    }
    public function get_doc_identite($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $identite = "{$this->prenom} {$this->nom}";
        if ($this->marque != "")
            $identite .= " ({$this->marque})";
        $result = array('valeur' => $identite, 'tag' => "formateur:$function_name", 'desc' => __("Identité complète de la personne : prénom nom (marque si existe). Adapté lorsque ce marqueur doit faire apparaître les infos de plusieurs personnes."));
        return $result[$arg];
    }
    public function get_doc_prenom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "formateur:$function_name", 'desc' => __("Prénom"));
        return $result[$arg];
    }
    public function get_doc_nom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "formateur:$function_name", 'desc' => __("Nom"));
        return $result[$arg];
    }
    public function get_doc_presentation($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "formateur:$function_name", 'desc' => __("Présentation"));
        return $result[$arg];
    }
    public function get_doc_photo($arg = 'valeur', $dimensions = "")
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $photo = "";
        if ($arg == 'valeur')
        {
            $style = get_style_dimensions($dimensions);
            $photo = '<img src="'.str_replace('https', 'http', wp_get_attachment_url($this->photo)).'" '.$style.' />';
        }
        $result = array('valeur' => $photo, 'tag' => "formateur:$function_name", 'desc' => __("Photo (dimensions à ajouter sous la forme {of:logo:largxhaut} unité(s) en mm obligatoirement ; ex. {formateur:photo:x50} pour une hauteur de 50mm et largeur automatique)"));
        return $result[$arg];
    }
    public function get_doc_marque($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $marque = $this->$function_name;
        if ($marque == "")
            $marque = $this->get_doc_prenom_nom();
        $result = array('valeur' => $marque, 'tag' => "formateur:$function_name", 'desc' => __("Marque (si l’option est activée)"));
        return $result[$arg];
    }
    public function get_doc_statut($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "formateur:$function_name", 'desc' => __("Statut formateur (si l’option est activée)"));
        return $result[$arg];
    }
    public function get_doc_realisations($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "formateur:$function_name", 'desc' => __("Réalisations"));
        return $result[$arg];
    }
    public function get_doc_cv_url($arg = 'valeur')
    {
        $cv_url = "";
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        if ($arg == 'valeur')
            $cv_url = $this->get_cv_url();
        $result = array('valeur' => $cv_url, 'tag' => "formateur:$function_name", 'desc' => __("URL du CV (interne ou externe)"));
        return $result[$arg];
    }
    public function get_doc_veille_bloc_notes($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "formateur:$function_name", 'desc' => __("Bloc-notes de veille"));
        return $result[$arg];
    }
}

function get_formateur_by_id($id)
{
    if ($id < 0) return null;
    global $Formateur;
    
    if (!isset($Formateur[$id]))
        $Formateur[$id] = new Formateur($id);
        
    return $Formateur[$id];
}


function get_tab_formateur_by_tab_id($ids)
{
    $formateur_tab = array();
    
    foreach($ids as $id)
        $formateur_tab[] = new Formateur($id);
    
    return $formateur_tab;
}

add_action('wp_ajax_check_my_formation', 'check_my_formation');
function check_my_formation()
{
    $reponse = array("my_formation" => 1);
    $user_id = get_current_user_id();
    $role = wpof_get_role($user_id);
    
    $entite = ($_POST['object_class'] == "Formation") ? "formation" : "session";
    
    if ($role == 'um_formateur-trice' && !in_array($user_id, $_POST['value']))
    {
        $reponse['my_formation'] = 0;
        $reponse['message'] = sprintf(__("Vous venez de vous retirer de l'animation de cette %s."), $entite).'<br />';
            $reponse['message'] .= '<span class="important">'.sprintf(__("Une fois cette page fermée ou rechargée vous ne pourrez plus modifier cette %s !"), $entite).'</span><br />'.__("Si ce n'est pas votre souhait, remettez-vous dans l'équipe pédagogique (Ctrl + clic pour sélectionner plusieurs noms)");
    }
    $reponse['log'] = "$role / user_id $user_id";
    echo json_encode($reponse);
    die();
}
