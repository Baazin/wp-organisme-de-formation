<?php
/*
 * class-session-stagiaire.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-document.php");
require_once(wpof_path . "/class/class-creneau.php");

class SessionStagiaire
{
    public $statut_stagiaire = "";
    public $attentes = "";
    public $date_modif_attentes = "aucune";
    public $date_entretien = "";
    public $etat_session = "initial";
    public $confirme = 1;
    public $temps; // Objet DateTime
    public $nb_heure; // au format horaire : 3:30
    public $nb_heure_decimal = 0; // nombre d'heures au format décimal : 3,5 pour 3:30
    public $nb_heure_estime_decimal = 0; // nombre d'heures au format décimal : 3,5 pour 3:30
    
    // Créneaux de session suivis par le stagiaire
    public $creneaux = array();
    public $dates_array = array();
    public $dates_texte = "";
    
    public $eval_prerequis = array();
    public $eval_preform = array();
    public $eval_postform = array();
    
    public $doc_necessaire = array();
    public $doc_uid = array();
    public $session_formation_id;
    public $prenom = "";
    public $nom = "";
    public $email = "";
    public $username = "";
    public $client_id = -1;
    public $id = -1;
    
    // token d'accès aux infos
    public $token = "";
    public $token_time = 0;
    public $acces = 0;
    
    // table suffix
    private $table_suffix;
    


    public function __construct($session_id = -1, $id = -1)
    {
        global $SessionFormation;
        global $wpof;
        global $suffix_session_stagiaire;
        
        $this->table_suffix = $suffix_session_stagiaire;
        
        if ($session_id > 0)
        {
            $this->session_formation_id = $session_id;
            
            $session = get_session_by_id($session_id);
            
            log_add(__METHOD__."({$this->get_displayname()})", "stagiaire");
            
            if ($id > 0)
            {
                $this->id = $id;
                global $SessionStagiaire;
                $SessionStagiaire[$this->id] = $this;
                
                $meta = $this->get_meta();
                
                foreach($meta as $m)
                {
                    $this->{$m['meta_key']} = stripslashes($m['meta_value']);
                }
                
                // recherche du numéro de client si absent
                /*
                if ($this->client_id == -1)
                {
                    $session = get_session_by_id($this->session_id);
                    $client_id = reset($session->clients);
                    while ($client_id !== false && $this->client_id == -1)
                    {
                        $client = get_client_by_id($this->session_id, $client_id);
                        if (in_array($this->id, $client->stagiaires))
                            $this->client_id = $client_id;
                        else
                            $client_id = next($session->clients);
                    }
                }
                */
                
                // nom d'utilisateur (non utilisé par OPAGA mais exportable)
                if (empty($this->username))
                    $this->update_meta("username", sanitize_user(strtolower(str_replace(" ", "-", $this->get_displayname()))));
            
                // documents nécessaires
                foreach($wpof->documents->term as $doc_index => $doc)
                {
                    if ($doc->contexte & $wpof->doc_context->stagiaire)
                        $this->doc_necessaire[] = $doc_index;
                }
                $this->doc_suffix = "s".$this->id;
                
                $this->creneaux = array();
                
                // les créneaux actifs sont stockés dans un tableau plat : l'id du créneau en clé et la valeur 0 (inactif) ou 1 (actif)
                $creneaux_from_db = $this->get_meta("creneaux");
                if (!empty($creneaux_from_db))
                    $creneaux_from_db = json_decode($creneaux_from_db, true);
                else
                    $creneaux_from_db = array();
                
                // on définit tous les créneaux comme actifs par défaut ou si $creneaux_from_db[id] n'est pas définit
                if (is_array($session->creneaux))
                    foreach($session->creneaux as $d)
                        foreach($d as $c)
                            $this->creneaux[$c->id] = (isset($creneaux_from_db[$c->id])) ? $creneaux_from_db[$c->id] : 1;

                $this->calcule_temps_session();
            }
        }
    }
    
    /*
     * Création des documents
     * En effet, on n'a pas toujours besoin des documents lorsqu'on instancie une session stagiaire
     */
    public function init_docs()
    {
        global $Documents;
        global $wpof;

        foreach ($this->doc_necessaire as $d)
        {
            $doc = new Document($d, $this->session_formation_id, $wpof->doc_context->stagiaire, $this->id);
            $Documents[$doc->id] = $doc;
            $this->doc_uid[] = $doc->id;
        }
    }
    
    /*
     * Retourne une métadonnée (ou toutes) de session stagiaire
     * $meta_key : clé de la valeur recherchée
     *
     * Retourne la valeur recherchée ou un tableau de toutes les valeurs
     */
    public function get_meta($meta_key = null)
    {
        return get_stagiaire_meta($this->id, $meta_key);
    }
    
    public function get_displayname()
    {
        return $this->prenom." ".$this->nom;
    }
    

    public function update_meta($meta_key, $meta_value = null)
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        if ($meta_value === null)
            $meta_value = $this->$meta_key;
        else
            $this->$meta_key = $meta_value;
        
        if (is_array($meta_value))
            $meta_value = serialize($meta_value);
        
        $query = $wpdb->prepare
        ("INSERT INTO $table (session_id, stagiaire_id, meta_key, meta_value)
            VALUES ('%d', '%d', '%s', '%s')
            ON DUPLICATE KEY UPDATE meta_value = '%s';",
            $this->session_formation_id, $this->id, $meta_key, $meta_value, $meta_value);
        
        $res = $wpdb->query($query);
        
        return $res;
    }
    
    
    public function delete_meta($meta_key)
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare ("DELETE FROM $table WHERE meta_key = '%s' AND session_id = '%d' AND stagiaire_id = '%d';", $meta_key, $this->session_formation_id, $this->id);
        
        return $wpdb->query($query);
    }
    
    
    // Renseigne le tableau des exercices comptables
    // En général ce tableau n'aura qu'une valeur (si toute la session se déroule dans la même année civile)
    public function set_exercice_comptable()
    {
        global $SessionFormation;
        if ($SessionFormation[$this->session_formation_id]->type_index == 'inter')
        {
            $this->exe_comptable = unserialize($this->get_meta("exe_comptable"));
            
            if (!is_array($this->exe_comptable) || count($this->exe_comptable) == 0)
            {
                $this->exe_comptable = array();
                foreach($this->dates_array as $d)
                {
                    $date = explode('/', $d);
                    $this->exe_comptable[$date[2]] = 0;
                }
                $last_year = array_keys($this->exe_comptable);
                $last_year = $last_year[count($last_year) - 1];
                $this->exe_comptable[$last_year] = $this->tarif_total_chiffre;
                
                $this->update_meta("exe_comptable");
            }
            $diff = $this->tarif_total_chiffre - array_sum($this->exe_comptable);
            if ($diff != 0)
            {
                $last_year = array_keys($this->exe_comptable);
                $last_year = $last_year[count($last_year) - 1];
                $this->exe_comptable[$last_year] += $diff;
                
                $this->update_meta("exe_comptable");
            }
        }
    }
    
    
    // Calcule le temps (en heures) de suis de la session par ce stagiaire (somme de tous les créneaux actifs)
    public function calcule_temps_session()
    {
        $session_formation = get_session_by_id($this->session_formation_id);
        $client = get_client_by_id($this->session_formation_id, $this->client_id);
        
        $this->temps = DateTime::createFromFormat("U", "0");
        $this->dates_array = array();
        
        foreach($session_formation->creneaux as $date => $tab_date)
            foreach($tab_date as $creno)
            {
                if (!isset($this->creneaux[$creno->id]) || !isset($client->creneaux[$creno->id]) || $this->creneaux[$creno->id] * $client->creneaux[$creno->id] != 0)
                {
                    if ($creno->type != "technique")
                        $this->temps->add($creno->duree);
                    $this->dates_array[] = $date;
                }
            }
        
        $this->dates_array = array_unique($this->dates_array);
        $this->dates_texte = pretty_print_dates($this->dates_array);
        
        $h = $this->temps->format("U") / 3600;
        $m = ($this->temps->format("U") % 3600) / 60;
        $this->nb_heure_decimal = $this->temps->format("U") / 3600;
        $this->nb_heure = sprintf("%02d:%02d", $h, $m);
        
        if ($this->nb_heure_estime_decimal == 0)
        {
            $this->nb_heure_estime_decimal = $this->nb_heure_decimal;
            $this->nb_heure_estime = $this->nb_heure;
        }
        else
        {
            $h = (integer) $this->nb_heure_estime_decimal;
            $m = ($this->nb_heure_estime_decimal - $h) * 60;
            $this->nb_heure_estime = sprintf("%02d:%02d", $h, $m);
        }
    }
    
    public function calcule_tarif()
    {
        if ($this->tarif_base_total == 1)
        {
            if ($this->nb_heure_decimal > 0)
                $this->tarif_heure = sprintf("%.2f", round($this->tarif_total_chiffre / $this->nb_heure_decimal, 2));
        }
        else
            $this->tarif_total_chiffre = sprintf("%.2f", round($this->tarif_heure * $this->nb_heure_decimal, 1));
        $this->tarif_total_lettre = num_to_letter($this->tarif_total_chiffre);
    }
    
    /*
     * Suppression du stagiaire pour cette session
     */
    public function delete()
    {
        // supression du client du tableau inscrits de la session TODO : supprimer ce tableau définitivement
        $session = get_session_by_id($this->session_formation_id);
        $session->supprime_sous_entite("inscrits", $this->id);
        
        $client = get_client_by_id($this->session_formation_id, $this->client_id);
        $client->supprime_sous_entite("stagiaires", $this->id);
        
        // suppression du stagiaire dans la base de données
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare
        ("DELETE FROM $table
            WHERE stagiaire_id = '%d' AND session_id = '%d';",
            $this->id, $this->session_formation_id
        );
        
        return $wpdb->query($query);
    }
    
    public function get_last_id()
    {
        global $wpdb, $suffix_session_stagiaire;
        $table = $wpdb->prefix.$suffix_session_stagiaire;
        
        $last_id = $wpdb->get_var("SELECT MAX(`stagiaire_id`) FROM $table;");
        if (!$last_id)
            $last_id = 0;
        
        return $last_id;
    }
    
    /*
     * Interface avec le stagiaire
     */
    public function the_interface()
    {
        echo $this->get_the_interface();
    }
    
    public function get_the_interface()
    {
        global $wpof;
        $session_formation = get_session_by_id($this->session_formation_id);
        
        ob_start();
        
        echo hidden_input("default_main_tab", (isset($_SESSION['main-tabs'])) ? $_SESSION['main-tabs'] : 0);
        ?>
        <p><?php echo $this->get_displayname(); ?></p>
        <h2><?php echo $session_formation->titre_formation; ?></h2>
        <p><strong><?php _e("Dates"); ?></strong> <?php echo $session_formation->dates_texte; ?></p>
        <div id="main-tabs">
            <ul>
                <li><a href="#tab-presentation"><?php _e("Votre formation"); ?></a></li>
                <li><a href="#tab-initial"><?php _e("Avant la formation"); ?></a></li>
                <li><a href="#tab-documents"><?php _e("Documents"); ?></a></li>
                <li><a href="#tab-apres"><?php _e("Après la formation"); ?></a></li>
            </ul>
            
            <div id="tab-presentation" class="stagiaire" data-userid="<?php echo $this->id; ?>">
            <?php echo $session_formation->get_html_presentation(array('entite' => $this)); ?>
            </div>
            
            <div id="tab-initial" class="stagiaire" data-userid="<?php echo $this->id; ?>">
                <form class="notif-modif">
                <h2><?php _e("Vos besoins"); ?></h2>
                <p><?php _e("Quelles sont vos attentes par rapport à cette formation ? Comment allez-vous mettre en pratique ce que vous allez apprendre ?"); ?></p>
                <textarea cols='60' rows='15' name='attentes'><?php echo $this->attentes; ?></textarea>
                
                <?php
                    if (!empty($session_formation->quizpr->sujet))
                    {
                        echo "<h2>{$session_formation->quizpr->sujet_titre}</h2>";
                        echo $session_formation->quizpr->get_html($this->id, $session_formation->quizpr_id, 0);
                    }
                    if (!empty($session_formation->quizobj->sujet))
                    {
                        echo "<h2>{$session_formation->quizobj->sujet_titre} avant la formation</h2>";
                        echo $session_formation->quizobj->get_html($this->id, $session_formation->quizobj_id, 0);
                    }
                ?>
                <div class='icone-bouton submit stagiaire-submit'><span class='dashicons dashicons-plus-alt'></span><?php _e("Enregistrez vos modifications"); ?></div>
                </form>
                <p class="message"></p>
            </div> <!-- initial -->
            
            <div id="tab-documents">
                <?php
                global $Documents;
                $this->init_docs();
                $liste_doc = "";
                foreach ($this->doc_uid as $docuid)
                {
                    $doc = $Documents[$docuid];
                    if ($doc->visible)
                        $liste_doc .= "<li class='doc {$doc->type}'>{$doc->link_name}</li>";
                }
                if ($liste_doc != "") : ?>
                <ul class="liste-doc-stagiaire">
                <?php echo $liste_doc; ?>
                </ul>
                <?php else : ?>
                <p><?php echo $wpof->doc_pas_de_docs; ?></p>
                <?php endif; ?>
            </div>
        
            <div id="tab-apres">
            <form>
            <?php
                if (!empty($session_formation->quizobj->sujet))
                {
                    echo "<h2>{$session_formation->quizobj->sujet_titre} après la formation</h2>";
                    echo $session_formation->quizobj->get_html($this->id, $session_formation->quizobj_id, 1);
                }
            ?>
            <div class='icone-bouton stagiaire-submit'><span class='dashicons dashicons-plus-alt'></span><?php _e("Enregistrez vos modifications"); ?></div>
            </form>
            <p class="message"></p>
            </div>
        
        </div>
        <?php
        return ob_get_clean();
    }
    
    /*
     * Tableau de bord stagiaire, pour le formateur
     */
    public function the_board()
    {
        echo $this->get_the_board();
    }
    public function get_the_board()
    {
        $current_user_id = get_current_user_id();
        $role = wpof_get_role($current_user_id);
        
        if ($role == "um_stagiaire")
            return $this->get_the_interface();
        
        $client = get_client_by_id($this->session_formation_id, $this->client_id);
        
        global $doc_nom;
        global $wpof;
        global $Documents;
        $this->init_docs();
        
        $session_formation = get_session_by_id($this->session_formation_id);
        
        ob_start();
        ?>
        <div class='board board-stagiaire edit-data client-<?php echo $this->client_id; ?> stagiaire-<?php echo $this->id; ?>' id='tab-s<?php echo $this->id; ?>'>
            <div class="infos-stagiaire flexrow">
            <?php
            ?>
                <div>
                <?php
                    echo get_input_jpost($this, "prenom", array('input' => 'text', 'label' => __("Prénom")));
                    echo get_input_jpost($this, "nom", array('input' => 'text', 'label' => __("Nom")));
                    echo get_input_jpost($this, "email", array('input' => 'text', 'label' => __("Adresse email")));
                    echo get_input_jpost($this, "statut_stagiaire", array('select' => '', 'label' => __("Statut du stagiaire")));
                    echo get_input_jpost($this, "nb_heure_estime_decimal", array('input' => 'number', 'label' => __("Durée estimée en heures (en décimal)"))); 
                    echo get_input_jpost($this, "confirme", array('input' => 'checkbox', 'label' => __("Inscription confirmée")));
                ?>
                </div>
                <div class="icones">
                <p class="delete-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>" data-parent=".stagiaire-<?php echo $this->id; ?>">
                <span class="dashicons dashicons-dismiss" > </span>
                <?php _e("Supprimer ce stagiaire"); ?>
                </p>
                
                <p class="token-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>" data-parent=".stagiaire-<?php echo $this->id; ?>">
                <span class="dashicons dashicons-admin-network" > </span>
                <?php _e("Envoyer accès privé"); ?>
                </p><?php echo get_icone_aide("envoi_token", "Accès privé"); ?>
                
                <?php if ($role == "admin"): ?>
                <p>ID : <?php echo $this->id; ?></p>
                <p>Client ID : <?php echo $this->client_id; ?></p>
                <?php endif; ?>
                
                <p><span class='last-modif'><?php //echo __("Dernière connexion")." ".get_last_login($this->id); ?></span></p>
                </div>
            </div> <!-- infos-stagiaire -->
            
            <fieldset>
                <legend><?php _e("Documents administratifs pour le/la stagiaire"); ?></legend>
                <?php echo get_gestion_docs($this); ?>
            </fieldset>
                
            <?php if (0) : ?>
            <fieldset><legend><?php _e("Créneaux de présence"); ?></legend>
            <?php
                if (count($session_formation->creneaux) == 0)
                    echo "<p class='alerte'>".__("Définissez d'abord des créneaux")."</p>";
                else
                    echo $session_formation->get_html_creneaux(false, $this);
            ?>
            </fieldset>
            <?php endif; ?>
            <fieldset>
                <legend><?php _e("Évaluations et besoins"); ?></legend>
                <?php echo $this->get_bilan_eval(); ?>
            </fieldset>
                
        </div> <!-- board-stagiaire -->
        <?php
        return ob_get_clean();
    }
    
    public function get_bilan_eval()
    {
        $session = get_session_by_id($this->session_formation_id);
        $role = wpof_get_role(get_current_user_id());
        
        $html = "";
        
        $html .= "<div class='attentes'>";
        $html .= get_input_jpost($this, "attentes", array('textarea' => 1, 'rows' => '10', 'cols' => '60', 'label' => __("Attentes, besoins, motivations"), 'postprocess' => 'update_data', 'ppargs' => array('ppkey' => 'date_modif_attentes', 'ppvalue' => date("d/m/Y", time()), 'ppdest' => "#date_modif_attentes{$this->id}")));
        if ($role != null)
            $html .= get_input_jpost($this, "date_entretien", array("input" => "datepicker", "label" => "Date de l'entretien", "inline" => 1));
        $html .= "<p>".__("Date de dernière modification des attentes")." : <span id='date_modif_attentes{$this->id}'>{$this->date_modif_attentes}</span></p>";
        $html .= "</div>";
        
        $html .= "<div class='radio-disable'>";
        if (!empty($session->quizpr->sujet))
        {
            $html .= "<h2>{$session->quizpr->sujet_titre}</h2>";
            $html .= $session->quizpr->get_html($this->id, $session->quizpr_id, 0);
        }
        if (!empty($session->quizobj->sujet))
        {
            $html .= "<h2>{$session->quizobj->sujet_titre} avant la formation</h2>";
            $html .= $session->quizobj->get_html($this->id, $session->quizobj_id, 0);
            $html .= "<h2>{$session->quizobj->sujet_titre} après la formation</h2>";
            $html .= $session->quizobj->get_html($this->id, $session->quizobj_id, 1);
        }
        $html .= "</div>";
        
        return $html;
    }

    public function get_doc_prenom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "stagiaire:$function_name", 'desc' => __("Prénom"));
        return $result[$arg];
    }
    public function get_doc_nom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "stagiaire:$function_name", 'desc' => __("Nom"));
        return $result[$arg];
    }
    public function get_doc_email($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "stagiaire:$function_name", 'desc' => __("Adresse email"));
        return $result[$arg];
    }
    public function get_doc_statut_stagiaire($arg = 'valeur')
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $valeur = "";
        if ($arg == 'valeur')
        {
            if ($wpof->$function_name->is_term($this->$function_name))
                $valeur = $wpof->$function_name->get_term($this->$function_name);
        }
        $result = array('valeur' => $valeur, 'tag' => "stagiaire:$function_name", 'desc' => __("Statut du stagiaire (cf. BPF)"));
        return $result[$arg];
    }
    public function get_doc_attentes($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "stagiaire:$function_name", 'desc' => __("Attentes, besoins, motivations"));
        return $result[$arg];
    }
    public function get_doc_confirme($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "stagiaire:$function_name", 'desc' => __("Inscription confirmée"));
        return $result[$arg];
    }
    public function get_doc_nb_heure($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "stagiaire:$function_name", 'desc' => __("Nombre d’heures suivies"));
        return $result[$arg];
    }
}

function get_stagiaire_meta($stagiaire_id, $meta_key)
{
    global $wpdb;
    global $suffix_session_stagiaire;
    $table_session_stagiaire = $wpdb->prefix.$suffix_session_stagiaire;
    
    if ($meta_key)
    {
        $query = $wpdb->prepare
        ("SELECT meta_value
            from $table_session_stagiaire
            WHERE stagiaire_id = '%d'
            AND meta_key = '%s';",
            $stagiaire_id, $meta_key);
        return $wpdb->get_var($query);
    }
    else
    {
        $query = $wpdb->prepare("SELECT meta_key, meta_value from $table_session_stagiaire WHERE stagiaire_id = '%d';", $stagiaire_id);
        return $wpdb->get_results($query, ARRAY_A);
    }
}

function get_stagiaire_by_id($session_id, $id)
{
    global $SessionStagiaire;
    
    if (!isset($SessionStagiaire[$id]))
        $SessionStagiaire[$id] = new SessionStagiaire($session_id, $id);
    
    return $SessionStagiaire[$id];
}

function get_all_stagiaires()
{
    global $wpdb;
    global $suffix_session_stagiaire;
    $table_session_stagiaire = $wpdb->prefix.$suffix_session_stagiaire;
    
    $stagiaires = array();
    $result = $wpdb->get_results("SELECT DISTINCT session_id, stagiaire_id FROM $table_session_stagiaire;");
    if (is_array($result))
        foreach($result as $row)
        {
            $s = new SessionStagiaire($row->session_id, $row->stagiaire_id);
            if (!empty($s->prenom))
                $stagiaires[] = $s;
        }
    
    return $stagiaires;
}

function get_stagiaire_displayname($id)
{
    global $wpdb;
    global $suffix_session_stagiaire;
    $table_session_stagiaire = $wpdb->prefix.$suffix_session_stagiaire;
    
    $query = $wpdb->prepare("SELECT meta_key, meta_value FROM $table_session_stagiaire WHERE stagiaire_id = '%d' AND (meta_key = 'prenom' OR meta_key = 'nom');", $id);
    $result = $wpdb->get_results($query);
    $prenom = $nom = "";
    foreach($result as $m)
        ${$m->meta_key} = $m->meta_value;
    
    return "$prenom $nom";
}

/*
 * Ajouter un stagiaire
 * Analyse des données du formulaire
 */
add_action( 'wp_ajax_add_stagiaire', 'add_stagiaire' );
function add_stagiaire($args = array())
{
    $reponse = array('message' => '', 'stagiaires_board' => '', 'html' => '');
    
    $session = null;
    $session_id = 0;
    $stagiaire = null;
    $ajax = false;
    
    if (empty($args))
    {
        if (isset($_POST))
        {
            $args = $_POST;
            $ajax = true;
        }
        else
            return false;
    }
    
    $reponse['log2'] = var_export($args, true);
    
    if (isset($args['session_id'])) $session_id = $args['session_id'];
    if (isset($args['client_id'])) $client_id = $args['client_id'];
        
    if ($session_id > 0)
    {
        $session = get_session_by_id($session_id);
        
        if ($client_id > 0)
        {   
            $client = get_client_by_id($session_id, $client_id);
            $stagiaire = new SessionStagiaire($session_id);
            
            foreach(array('genre', 'prenom', 'nom', 'email') as $field)
                $stagiaire->$field = (isset($args[$field])) ? $args[$field] : "";
            
            if ($stagiaire->prenom != "" && $stagiaire->nom != "")
            {
                $stagiaire->client_id = $client_id;
                $stagiaire_id = $stagiaire->id = $stagiaire->get_last_id() + 1;
                $stagiaire->update_meta("prenom");
                $stagiaire->update_meta("nom");
                $stagiaire->update_meta("email");
                $stagiaire->update_meta("genre");
                $stagiaire->update_meta("username", sanitize_user(strtolower(str_replace(" ", "-", $stagiaire->get_displayname()))));
                $stagiaire->update_meta("confirme", 1);
                $stagiaire->update_meta("client_id", $client_id);
                $reponse['message'] .= "<span class='succes'>".$stagiaire->get_displayname()." ".__("ajouté⋅e")."</span>";
            }
            
            $client->stagiaires[] = $stagiaire->id;
            $client->update_meta("stagiaires", $client->stagiaires);
            if (debug)
                $reponse['log2'] .= var_export($stagiaire, true);
        }
    }
    
    if ($reponse['message'] == "")
    {
        $reponse['message'] .= "<span class='erreur'>".__("Saisissez un prénom et un nom pour inscrire un⋅e stagiaire")."</span>";
        $reponse['erreur'] = $reponse['message'];
    }
    else
    {
        $reponse['succes'] = $reponse['message'];
        if ($session)
            $reponse['html'] = $session->get_tabs_clients();
    }
    
    if ($ajax)
    {
        echo json_encode($reponse);
        die();
    }
    elseif ($stagiaire)
        return $stagiaire;
    else
        return $reponse;
}
