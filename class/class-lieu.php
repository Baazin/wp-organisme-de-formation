<?php
/*
 * class-lieu.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


class Lieu
{
    public $nom = "";
    public $permalien = "";
    public $slug = "";
    
    public $ville = "";
    public $adresse = "";
    public $code_postal = "";
    public $localisation = "";
    public $secu_erp = -1;
    
    public $id;
    
    public function __construct($lieu_id = -1)
    {
        $this->id = $lieu_id;
        
        if ($lieu_id > 0)
        {
            $data = get_post($lieu_id);
            $meta = get_post_meta($lieu_id);
            
            // infos issues du post
            $this->nom = $data->post_title;
            $this->permalien = get_the_permalink($lieu_id);
            $this->slug = $data->post_name;
            
            // metadonnées
            $this->ville = $meta['ville'][0];
            $this->adresse = $meta['adresse'][0];
            $this->code_postal = $meta['code_postal'][0];
            $this->localisation = $meta['localisation'][0];
            $this->secu_erp = isset($meta['secu_erp']) ? $meta['secu_erp'][0] : "";
        }
    }
}

function get_lieu_by_id($id)
{
    if ($id < 0) return null;
    global $Lieu;
    
    if (!isset($Lieu[$id]))
        $Lieu[$id] = new Lieu($id);
        
    return $Lieu[$id];
}

