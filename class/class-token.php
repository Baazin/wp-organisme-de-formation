<?php
/*
 * class-token.php
 * 
 * Copyright 2021 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


class Token
{
    public $token;
    
    public function __construct($token = null)
    {
        if ($token === null)
            $this->init(); 
        else
            $this->token = $token;
    }

    /*
    * Recherche du client ou du stagiaire correspondant au token
    * Vérification de sa validité
    */
    public function get_acces()
    {
        global $wpdb, $wpof, $suffix_client, $suffix_session_stagiaire;
        
        $reponse = array();
        
        $query = $wpdb->prepare("SELECT client_id,session_id FROM ".$wpdb->prefix.$suffix_client." WHERE meta_key = 'token' AND meta_value = '%s';", $this->token);
        $ids = $wpdb->get_row($query);
        
        if ($ids != null)
        {
            $token_time = (integer) get_client_meta($ids->client_id, "token_time");
            $client = get_client_by_id($ids->session_id, $ids->client_id);
            $reponse['session_id'] = $ids->session_id;
            
            if ($token_time == 0 && !is_user_logged_in())
                $client->update_meta("token_time", time());
            
            if (valid_token($client->token_time))
            {
                $reponse['class'] = "Client";
                $reponse['user'] = $client;
                if (!is_user_logged_in())
                    $client->update_meta("acces", time());
            }
            else
            {
                $reponse['erreur'] = __("Délai expiré");
                /*$client->update_meta("token", "");
                $client->update_meta("token_time", 0);*/
            } 
        }
        else
        {
            $query = $wpdb->prepare("SELECT stagiaire_id,session_id FROM ".$wpdb->prefix.$suffix_session_stagiaire." WHERE meta_key = 'token' AND meta_value = '%s';", $this->token);
            $ids = $wpdb->get_row($query);
            
            if ($ids != null)
            {
                $reponse['session_id'] = $ids->session_id;
                $token_time = (integer) get_stagiaire_meta($ids->stagiaire_id, "token_time");
                $stagiaire = get_stagiaire_by_id($ids->session_id, $ids->stagiaire_id);
                if ($token_time == 0 && !is_user_logged_in())
                    $stagiaire->update_meta("token_time", time());
                
                if (valid_token($stagiaire->token_time))
                {
                    $reponse['class'] = "SessionStagiaire";
                    $reponse['user'] = $stagiaire;
                    if (!is_user_logged_in())
                        $stagiaire->update_meta("acces", time());
                }
                else
                {
                    $reponse['erreur'] = __("Délai expiré");
                    /*$stagiaire->update_meta("token", "");
                    $stagiaire->update_meta("token_time", 0);*/
                }
            }
            else
                $reponse['erreur'] = __("Jeton inconnu");
        }
        
        return $reponse;
    }

    public function init()
    {
        global $wpof;
        
        $this->token = "";
        $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $max_charset = strlen($charset) - 1;
        
        for ($x = 1; $x <= random_int(1, 15); $x++)
            $charset = str_shuffle($charset);
            
        for ($s = 1; $s <= $wpof->token_length; $s++)
            $this->token .= substr($charset, random_int(0, $max_charset), 1);
    }    
}

function valid_token($token_time)
{
    global $wpof;
    if ($wpof->token_validity == 0 || $token_time == 0)
        return true;
    $reste = ($token_time + $wpof->token_validity * $wpof->token_seconds_by_unit) - time();
    if ($reste > 0)
        return $reste;
    else
        return false;
}

add_action('wp_ajax_new_token', 'new_token');
function new_token()
{
    global $wpof;
    $reponse = array();
    $session_id = $_POST['session_id'];
    $entite_id = $_POST['id'];
    $object_class = $_POST['object_class'];
    
    if ($object_class == null)
        return "";
    
    $entite = new $object_class($session_id, $entite_id);
    
    if ($object_class == "Client")
    {
        $nom = $entite->contact;
        $email = array($entite->contact_email, $entite->responsable_email);
    }
    else
    {  
        $nom = $entite->get_displayname();
        $email = array($entite->email);
    }
    $session_titre = get_the_title($entite->session_formation_id);
    
    $email_valide = false;
    $i = 0;
    while(!$email_valide)
        $email_valide = filter_var($email[$i++], FILTER_VALIDATE_EMAIL);
    
    if ($email_valide)
    {
        $token = new Token();
        
        $entite->update_meta("token", $token->token);
        $entite->update_meta("token_time", 0);
        
        $corps_email_modele = "Bonjour %s,\n\nvoici votre lien privé pour accéder à la page concernant votre session de formation « %s ».\n%s\n\nCe message est automatique et vous a été envoyé par %s";
        $sujet_email_modele = "%s — Lien d'accès pour la session %s";
        
        $sujet_email = sprintf($sujet_email_modele, $wpof->of_nom, $session_titre);
        $corps_email = sprintf($corps_email_modele, $nom, $session_titre, get_site_url()."/".$wpof->url_acces."/?t=".$token->token, $wpof->of_nom);
        $message = new Message(array('to' => $email, 'subject' => $sujet_email, 'content' => $corps_email));
        $res = $message->sendmail();
        if ($res)
            $reponse['succes'] = sprintf(__("Lien envoyé par courriel à %s"), join(" ", $email));
        else
            $reponse['erreur'] = __("Problème d'envoi du courriel");
        
        $reponse['token'] = $token->token;
    }
    else
        $reponse['erreur'] = __("Email non valide");
        
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_delete_token', 'delete_token');
function delete_token()
{
    $reponse = array();
    if ($_POST['object'] == "Client")
        $entite = get_client_by_id($_POST['session_id'], $_POST['id']);
    else
        $entite = get_stagiaire_by_id($_POST['session_id'], $_POST['id']);
    
    if ($entite == null)
        $reponse['erreur'] = sprintf(__("Impossible d'identifier le détenteur du jeton : %s id %d pour la session d'id %d"), $_POST['object'], $_POST['id'], $_POST['session_id']);
    
    $entite->delete_meta("token");
    $entite->delete_meta("token_time");
    $reponse['succes'] = __("Jeton supprimé");
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_reset_token_with_email', 'reset_token_with_email');
add_action('wp_ajax_nopriv_reset_token_with_email', 'reset_token_with_email');
function reset_token_with_email($session_id = null, $email = null)
{
    global $wpdb, $wpof, $suffix_client, $suffix_session_stagiaire;
    
    if (isset($_POST['action']))
    {
        $session_id = $_POST['session_id'];
        $email = $_POST['email'];
    }
    
    if ($session_id == null || filter_var($email, FILTER_VALIDATE_EMAIL) == false)
        die();
    
    $message = array();
    $corps_email_modele = "Bonjour %s,\n\nvous venez de demander un nouveau code d'accès pour votre session de formation « %s ». Le voici :\n%s\n\nCe message est automatique et vous a été envoyé par %s";
    
    $sujet_email_modele = "%s — Nouveau lien d'accès pour la session %s";
    
    $query = $wpdb->prepare("SELECT client_id FROM ".$wpdb->prefix.$suffix_client." WHERE (meta_key = 'contact_email' OR meta_key = 'responsable_email') AND meta_value = '%s' AND session_id = '%d';", $email, $session_id);
    
    $dest_email = $sujet_email = $corps_email = null;
    
    $client_data = $wpdb->get_row($query);
    
    if ($client_data != null)
    {
        $client = get_client_by_id($session_id, $client_data->client_id);
        $dest_email = array($client->contact_email, $client->responsable_email);
        
        $token = new Token();
        $client->update_meta("token", $token->token);
        $client->update_meta("token_time", 0);
        $session_titre = get_the_title($client->session_formation_id);
        
        $sujet_email = sprintf($sujet_email_modele, $wpof->of_nom, $session_titre);
        $corps_email = sprintf($corps_email_modele, $client->contact, $session_titre, get_site_url()."/".$wpof->url_acces."/?t=".$token->token, $wpof->of_nom);
    }
    else
    {
        $query = $wpdb->prepare("SELECT stagiaire_id FROM ".$wpdb->prefix.$suffix_session_stagiaire." WHERE meta_key = 'email' AND meta_value = '%s' AND session_id = '%d';", $email, $session_id);
        
        $stagiaire_data = $wpdb->get_row($query);
        
        if ($stagiaire_data != null)
        {
            $stagiaire = get_stagiaire_by_id($session_id, $stagiaire_data->stagiaire_id);
            $dest_email = array($stagiaire->email);
            
            $token = new Token();
            $stagiaire->update_meta("token", $token->token);
            $stagiaire->update_meta("token_time", 0);
            $session_titre = get_the_title($stagiaire->session_formation_id);
            
            $sujet_email = sprintf($sujet_email_modele, $wpof->of_nom, $session_titre);
            $corps_email = sprintf($corps_email_modele, $stagiaire->get_displayname(), $session_titre, get_site_url()."/".$wpof->url_acces."/?t=".$token->token, $wpof->of_nom);
        }
        else
            $message['erreur'] = __("Adresse email inconnue");
    }
    $message = new Message(array('to' => $dest_email, 'subject' => $sujet_email, 'content' => $corps_email));
    $res = $message->sendmail();
        
    if ($res)
        $message['succes'] = sprintf(__("Le lien d'accès a été envoyé à %s"), join(" ", $dest_email));
    else
        $message['erreur'] = sprintf(__("Attention ! Le lien d'accès n'a pas été envoyé à %s"), join(" ", $dest_email));
    
    if (!isset($_POST['action']))
        return $message;
    else
    {
        echo json_encode($message);
        die();
    }
}
