<?php
/*
 * class-client.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

class Client
{
    public $adresse = "";
    public $code_postal = "";
    public $ville = "";
    public $pays = "France";
    public $telephone = "";
    public $nom = "";
    
    public $entite_client = "morale"; // personne morale ou physique
    
    public $contact = "";
    public $contact_email = "";
    public $contact_tel = "";
    
    public $responsable = "";
    public $responsable_email = "";
    public $responsable_tel = "";
    public $responsable_fonction = "";
    
    public $rcs = "";
    public $siret = "";
    public $num_of = "";
    public $nature_formation = "";
    public $financement = "";
    public $echeancier = "";
    public $opco = "";
    public $no_dossier_opco = "";
    public $etat_session = "initial";
    
    public $numero_contrat ="";
    
    public $tarif_heure = 0;
    public $tarif_total_chiffre = 0;
    public $tarif_total_autres_chiffre = 0;
    public $acompte = 0;
    public $autres_frais = "";
    public $exe_comptable = array();
    
    public $dates_array = array();
    public $dates_texte = "";
    public $nb_heure;
    public $nb_heure_decimal = 0;
    public $nb_heure_estime;
    public $nb_heure_estime_decimal = 0;
    public $creneaux = array();
    
    public $doc_necessaire = array();
    public $doc_uid = array();
    
    public $attentes = "";
    public $date_modif_attentes = "aucune";
    public $date_entretien = "";
    
    public $stagiaires = array();
    public $nb_stagiaires = 0;
    public $nb_confirmes = 0;
    public $nb_heures_stagiaires = 0;
    public $session_formation_id = -1;
    public $id = -1;
    
    // token d'accès aux infos
    public $token = "";
    public $token_time = 0;
    public $acces = 0;
    
    // table suffix
    private $table_suffix;
    
    public function __construct($session_id = -1, $client_id = -1)
    {
        global $suffix_client;
        global $wpof;
        $this->table_suffix = $suffix_client;
        
        if ($session_id > 0)
        {
            $this->session_formation_id = $session_id;
            $session_formation = get_session_by_id($session_id);
            
            log_add(__METHOD__."({$this->nom})", "client");
            if ($client_id > 0)
            {
                $this->id = $client_id;
                global $Client;
                $Client[$client_id] = $this;
                
                $meta = $this->get_meta();
                
                foreach($meta as $m)
                {
                    if (isset($this->{$m['meta_key']}))
                    {
                        if (is_array($this->{$m['meta_key']}))
                            $this->{$m['meta_key']} = unserialize(stripslashes($m['meta_value']));
                        else
                            $this->{$m['meta_key']} = stripslashes($m['meta_value']);
                    }
                }
                
                /*$this->creneaux = array();
                
                // les créneaux actifs sont stockés dans un tableau plat : l'id du créneau en clé et la valeur 0 (inactif) ou 1 (actif)
                $creneaux_from_db = json_decode($this->get_meta("creneaux"), true);
                */
                // on définit tous les créneaux comme actifs par défaut ou si $creneaux_from_db[id] n'est pas définit
                
                if (empty($this->creneaux) && is_array($session_formation->creneaux))
                    foreach($session_formation->creneaux as $d)
                        foreach($d as $c)
                            $this->creneaux[$c->id] = 1;
                else
                    foreach($session_formation->creneaux as $d)
                        foreach($d as $c)
                            if (!isset($this->creneaux[$c->id]))
                                $this->creneaux[$c->id] = 0;

                $this->calcule_temps_session();
                
                $this->calcule_tarif();
                
                $this->init_stagiaires();
                
                $doc_contexte = 0;
                
                // cas de la sous-traitance : le client est un autre OPAC
                if ($this->financement == "opac")
                {
                    $doc_contexte = $wpof->doc_context->sous_traitance | $wpof->doc_context->client;
                    if ($this->nb_heures_stagiaires == 0)
                        $this->nb_heures_stagiaires = $this->nb_stagiaires * $this->nb_heure_estime_decimal;
                }
                elseif ($this->entite_client == "morale")
                {
                    $doc_contexte = $wpof->doc_context->morale | $wpof->doc_context->client;
                    
                    // si convention en direct, on rectifie éventuellement le nombre de stagiaires inscrit dans la BD
                    if ($this->nb_stagiaires != count($this->stagiaires))
                        $this->update_meta("nb_stagiaires", count($this->stagiaires));
                    //$this->nb_confirmes = 0;
                    foreach ($this->stagiaires as $i)
                    {
                        if (get_stagiaire_meta($i, "confirme") != 0)
                            $this->nb_confirmes++;
                    }
                }
                else
                {
                    $doc_contexte = $wpof->doc_context->physique | $wpof->doc_context->client | $wpof->doc_context->stagiaire;
                }

                // documents nécessaires
                foreach($wpof->documents->term as $doc_type => $doc)
                {
                    if (($doc->contexte & $doc_contexte & $wpof->doc_context->contrat) != 0 && ($doc->contexte & $doc_contexte & $wpof->doc_context->entite) != 0)
                        $this->doc_necessaire[] = $doc_type;
                }
                $this->doc_suffix = "c".$this->id;
            }
        }
    }
    
    
    /*
     * Création des documents
     * En effet, on n'a pas toujours besoin des documents lorsqu'on instancie une session formation
     */
    public function init_docs()
    {
        global $Documents;
        global $wpof;
        
        foreach ($this->doc_necessaire as $d)
        {
            $doc = new Document($d, $this->session_formation_id, $wpof->doc_context->client, $this->id);
            $Documents[$doc->id] = $doc;
            $this->doc_uid[] = $doc->id;
        }
    }
    
    public function init_stagiaires()
    {
        global $SessionStagiaire;
        //$this->nb_confirmes = 0;
        
        foreach($this->stagiaires as $stagiaire_id)
        {
            $SessionStagiaire[$stagiaire_id] = new SessionStagiaire($this->session_formation_id, $stagiaire_id);
            if ($SessionStagiaire[$stagiaire_id]->client_id == -1)
                $SessionStagiaire[$stagiaire_id]->update_meta("client_id", $this->id);
            //if ($SessionStagiaire[$stagiaire_id]->confirme != 0)
            //    $this->nb_confirmes++;
        }
        
        if ($this->entite_client == "physique")
        {
            if (empty($this->stagiaires))
            {
                $this->init_stagiaire_from_db();
                if (empty($this->stagiaires))
                    log_add("Erreur : client ".$this->nom." n'a pas de stagiaire");
            }
            else
            {
                $this->stagiaire = $SessionStagiaire[$this->stagiaires[0]];
                $this->nom = $this->stagiaire->get_displayname();
            }
        }
    }
    
    /*
     * Procédure de secours pour réinitialiser les stagiaires en cherchant dans la table des stagiaires le client_id courant
     */
    private function init_stagiaire_from_db()
    {
        global $wpdb, $suffix_session_stagiaire;
        
        $this->stagiaires = array();
        $table_stagiaire = $wpdb->prefix.$suffix_session_stagiaire;
        $query = $wpdb->prepare("SELECT stagiaire_id FROM $table_stagiaire WHERE meta_key = 'client_id' AND meta_value = '%d';", $this->id);
        $stagiaire = $wpdb->get_results($query);
        foreach($stagiaire as $row)
            $this->stagiaires[] = $row->stagiaire_id;
        $this->update_meta("stagiaires");
    }
    
    /*
     * renvoie la liste des stagiaires (prénom nom) sous forme de liste
     * , : séparés par des virgules
     * liste : liste à puce li sans les balises ul ou ol, à rajouter autour de cet appel
     * tableau : tableau tr td sans les balises table, à rajouter autour de cet appel
     * En fait, seule la première lettre est analysée, donc souplesse
     * 
     * Si $confirme est vrai, on ne sort que les stagiaires confirmés
     */ 
    public function get_liste_stagiaires($type_liste = ',', $confirme = false)
    {
        $liste_stagiaires = "";
        $stagiaires = array();
        
        foreach($this->stagiaires as $sid)
        {
            $s = get_stagiaire_by_id($this->session_formation_id, $sid);
            if (!$confirme || $s->confirme == 1)
                $stagiaires[] = $s->get_displayname();
        }
        
        if (!empty($stagiaires))
            switch(substr($type_liste, 0, 1))
            {
                case 'l':
                    $liste_stagiaires = "<li>".join("</li><li>", $stagiaires)."</li>";
                    break;
                case 't':
                    $liste_stagiaires = "<tr><td>".join("</td></tr><tr><td>", $stagiaires)."</td></tr>";
                    break;
                default:
                    $liste_stagiaires = join(', ', $stagiaires);
                    break;
            }
        
        return $liste_stagiaires;
    }
    
    public function get_nom()
    {
        global $wpof;
        $nom = "";
        if (champ_additionnel('numero_contrat'))
            $nom = "<strong>".$this->numero_contrat."</strong> ";
        $nom .= (empty($this->nom)) ? __("Sans nom")." ID = ".$this->id : $this->nom;
        
        return $nom;
    }
    
    public function get_nb_heure_stagiaire()
    {
        if ($this->financement != "opac")
        {
            $total = 0;
            
            foreach($this->stagiaires as $user_id)
            {
                $stagiaire = new SessionStagiaire($this->session_formation_id, $user_id);
                $total += $stagiaire->nb_heure_estime_decimal;
            }
            return $total;
        }
        else 
            return $this->nb_heures_stagiaires;
    }
    
    public function get_meta($meta_key = null)
    {
        return get_client_meta($this->id, $meta_key);
    }

    public function update_meta($meta_key, $meta_value = null)
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        if ($meta_value === null)
            $meta_value = $this->$meta_key;
        else
            $this->$meta_key = $meta_value;
            
        if (is_array($meta_value))
            $meta_value = serialize($meta_value);
        
        // si le client n'est pas encore entré dans la base on lui crée un nouvel id
        if ($this->id < 1)
            $this->id = $this->last_client_id() + 1;
        
        $query = $wpdb->prepare
        ("INSERT INTO $table (session_id, client_id, meta_key, meta_value)
            VALUES ('%d', '%d', '%s', '%s')
            ON DUPLICATE KEY UPDATE meta_value = '%s';",
            $this->session_formation_id, $this->id, $meta_key, $meta_value, $meta_value);
        
        return $wpdb->query($query);
    }
    
    public function delete_meta($meta_key)
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare ("DELETE FROM $table WHERE meta_key = '%s' AND session_id = '%d' AND client_id = '%d';", $meta_key, $this->session_formation_id, $this->id);
        
        return $wpdb->query($query);
    }
    
    // Supprime un élément d'un tableau de sous-entité (stagiaires)
    public function supprime_sous_entite($tab_name, $id)
    {
        if (isset($this->$tab_name) && is_array($this->$tab_name))
        {
            $key = array_search($id, $this->$tab_name);
            unset($this->$tab_name[$key]);
            $this->update_meta($tab_name, $this->$tab_name);
        }
    }

    // Calcule le tarif total
    public function calcule_tarif()
    {
        if ($this->nb_heure_decimal > 0)
            $this->tarif_heure = sprintf("%.2f", round($this->tarif_total_chiffre / $this->nb_heure_decimal, 2));
            
        $this->tarif_total_lettre = num_to_letter($this->tarif_total_chiffre);
        $this->tarif_total_autres_lettre = num_to_letter($this->tarif_total_autres_chiffre);
    }
    

    // Calcule le temps (en heures) de la session contractualisé avec ce client (somme de tous les créneaux actifs)
    public function calcule_temps_session()
    {
        $session_formation = get_session_by_id($this->session_formation_id);
        
        $this->temps = DateTime::createFromFormat("U", "0");
        $this->dates_array = array();
        
        foreach($session_formation->creneaux as $date => $tab_date)
            foreach($tab_date as $creno)
            {
                if ($this->creneaux[$creno->id] != 0)
                {
                    if ($creno->type != "technique")
                        $this->temps->add($creno->duree);
                    $this->dates_array[] = $date;
                }
            }
        
        $this->dates_array = array_unique($this->dates_array);
        $this->dates_texte = pretty_print_dates($this->dates_array);
        
        $h = $this->temps->format("U") / 3600;
        $m = ($this->temps->format("U") % 3600) / 60;
        $this->nb_heure_decimal = $this->temps->format("U") / 3600;
        $this->nb_heure = sprintf("%02d:%02d", $h, $m);
        
        if ($this->nb_heure_estime_decimal == 0)
        {
            $this->nb_heure_estime_decimal = $this->nb_heure_decimal;
            $this->nb_heure_estime = $this->nb_heure;
        }
        else
        {
            $h = (integer) $this->nb_heure_estime_decimal;
            $m = ($this->nb_heure_estime_decimal - $h) * 60;
            $this->nb_heure_estime = sprintf("%02d:%02d", $h, $m);
        }
    }
    
    
    public function delete()
    {
        // supression du client du tableau clients de la session
        $session = get_session_by_id($this->session_formation_id);
        
        foreach($this->stagiaires as $user_id)
        {
            $stagiaire = get_stagiaire_by_id($this->session_formation_id, $user_id);
            $stagiaire->delete();
        }
        
        $session->supprime_sous_entite("clients", $this->id);
        
        // suppression du client dans la base de données
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare
        ("DELETE FROM $table
            WHERE client_id = '%d';",
            $this->id
        );
        
        return $wpdb->query($query);
    }
    
    public function last_client_id()
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = "SELECT MAX(`client_id`) FROM $table;";
        return $wpdb->get_var($query);
    }
    
    public function get_the_board()
    {
        global $SessionFormation;
        global $SessionStagiaire;
        global $wpof;
        
        $current_user_id = get_current_user_id();
        $role = wpof_get_role($current_user_id);
        
        $session = get_session_by_id($this->session_formation_id);
        $this->init_docs();
        
        ob_start();
        
        ?>
        <div id="tab-c<?php echo $this->id; ?>" class="board board-client edit-data client-<?php echo $this->id; ?>">
            <?php if ($this->financement == "opac") : ?>
            <p class="bg-alerte center"><?php _e("Vous êtes sous-traitant⋅e de ce client"); ?></p>
            <?php elseif ($this->entite_client == "physique") : ?>
            <p><?php _e("Ce client est une personne physique (un particulier)"); ?></p>
            <?php else : ?>
            <p><?php _e("Ce client est une personne morale"); ?></p>
            <?php endif; ?>
            
            
            <div class="infos-client flexrow">
            <?php
            $infos_client_func = "the_infos_client_".$this->entite_client;
            $this->$infos_client_func(); ?>
            
            <?php if (debug && $role != 'um_formateur-trice' && $this->financement != "opac") : ?>
            <div class="action-resp">
                <?php echo get_input_jpost($this, "entite_client", array('select' => '', 'display' => 'inline', 'label' => __("Changez uniquement pour réparer une erreur ! De quel type de personne est ce client ?"), 'postprocess' => 'page-reload')); ?>
            </div>
            <?php endif; ?>
            </div>
            
            <fieldset>
                <legend><?php _e("Documents administratifs pour le client"); ?></legend>
                <?php echo get_gestion_docs($this); ?>
            </fieldset>
            
            <fieldset><legend><?php _e("Créneaux contractualisés"); ?></legend>
            <?php echo get_icone_aide("client_creneaux", __("Créneaux contractualisés")); ?>
            <?php
                if (count($session->creneaux) == 0)
                    echo "<p class='alerte'>".__("Définissez d'abord des créneaux")."</p>";
                else
                    echo $session->get_html_creneaux(false, $this);
            ?>
            </fieldset>
            <?php if ($this->entite_client == "physique") : ?>
            <fieldset>
                <legend><?php _e("Évaluations et besoins"); ?></legend>
                <?php echo $this->stagiaire->get_bilan_eval(); ?>
            </fieldset>
            
            <?php endif; ?>
        </div>
        <?php
        return ob_get_clean();
    }
    
    public function the_board()
    {
        echo $this->get_the_board();
    }
    
    // Infos clients personne morale
    public function the_infos_client_morale()
    {
        global $wpof;
        
        $role = wpof_get_role(get_current_user_id());
        
        ?>
        <div class="coordonnees egalise">
        <?php
            if (champ_additionnel('numero_contrat'))
                echo get_input_jpost($this, "numero_contrat", array('input' => 'text', 'label' => __("Numéro de contrat")));
            
            echo get_input_jpost($this, "nom", array('input' => 'text', 'label' => __("Nom"), 'postprocess' => 'update_client_nom'));
            echo get_input_jpost($this, "adresse", array('textarea' => '', 'label' => __("Adresse")));
            echo get_input_jpost($this, "code_postal", array('input' => 'text', 'label' => __("Code postal")));
            echo get_input_jpost($this, "ville", array('input' => 'text', 'label' => __("Ville")));
            echo get_input_jpost($this, "pays", array('input' => 'text', 'label' => __("Pays")));
            echo get_input_jpost($this, "telephone", array('input' => 'text', 'label' => __("Téléphone")));
            echo get_input_jpost($this, "rcs", array('input' => 'text', 'label' => __("RCS")));
            echo get_input_jpost($this, "siret", array('input' => 'text', 'label' => __("Siret")));
            if ($this->financement == 'opac')
                echo get_input_jpost($this, "num_of", array('input' => 'text', 'label' => __("Numéro de déclaration d'activité de formation")));
            
            echo "<hr />";
            
            echo get_input_jpost($this, "contact", array('input' => 'text', 'label' => __("Contact")));
            echo get_input_jpost($this, "contact_email", array('input' => 'text', 'label' => __("E-mail contact")));
            echo get_input_jpost($this, "contact_tel", array('input' => 'text', 'label' => __("Téléphone contact")));
            
            echo "<hr />";
            
            // le responsable client est nécessaire s'il y a un document à signer par le client
            $responsable_necessaire = false;
            foreach($this->doc_necessaire as $doc)
            {
                if ($wpof->documents->term[$doc]->signature & Document::VALID_CLIENT_NEED)
                {
                    $responsable_necessaire = true;
                    break;
                }
            }
            if ($responsable_necessaire)
            {
                echo get_input_jpost($this, "responsable", array('input' => 'text', 'label' => __("Responsable signataire")));
                echo get_input_jpost($this, "responsable_fonction", array('input' => 'text', 'label' => __("Fonction responsable")));
                echo get_input_jpost($this, "responsable_email", array('input' => 'text', 'label' => __("E-mail responsable")));
                echo get_input_jpost($this, "responsable_tel", array('input' => 'text', 'label' => __("Téléphone responsable")));
            }
        ?>
        </div>
        
        <div class="bpf">
        <?php
            $financement_args = array('select' => '', 'label' => __("Principale source de financement"), 'postprocess' => 'toggle_opco');
            if ($this->financement == "opac") $financement_args["readonly"] = 1;
            echo get_input_jpost($this, "financement", $financement_args);
            
            $opco_jpost_args = array('select' => '', 'label' => __("Opérateur de compétences"), 'first' => 'Autre', 'class' => 'toggle-opco');
            $no_dossier_opco_jpost_args = array('input' => 'text', 'label' => __("Numéro de dossier chez l'OPCO"), 'class' => 'toggle-opco');
            if (!in_array($this->financement, array("mutu8", "mutu7")))
            {
                $opco_jpost_args['display'] = 'none';
                $no_dossier_opco_jpost_args['display'] = 'none';
            }
            echo get_input_jpost($this, "opco", $opco_jpost_args);
            echo get_input_jpost($this, "no_dossier_opco", $no_dossier_opco_jpost_args);
                
            if ($this->financement != 'opac')
                echo get_input_jpost($this, "nature_formation", array('select' => '', 'label' => __("Objectif de la prestation")));
            
            $taxe = ($wpof->of_hastva) ? __("HT") : __("TTC");
            echo get_input_jpost($this, "tarif_total_chiffre", array('input' => 'number', 'step' => '0.01', 'label' => __("Tarif total en ").$wpof->monnaie." ".$taxe, 'postprocess' => 'update_pour_infos_client+update_pour_infos_session'));
            echo get_input_jpost($this, "tarif_total_autres_chiffre", array('input' => 'number', 'step' => '0.01', 'label' => __("Dont autres frais non pédagogiques en ").$wpof->monnaie." ".$taxe, 'postprocess' => 'update_pour_infos_client'));
            echo get_input_jpost($this, "acompte", array('input' => 'number', 'step' => '0.01', 'label' => __("Acompte "),
                                                         'after' => '<span title="'.sprintf(__("Appliquer un acompte de %d %%"), $wpof->acompte_pourcent).'" class="icone-bouton set-acompte" data-acompte="'.$wpof->acompte_pourcent.'">'.$wpof->acompte_pourcent.' %</span>'));
            echo get_input_jpost($this, "autres_frais", array('input' => 'text', 'label' => __("Nature des autres frais")));
            
            if ($this->financement == "opac")
            {
                echo get_input_jpost($this, "nb_stagiaires", array('input' => 'number', 'step' => 1, 'min' => 0, 'label' => 'Nombre de stagiaires'));
                echo get_input_jpost($this, "nb_heures_stagiaires", array('input' => 'number', 'step' => 1, 'min' => 0, 'label' => "Nombre d'heures × stagiaires"));
            }
            echo get_input_jpost($this, "nb_heure_estime_decimal", array('input' => 'number', 'step' => 0.25, 'min' => 0, 'label' => "Nombre d'heures estimé"));
            
            if ($this->financement != "opac") : ?>
            <div>
                <h3><?php _e("Analyse des besoins"); ?></h3>
                <?php echo $this->get_attentes(); ?>
            </div>
            <?php endif; ?>

        </div>
        <div class="pour-infos">
            <?php echo $this->get_pour_infos_box(); ?>
        </div>
        <div class="icones">
            <?php if ($this->financement != "opac") : ?>
            <p class="icone-bouton dynamic-dialog" data-function="new_stagiaire" data-clientid="<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>"><span class="dashicons dashicons-plus-alt" > </span> <?php _e("Ajouter stagiaire"); ?></p>
            <?php echo get_icone_aide("inscrire_stagiaires", __("Stagiaires")); ?>
            <?php else: ?>
            <?php echo get_icone_aide("sous_traitance_stagiaires", __("Stagiaires")); ?>
            <?php endif; ?>
            
            <p class="token-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>" data-parent=".client-<?php echo $this->id; ?>">
            <span class="dashicons dashicons-admin-network" > </span>
            <?php _e("Envoyer accès privé"); ?>
            </p><?php echo get_icone_aide("envoi_token", "Accès privé"); ?>
            
            <p class="dynamic-dialog icone-bouton" data-function="copy_move_client" data-clientid="<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>">
            <span class="dashicons dashicons-controls-forward" > </span>
            <?php _e("Copier/déplacer ce client"); ?>
            </p>
            
            <p class="delete-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>" data-parent=".client-<?php echo $this->id; ?>">
            <span class="dashicons dashicons-dismiss" > </span>
            <?php _e("Supprimer ce client"); ?>
            </p>
            
            <?php if ($role == "admin") : ?>
            <p>ID : <?php echo $this->id; ?></p>
            <p class="icone-bouton dynamic-dialog" data-function="sql_session_formation" data-clientid="<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>"><span class="dashicons dashicons-plus-alt" > </span> <?php _e("SQL client"); ?></p>
            <?php endif; ?>
        </div>
        
        
        <?php
    }
    
    // infos client persone physique
    public function the_infos_client_physique()
    {
        global $wpof;
        
        $role = wpof_get_role(get_current_user_id());
        
        ?>
        <div class="coordonnees egalise">
        <?php
            if (champ_additionnel('numero_contrat'))
                echo get_input_jpost($this, "numero_contrat", array('input' => 'text', 'label' => __("Numéro de contrat")));
            
            echo get_input_jpost($this->stagiaire, "prenom", array('input' => 'text', 'label' => __("Prénom"), 'postprocess' => 'update_client_nom'));
            echo get_input_jpost($this->stagiaire, "nom", array('input' => 'text', 'label' => __("Nom"), 'postprocess' => 'update_client_nom'));
            echo get_input_jpost($this->stagiaire, "email", array('input' => 'text', 'label' => __("Adresse email")));
            
            echo get_input_jpost($this, "adresse", array('textarea' => '', 'label' => __("Adresse")));
            echo get_input_jpost($this, "code_postal", array('input' => 'text', 'label' => __("Code postal")));
            echo get_input_jpost($this, "ville", array('input' => 'text', 'label' => __("Ville")));
            echo get_input_jpost($this, "pays", array('input' => 'text', 'label' => __("Pays")));
            echo get_input_jpost($this, "telephone", array('input' => 'text', 'label' => __("Téléphone")));
        ?>
        </div>
        
        <div class="bpf">
        <?php
            $financement_args = array('select' => '', 'label' => __("Principale source de financement"), 'postprocess' => 'toggle_opco');
            echo get_input_jpost($this, "financement", $financement_args);
            echo get_input_jpost($this, "nature_formation", array('select' => '', 'label' => __("Objectif de la prestation")));
            
            $taxe = ($wpof->of_hastva) ? __("HT") : __("TTC");
            echo get_input_jpost($this, "tarif_total_chiffre", array('input' => 'number', 'step' => '0.01', 'label' => __("Tarif total en ").$wpof->monnaie." ".$taxe, 'postprocess' => 'update_pour_infos_client+update_pour_infos_session'));
            echo get_input_jpost($this, "tarif_total_autres_chiffre", array('input' => 'number', 'step' => '0.01', 'label' => __("Dont autres frais non pédagogiques en ").$wpof->monnaie." ".$taxe, 'postprocess' => 'update_pour_infos_client'));
            echo get_input_jpost($this, "acompte", array('input' => 'number', 'step' => '0.01', 'label' => __("Acompte "),
                                                         'after' => '<span title="'.sprintf(__("Appliquer un acompte de %d %%"), $wpof->acompte_pourcent).'" class="icone-bouton set-acompte" data-acompte="'.$wpof->acompte_pourcent.'">'.$wpof->acompte_pourcent.' %</span>'));
            echo get_input_jpost($this, "autres_frais", array('input' => 'text', 'label' => __("Nature des autres frais")));
            echo get_input_jpost($this, "echeancier", array('label' => __("Échéancier de paiement après l'acompte"), 'textarea' => ''));
            echo get_input_jpost($this->stagiaire, "nb_heure_estime_decimal", array('input' => 'number', 'step' => 0.25, 'min' => 0, 'label' => "Nombre d'heures estimé"));
            echo get_input_jpost($this->stagiaire, "statut_stagiaire", array('select' => '', 'label' => __("Statut du stagiaire")));
            echo get_input_jpost($this->stagiaire, "confirme", array('input' => 'checkbox', 'label' => __("Inscription confirmée")));
            
            ?>
        </div>
        <div class="pour-infos">
            <?php echo $this->get_pour_infos_box(); ?>
        </div>
        <div class="icones">
            <p class="token-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>" data-parent=".client-<?php echo $this->id; ?>">
            <span class="dashicons dashicons-admin-network" > </span>
            <?php _e("Envoyer accès privé"); ?>
            </p><?php echo get_icone_aide("envoi-token", "Accès privé"); ?>
            
            <p class="dynamic-dialog icone-bouton" data-function="copy_move_client" data-clientid="<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>">
            <span class="dashicons dashicons-controls-forward" > </span>
            <?php _e("Copier/déplacer ce client"); ?>
            </p>
            
            <p class="delete-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>" data-parent=".client-<?php echo $this->id; ?>">
            <span class="dashicons dashicons-dismiss" > </span>
            <?php _e("Supprimer ce client"); ?>
            </p>
            
            <?php if ($role == "admin") : ?>
            <p>ID : <?php echo $this->id; ?></p>
            <p class="icone-bouton dynamic-dialog" data-function="sql_session_formation" data-clientid="<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>"><span class="dashicons dashicons-plus-alt" > </span> <?php _e("SQL client"); ?></p>
            <?php endif; ?>
        </div>
        
        
        <?php
        
    }
    
    public function get_pour_infos_box()
    {
        global $wpof;
        ob_start();
        ?>
            <h3><?php _e("Pour information"); ?></h3>
            <p><?php _e("Dates concernées"); ?> : <span class="dates_concernees"><?php echo (empty($this->dates_texte)) ? __("aucune, cochez des créneaux pour choisir les dates") : $this->dates_texte; ?></span></p> 
            <p><?php _e("Tarif horaire"); ?> : <span class="tarif_heure"><?php echo get_tarif_formation($this->tarif_heure); ?></span> </p>
            <p><?php _e("Nombre d'heures"); ?> : <span class="nb_heure"><?php echo $this->nb_heure; ?></span> </p>
            <?php if ($this->acompte > 0) : ?>
            <p><?php _e("Acompte"); ?> : <span class="acompte"><?php echo get_tarif_formation($this->acompte); ?></span> </p>
            <?php endif; ?>
            <p><?php _e("Tarif total"); ?> : <span class="tarif_total"><?php echo get_tarif_formation($this->tarif_total_chiffre); ?></span> </p>
            <p><?php _e("Tarif total (lettres)"); ?> : <span class="tarif_total_lettre"><?php echo $this->tarif_total_lettre; ?></span> </p>
            <?php if ($this->tarif_total_autres_chiffre > 0) : ?>
            <p><?php _e("Dont autres frais"); ?> : <span class="tarif_total_autres_lettre"><?php echo $this->tarif_total_autres_lettre; ?></span> </p>
            <?php endif; ?>
        <?php
        return ob_get_clean();
    }
    
    public function get_attentes()
    {
        $role = wpof_get_role(get_current_user_id());
        
        $html = "";
        
        $html .= "<div class='attentes'>";
        $html .= get_input_jpost($this, "attentes", array('textarea' => 1, 'rows' => '10', 'cols' => '60', 'label' => __("Attentes, besoins, motivations"), 'postprocess' => 'update_data', 'ppargs' => array('ppkey' => 'date_modif_attentes', 'ppvalue' => date("d/m/Y", time()), 'ppdest' => "#date_modif_attentes{$this->id}")));
        if ($role != null)
            $html .= get_input_jpost($this, "date_entretien", array("input" => "datepicker", "label" => "Date de l'entretien", "inline" => 1));
        $html .= "<p>".__("Date de dernière modification des attentes")." : <span id='date_modif_attentes{$this->id}'>{$this->date_modif_attentes}</span></p>";
        $html .= "</div>";
        
        return $html;
    }
    
    /*
     * Interface avec le client
     */
    public function the_interface()
    {
        echo $this->get_the_interface();
    }
    
    public function get_the_interface()
    {
        global $wpof;
        $session_formation = get_session_by_id($this->session_formation_id);
        
        ob_start();
        
        echo hidden_input("default_main_tab", (isset($_SESSION['main-tabs'])) ? $_SESSION['main-tabs'] : 0);
        ?>
        <p><strong><?php echo $this->nom; ?></strong> <?php if (!empty($this->contact)) echo __("représenté par")." ".$this->contact; ?></p>
        <h2><?php echo $session_formation->titre_formation; ?></h2>
        <p><strong><?php _e("Dates"); ?></strong> <?php echo $session_formation->dates_texte; ?></p>
        <div id="main-tabs">
            <ul>
                <li><a href="#tab-presentation"><?php _e("La formation"); ?></a></li>
                <li><a href="#tab-planning"><?php _e("Planning"); ?></a></li>
                <li><a href="#tab-documents"><?php _e("Documents"); ?></a></li>
            </ul>
            
            <div id="tab-presentation" class="client" data-id="<?php echo $this->id; ?>">
            <?php echo $session_formation->get_html_presentation(array('entite' => $this)); ?>
            </div> <!-- presentation -->
            
            <div id="tab-planning" class="client" data-id="<?php echo $this->id; ?>">
            <?php echo $session_formation->get_decoupage_temporel($this->creneaux); ?>
            </div> <!-- planning -->
            
            <div id="tab-documents">
                <?php
                global $Documents;
                $this->init_docs();
                $liste_doc = "";
                foreach ($this->doc_uid as $docuid)
                {
                    $doc = $Documents[$docuid];
                    if ($doc->visible)
                        $liste_doc .= "<li class='doc {$doc->type}'>{$doc->link_name}</li>";
                }
                if ($liste_doc != "") : ?>
                <ul class="liste-doc-stagiaire">
                <?php echo $liste_doc; ?>
                </ul>
                <?php else : ?>
                <p><?php echo $wpof->doc_pas_de_docs; ?></p>
                <?php endif; ?>
            </div> <!-- documents -->
        
        </div>
        
        <?php
        return ob_get_clean();
    }
    
    /*
     * retourne le nom du client
     * pour compatibilité avec les classes Stagiaire et Formateur
     */
    public function get_displayname()
    {
        return $this->nom;
    }
    
    public function get_doc_est_personne_morale($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $personne = 1;
        if ($arg == 'valeur')
            $personne = ($this->entite_client == "morale") ? 1 : 0;
        $result = array('valeur' => $personne, 'tag' => "client:$function_name", 'desc' => __("Le client est-il une personne morale ?"));
        return $result[$arg];
    }
    public function get_doc_est_personne_physique($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $personne = 1;
        if ($arg == 'valeur')
            $personne = ($this->entite_client == "physique") ? 1 : 0;
        $result = array('valeur' => $personne, 'tag' => "client:$function_name", 'desc' => __("Le client est-il une personne physique ?"));
        return $result[$arg];
    }
    public function get_doc_nom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Raison sociale du client (ou prénom nom si particulier)"));
        return $result[$arg];
    }
    public function get_doc_adresse($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Adresse postale"));
        return $result[$arg];
    }
    public function get_doc_code_postal($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Code postal"));
        return $result[$arg];
    }
    public function get_doc_ville($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Ville"));
        return $result[$arg];
    }
    public function get_doc_pays($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $pays = ($this->pays != "France") ? $this->pays : ""; // on n'affiche le pays que s'il est différent de France
        $result = array('valeur' => $pays, 'tag' => "client:$function_name", 'desc' => __("Pays (affiché seulement si différent de France)"));
        return $result[$arg];
    }
    public function get_doc_telephone($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Numéro de téléphone"));
        return $result[$arg];
    }
    public function get_doc_contact($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Nom du contact chez le client"));
        return $result[$arg];
    }
    public function get_doc_contact_email($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Email du contact"));
        return $result[$arg];
    }
    public function get_doc_contact_tel($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Numéro de téléphone du contact"));
        return $result[$arg];
    }
    public function get_doc_responsable($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Prénom et nom du/de la responsable qui signe les documents"));
        return $result[$arg];
    }
    public function get_doc_responsable_email($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Email du/de la responsable qui signe les documents"));
        return $result[$arg];
    }
    public function get_doc_responsable_tel($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Numéro de téléphone du/de la responsable qui signe les documents"));
        return $result[$arg];
    }
    public function get_doc_responsable_fonction($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Fonction du/de la responsable qui signe les documents"));
        return $result[$arg];
    }
    public function get_doc_rcs($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Numéro de RCS"));
        return $result[$arg];
    }
    public function get_doc_siret($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Numéro de Siret"));
        return $result[$arg];
    }
    public function get_doc_num_of($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Numéro de déclaration d’activité (le client est un organisme de formation)"));
        return $result[$arg];
    }
    public function get_doc_nature_formation($arg = 'valeur')
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $valeur = "";
        if ($arg == 'valeur')
        {
            if ($wpof->$function_name->is_term($this->$function_name))
                $valeur = $wpof->$function_name->get_term($this->$function_name);
        }
        $result = array('valeur' => $valeur, 'tag' => "client:$function_name", 'desc' => __("Objectif de la prestation (cf. BPF)"));
        return $result[$arg];
    }
    public function get_doc_financement($arg = 'valeur')
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $valeur = "";
        if ($arg == 'valeur')
        {
            if ($wpof->$function_name->is_term($this->$function_name))
                $valeur = $wpof->$function_name->get_term($this->$function_name);
        }
        $result = array('valeur' => $valeur, 'tag' => "client:$function_name", 'desc' => __("Mode de financement (cf. BPF)"));
        return $result[$arg];
    }
    public function get_doc_echeancier($arg = 'valeur')
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => wpautop($this->$function_name), 'tag' => "client:$function_name", 'desc' => __("Échéancier de paiement après l'acompte (pour un particulier)"));
        return $result[$arg];
    }
    public function get_doc_opco($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("OPCO du client"));
        return $result[$arg];
    }
    public function get_doc_no_dossier_opco($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Numéro de dossier chez l’OPCO du client"));
        return $result[$arg];
    }
    public function get_doc_numero_contrat($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Numéro de contrat interne pour ce client"));
        return $result[$arg];
    }
    public function get_doc_tarif_heure($arg = 'valeur', $taxe = 'HT')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $tarif = "";
        if ($arg == 'valeur')
            $tarif = get_tarif_taxe($this->$function_name, $taxe);
        $result = array('valeur' => $tarif, 'tag' => "client:$function_name", 'desc' => __("Tarif horaire (HT ou TTC affiché par le mot-clé)"));
        return $result[$arg];
    }
    public function get_doc_tarif_stagiaire($arg = 'valeur', $taxe = 'HT')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $tarif = "";
        if ($arg == 'valeur')
        {
            if ($this->nb_confirmes > 0)
                $tarif = get_tarif_taxe($this->tarif_total_chiffre / $this->nb_confirmes, $taxe);
            else
                $tarif = __("Non applicable");
        }
        $result = array('valeur' => $tarif, 'tag' => "client:$function_name", 'desc' => __("Tarif par stagiaire (HT ou TTC affiché par le mot-clé)"));
        return $result[$arg];
    }
    public function get_doc_tarif_total_chiffre($arg = 'valeur', $taxe = 'HT')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $tarif = "";
        if ($arg == 'valeur')
            $tarif = get_tarif_taxe($this->$function_name, $taxe);
        $result = array('valeur' => $tarif, 'tag' => "client:$function_name", 'desc' => __("Tarif total de la prestation (HT ou TTC affiché par le mot-clé)"));
        return $result[$arg];
    }
    public function get_doc_acompte($arg = 'valeur', $taxe = 'HT')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $tarif = "";
        if ($arg == 'valeur')
            $tarif = get_tarif_taxe($this->$function_name, $taxe);
        $result = array('valeur' => $tarif, 'tag' => "client:$function_name", 'desc' => __("Acompte (HT ou TTC affiché par le mot-clé)"));
        return $result[$arg];
    }
    public function get_doc_solde($arg = 'valeur', $taxe = 'HT')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $tarif = "";
        if ($arg == 'valeur')
            $tarif = get_tarif_taxe($this->tarif_total_chiffre - $this->acompte, $taxe);
        $result = array('valeur' => $tarif, 'tag' => "client:$function_name", 'desc' => __("Solde (HT ou TTC affiché par le mot-clé)"));
        return $result[$arg];
    }
    public function get_doc_tarif_total_autres_chiffre($arg = 'valeur', $taxe = 'HT')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $tarif = "";
        if ($arg == 'valeur')
            $tarif = get_tarif_taxe($this->$function_name, $taxe);
        $result = array('valeur' => $tarif, 'tag' => "client:$function_name", 'desc' => __("Montant des frais non pédagogiques (HT ou TTC affiché par le mot-clé)"));
        return $result[$arg];
    }
    public function get_doc_autres_frais($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Nature des frais non pédagogiques"));
        return $result[$arg];
    }
    public function get_doc_nb_heure($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Nombre d’heures"));
        return $result[$arg];
    }
    public function get_doc_nb_heure_estime($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Nombre d’heures estimé"));
        return $result[$arg];
    }
    public function get_doc_attentes($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Attentes, besoins"));
        return $result[$arg];
    }
    public function get_doc_dates($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->dates_texte, 'tag' => "client:$function_name", 'desc' => __("Dates contractuelles concernées"));
        return $result[$arg];
    }
/*    public function get_doc_creneaux($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Créneaux contractualisés avec le client"));
        return $result[$arg];
    }*/
    public function get_doc_stagiaires($arg = 'valeur', $type_liste = ',')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $liste_stagiaires = '';
        if ($arg == 'valeur')
        {
            $liste_stagiaires = $this->get_liste_stagiaires($type_liste);
            switch(substr($type_liste, 0, 1))
            {
                case 't':
                    $liste_stagiaires = "<table>".$liste_stagiaires."</table>";
                    break;
                case 'l':
                    $liste_stagiaires = "<ul>".$liste_stagiaires."</ul>";
                    break;
                case ',':
                default:
                    break;
            }
        }
        
        $result = array('valeur' => $liste_stagiaires, 'tag' => "client:$function_name", 'desc' => __("Liste des stagiaires. En paramètre indiquez « liste » pour une liste à puce, « tableau » pour un tableau ou rien pour avoir les noms séparés par des virgules"));
        return $result[$arg];
    }
    public function get_doc_stagiaires_confirmes($arg = 'valeur', $type_liste = ',')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $liste_stagiaires = '';
        if ($arg == 'valeur')
        {
            $liste_stagiaires = $this->get_liste_stagiaires($type_liste, true);
            switch(substr($type_liste, 0, 1))
            {
                case 't':
                    $liste_stagiaires = "<table>".$liste_stagiaires."</table>";
                    break;
                case 'l':
                    $liste_stagiaires = "<ul>".$liste_stagiaires."</ul>";
                    break;
                case ',':
                default:
                    break;
            }
        }
        
        $result = array('valeur' => $liste_stagiaires, 'tag' => "client:$function_name", 'desc' => __("Liste des stagiaires confirmés. En paramètre indiquez « liste » pour une liste à puce, « tableau » pour un tableau ou rien pour avoir les noms séparés par des virgules"));
        return $result[$arg];
    }
    public function get_doc_nb_confirmes($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "client:$function_name", 'desc' => __("Nombre de stagiaires confirmés"));
        return $result[$arg];
    }
    public function get_doc_emargement_collectif_vierge($arg = 'valeur', $nb_jour_max_par_page = 1, $premier_jour = 0)
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $tableau = "";
        if ($arg == 'valeur')
            $tableau = $this->get_doc_emargement_collectif_stagiaires('vierge', $nb_jour_max_par_page, $premier_jour);
        $result = array('valeur' => $tableau, 'tag' => "client:$function_name", 'desc' => __("Tableau vierge pour des feuilles d'émargement vierges (précisez le nombre de jours maximum par page en dernier paramètre)"));
        return $result[$arg];
    }
    public function get_doc_emargement_collectif_stagiaires($arg = 'valeur', $nb_jour_max_par_page = 1, $premier_jour = 0, $sp = 0)
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        
        $tableau = "";
        if (in_array($arg, array('valeur', 'vierge')))
        {
            $session = get_session_by_id($this->session_formation_id);
            $nb_stagiaires = 0;
            if ($arg != 'vierge')
            {
                global $SessionStagiaire;
                $this->init_stagiaires();
                $nb_stagiaires = count($SessionStagiaire);
            }
            
            $dates = array_keys($session->creneaux);
            $nb_jour_max_par_page = min($nb_jour_max_par_page, count($dates) - $premier_jour);
            $cases_stagiaires = "";
            $cases_formateurs = "";
            
            //return var_export($dates, true);
            ob_start();
            ?>
            <table class="tableau-stagiaires">
            <tr><th rowspan="2" class="stagiaire"><?php _e("Prénom, nom des stagiaires"); ?></th>
            <?php 
            for($i = $premier_jour; $i < $premier_jour + $nb_jour_max_par_page; $i++)
            {
                $colspan = count($session->creneaux[$dates[$i]]); // nombre de créneau dans la journée
                ?>
                <th class="plage" colspan="<?php echo $colspan; ?>"><?php echo $dates[$i]; ?></th>
                <?php
            }
            ?>
            <th rowspan="2" class="col-nb-heures"><?php _e("Nombre d'heures total"); ?></th>
            </tr>
            <tr>
            <?php 
            for($i = $premier_jour; $i < $premier_jour + $nb_jour_max_par_page; $i++)
            {
                foreach($session->creneaux[$dates[$i]] as $creno)
                {
                    echo "<th class=\"plage {$creno->type}\">".$creno->titre."</th>";
                    $cases_stagiaires .= "<td class=\"{$creno->type} contrat{$this->creneaux[$creno->id]} case-signature\"></td>";
                    $cases_formateurs .= "<td class=\"{$creno->type} case-signature\"></td>";
                }
            }
            $cases_stagiaires .= "<td></td>"; // ajout de la case pour le nombre d'heures
            $cases_formateurs .= "<td></td>"; // ajout de la case pour le nombre d'heures
            $ligne_vierge = "<tr><td></td>$cases_stagiaires</tr>";
            ?>
            </tr>
            <?php
            if ($arg != 'vierge') :
                global $wpof;
                $max_confirmes = $wpof->nb_ligne_emargement - count($session->formateur);
                $stagiaires = array_chunk($SessionStagiaire, $max_confirmes);
                foreach($stagiaires[$sp] as $s) :
                    if ($s->confirme == 1) : ?>
                    <tr>
                    <td><?php echo $s->get_displayname(); ?> – <?php echo $this->nom; ?></td>
                    <?php echo $cases_stagiaires; ?>
                    </tr>
                    <?php endif;
                endforeach;
            endif;
            
            $nb_lignes_vierges = $wpof->nb_ligne_emargement - $nb_stagiaires - count($session->formateur);
            
            for ($i = 0; $i < $nb_lignes_vierges; $i++)
                echo $ligne_vierge;
            
            foreach($session->formateur as $fid) : ?>
            <tr class="formateur">
            <td><?php $formateur = get_formateur_by_id($fid); echo $formateur->get_displayname(); ?></td>
            <?php echo $cases_formateurs; ?>
            </tr>
            <?php endforeach; ?>
            
            </table>
            
            <?php
            $tableau .= ob_get_clean();
            $arg = 'valeur';
        }
        else
            $tableau = "";
        
        $result = array('valeur' => $tableau, 'tag' => "client:$function_name", 'desc' => __("Tableau des stagiaires du client (précisez le nombre de jours maximum par page en dernier paramètre)"));
        return $result[$arg];
    }
    public function get_doc_emargement_dates_par_page($arg = 'valeur', $nb_jour_max_par_page = 1, $premier_jour = 0, $num_sp = null)
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        
        $select_dates = array();
        if ($arg == 'valeur')
        {
            $session = get_session_by_id($this->session_formation_id);
            $dates = array_keys($session->creneaux);
            $nb_jour_max_par_page = min($nb_jour_max_par_page, count($dates) - $premier_jour);
            
            for($i = $premier_jour; $i < $premier_jour + $nb_jour_max_par_page; $i++)
                $select_dates[] = $dates[$i];
            
            $select_dates = pretty_print_dates($select_dates);
            if ($num_sp)
                $select_dates .= " (p$num_sp)";
            //$select_dates = join(", ", $select_dates);
        }
            
        $result = array('valeur' => $select_dates, 'tag' => "client:$function_name", 'desc' => __("Dates concernées par cette page de feuille d’émargement (correspond aux dates mentionnées dans le tableau)"));
        return $result[$arg];
    }
}

function get_client_meta($client_id, $meta_key)
{
    global $wpdb;
    global $suffix_client;
    $table_client = $wpdb->prefix.$suffix_client;
    
    if ($meta_key)
    {
        $query = $wpdb->prepare
        ("SELECT meta_value
            from $table_client
            WHERE client_id = '%d'
            AND meta_key = '%s';",
            $client_id, $meta_key);
        return $wpdb->get_var($query);
    }
    else
    {
        $query = $wpdb->prepare("SELECT meta_key, meta_value from $table_client WHERE client_id = '%d';", $client_id);
        return $wpdb->get_results($query, ARRAY_A);
    }
}

/*
 * Copie ou déplacement d'un client vers une autre session
 * $session_dest_id : id de la session de destination
 * $client_source_id : id du client source
 * $choix_cp_mv : copy ou move
 */
add_action('wp_ajax_copie_ou_deplace_client', 'copie_ou_deplace_client');
function copie_ou_deplace_client($session_dest_id = 0, $client_source_id = 0, $choix_cp_mv = 'copy')
{
    global $wpdb;
    global $suffix_client, $suffix_session_stagiaire;
    $table_client = $wpdb->prefix.$suffix_client;
    $table_stagiaire = $wpdb->prefix.$suffix_session_stagiaire;
    
    $reponse = array('log' => '', 'query' => array());
    
    if (isset($_POST['valid_once']))
        $reponse['valid_once'] = 1;
    
    $meta_key_copy = array
    (
        'adresse',
        'code_postal',
        'ville',
        'pays',
        'telephone',
        'nom',
        'entite_client',
        'contact',
        'contact_email',
        'contact_tel',
        'responsable',
        'responsable_email',
        'responsable_tel',
        'responsable_fonction',
        'rcs',
        'siret',
        'num_of',
    );
    
    if (isset($_POST['action']) && $_POST['action'] == __FUNCTION__)
    {
        $client_source_id = $_POST['client_id'];
        $session_source_id = $_POST['session_source_id'];
        $session_dest_id = $_POST['session_dest_id'];
        $choix_cp_mv = (isset($_POST['choix_cp_mv'])) ? $_POST['choix_cp_mv'] : "";
    }
    
    $client_dest_id = $client_source_id;
    
    if ($choix_cp_mv == "copy")
    {
        $client = get_client_by_id($session_source_id, $client_source_id);
        $client_dest_id = $client->last_client_id() + 1;
        
        $query = $wpdb->prepare("SELECT meta_key, meta_value from $table_client WHERE client_id = '%d' AND meta_key IN ('".join("','", $meta_key_copy)."');", $client_source_id);
        $client_data = $wpdb->get_results($query);
        $reponse['query'][] = $query;
        
        foreach($client_data as $data)
        {
            $query = $wpdb->prepare("INSERT INTO $table_client SET client_id = %d, session_id = %d, meta_key = '%s', meta_value = '%s';",
                $client_dest_id,
                $session_dest_id,
                $data->meta_key,
                $data->meta_value
            );
            $wpdb->query($query);
            $reponse['query'][] = $query;
        }
        if ($client->entite_client == "physique")
        {
            $stagiaire_source_id = $client->stagiaires[0];
            $stagiaire = get_stagiaire_by_id($session_source_id, $stagiaire_source_id);
            $stagiaire_dest_id = $stagiaire->get_last_id() + 1;
            $query = $wpdb->prepare("SELECT meta_key, meta_value from $table_stagiaire WHERE stagiaire_id = '%d';", $stagiaire_source_id);
            $stagiaire_data = $wpdb->get_results($query);
            
            foreach($stagiaire_data as $data)
            {
                $query = $wpdb->prepare("INSERT INTO $table_stagiaire SET session_id = %d, stagiaire_id = %d, meta_key = '%s', meta_value = '%s';",
                    $session_dest_id, $stagiaire_dest_id, $data->meta_key, $data->meta_value);
                $wpdb->query($query);
                $reponse['query'][] = $query;
            }
            $client = get_client_by_id($session_dest_id, $client_dest_id);
            $client->update_meta("stagiaires", array($stagiaire_dest_id));
        }
        $reponse['log'] .= "ajout client id $client_dest_id";
    }
    elseif ($choix_cp_mv == "move")
    {
        $client = get_client_by_id($session_source_id, $client_source_id);
        $query = $wpdb->prepare("UPDATE $table_client SET session_id = %d WHERE client_id = %d;", $session_dest_id, $client_source_id);
        $wpdb->query($query);
        $reponse['query'][] = $query;
        foreach($client->stagiaires as $stagiaire_id)
        {
            $query = $wpdb->prepare("UPDATE $table_stagiaire SET session_id = %d WHERE stagiaire_id = %d;", $session_dest_id, $stagiaire_id);
            $wpdb->query($query);
            $reponse['query'][] = $query;
        }
        $session_source = new SessionFormation($session_source_id);
        unset($session_source->clients[array_search($client_source_id, $session_source->clients)]);
        $session_source->update_meta("clients");
    }
    else
        $reponse['erreur'] = __("Vous devez choisir de copier ou déplacer ce client.");
    
    if (!isset($reponse['erreur']))
    {
        $session_dest = new SessionFormation($session_dest_id);
        $session_dest->clients[] = $client_dest_id;
        $session_dest->update_meta("clients", array_unique($session_dest->clients));
        $reponse['succes'] = sprintf(__("Allez à la session <a href=\"%s\">%s</a><br />ou cliquez sur <strong>Fermer</strong> pour rester sur celle-ci"), $session_dest->permalien, $session_dest->titre_session);
        $reponse['succes'] .= '<input type="hidden" name="deja_valide" value="1" />';
    }
    
    if (isset($_POST['action']) && $_POST['action'] == __FUNCTION__)
    {
        echo json_encode($reponse);
        die();
    }
}

function get_client_by_id($session_id, $client_id)
{
    global $Client, $global_log;
    
    $client_id = (integer) $client_id;
    
    if (!isset($Client[$client_id]))
        $Client[$client_id] = new Client($session_id, $client_id);
    
    return $Client[$client_id];
}
