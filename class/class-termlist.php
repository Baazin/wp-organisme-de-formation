<?php
/*
 * class-wpof.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

class TermList
{
    public $term = array();
    public $group = array();
    public $name;
    
    public function __construct($name)
    {
        $this->name = $name;
    }
    
    public function add_term($key, $text, $args = array('group' => null))
    {
        $this->term[$key] = (object) $args;
        $this->term[$key]->text = $text;
    }
    
    public function get_term($key)
    {
        if (!is_array($key))
            $array_key = array($key);
        else
            $array_key = $key;
        
        $array_term = array();
        
        foreach($array_key as $k)
        {
            if (isset($this->term[$k]))
                $array_term[] = $this->term[$k]->text;
            elseif ($k != -1) //if (wpof_get_role(get_current_user_id()) == "admin")
                $array_term[] = __("!!! à définir !!!");
        }
        return join(", ", $array_term);
    }
    
    public function is_term($key)
    {
        return in_array($key, array_keys($this->term));
    }
    
    public function add_group($key, $text)
    {
        $this->group[$key] = $text;
    }
    
    public function in_group($key, $group)
    {
        return ($this->term[$key]->group == $group);
    }
    
    public function get_group($g)
    {
        $groupe = new TermList($this->name."_".$g);
        
        foreach($this->term as $key => $term)
        {
            if (isset($term->group) && $term->group == $g)
                $groupe->add_term($key, $term->text);
        }
        return $groupe;
    }
    
    public function get_select_list($selected = "", $options = "", $first = null)
    {
        if (!is_array($selected))
            $selected = array($selected);
            
        $html = "<select name='$this->name' $options>";
        
        if ($first)
            $html .= "<option value='-1' class='first'><em>$first</em></option>";
        
        $group = null;
        foreach ($this->term as $key => $term)
        {
            if ($term->group != $group)
                $html .= "</optgroup>";
                
            if ($term->group && $term->group != $group)
            {
                if (isset($this->group[$term->group]))
                    $html .= "<optgroup label='".$this->group[$term->group]."'>";
                else 
                    $html .= "<optgroup label='".$term->group."'>";
            }
            $group = $term->group;
            $html .= "<option value='$key' ".selected(in_array($key, $selected), true, false).">{$term->text}</option>";
        }
        $html .= "</select>";
        
        return $html;        
    }
    
    // Renvoie un tableau avec les valeurs d'un champ particulier
    public function get_fields($field)
    {
        $fields = array();
        foreach($this->term as $key => $term)
            if (!empty($term->$field))
                $fields[$key] = $term->$field;
        return $fields;
    }
}
