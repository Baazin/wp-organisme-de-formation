<?php
/*
 * class-upload.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


class Upload
{
    public $filename = "";
    public $path = wpof_path_pdf;
    public $date_text = "";
    public $timestamp = 0;
    public $user_id = -1;
    public $session_id = -1;
    public $md5sum = -1;
    
    public function __construct($session_id = -1, $md5sum = -1)
    {
        $this->session_id = $session_id;
        $this->md5sum = $md5sum;
        
        if ($session_id > 0)
            $this->path .= "/". $session_id . "/scan/";
    }
    
    public function get_html($tag = "")
    {
        $html = "";
        global $SessionFormation;
        if (!isset($SessionFormation[$this->session_id]))
            $SessionFormation[$this->session_id] = new SessionFormation($this->session_id);
        $session_formation = $SessionFormation[$this->session_id];
        
        
        $rand_id = rand();
        
        $fields = array
        (
            $this->get_link_html(),
            $this->date_text,
            "<span class='bouton del-scan attention' data-sessionid='{$this->session_id}' data-md5sum='{$this->md5sum}' data-message='archive-message' data-conteneur='$rand_id'>".__("Supprimer")."</span>"
        );
        $tag = strtolower($tag);
        switch($tag)
        {
            case "td":
            case "tr":
                $html = "<tr id='$rand_id'><td>".join("</td><td>", $fields)."</td></tr>";
                break;
            case "ul":
            case "li":
                $html = "<li id='$rand_id'>".join(" — ", $fields)."</li>";
                break;
            default:
                $html = "<p id='$rand_id'>".join(" — ", $fields)."</p>";
                break;
        }
        return $html;
    }
    
    public function set_md5sum()
    {
        if (file_exists($this->path . $this->filename))
            $this->md5sum = md5_file($this->path . $this->filename);
    }
    
    public function get_link_html()
    {
        return "<a href='?download=&s={$this->session_id}&m={$this->md5sum}'>".$this->filename."</a>";
    }
}
