<?php
/*
 * class-wpof.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


class WPOF
{
    // Valeurs par défaut
    
    // Aide en ligne
    public $aide_file = "aide";
    public $aide_url = "https://opaga.fr/ressources/";
    
    // Traitement des pages spéciales
    public $special_page = "single";
    
    // valeurs par défaut
    public $of_adresse = "";
    public $of_code_postal = "";
    public $of_datadock = "";
    public $of_qualiopi = "";
    public $of_description = "";
    public $of_exotva = "";
    public $of_hastva = "";
    public $of_logo = "";
    public $of_nom = "";
    public $of_noof = "";
    public $of_siret = "";
    public $of_rcs = "RCS";
    public $of_tva_intracom = "";
    public $of_ape = "";
    public $of_tauxtva = "";
    public $of_telephone = "";
    public $of_email = "";
    public $of_url = "";
    public $of_ville = "";
    public $pdf_header = "";
    public $pdf_footer = "";
    public $pdf_titre_font = "sans-serif";
    public $pdf_texte_font = "sans-serif";
    public $pdf_couleur_titre_doc = "#000000";
    public $pdf_couleur_titre_autres = "#000000";
    
    public $token_validity = 24; // validité d'un token en heures
    public $token_seconds_by_unit = 3600; // nombre de secondes par unité de token_validity
    public $token_length = 40;
    public $acompte_pourcent = 30;
    
    // taxonomies WordPress
    public $use_category = 1;
    public $use_tag = 1;

    public $max_upload_size = 16; // taille maxi d'upload pour formateur⋅trices en Mo
    
    public $role;

    public function __construct()
    {
        $all_options = wp_load_alloptions();
        
        $this->aide = $this->init_aide();
        
        $this->role = wpof_get_role(get_current_user_id());
        
        foreach ($all_options as $key => $value)
        {
            if (substr($key, 0, 10) == "wpof_aide_")
            {
                $prop = str_replace("-", "_", substr($key, 10));
                $this->aide->$prop = json_decode($value);
            }
            elseif (substr($key, 0, 5) == "wpof_")
            {
                $value = apply_filters( "option_{$key}", maybe_unserialize($value), $key);
                $prop = str_replace("-", "_", substr($key, 5));
                $this->$prop = $value;
            }
        }
    }
    
    public function init_aide()
    {
        $aide_data = file_get_contents(wpof_path . $this->aide_file . ".json");
            
        return json_decode($aide_data);
    }
    
    public function export_aide($format = "json", $display = false)
    {
        $data = "";
        switch ($format)
        {
            case "json":
                $data = json_encode($this->aide, JSON_UNESCAPED_UNICODE);
                break;
            case "html":
                foreach($this->aide as $a)
                {
                    $data .= "<h2>{$a->titre}</h2>";
                    $data .= $a->texte;
                }
                break;
            default:
                break;
        }
        
        if ($display)
            echo $data;
        else
        {
            $file_name = wpof_path . $this->aide_file . "." . $format;
            $file_handle = fopen($file_name, "w");
            fwrite($file_handle, $data);
            fclose($file_handle);
            return $file_name;
        }
    }
    
    public function get_doc_nom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->of_nom, 'tag' => "of:$function_name", 'desc' => __("Nom de l’organisme de formation"));
        return $result[$arg];
    }
    public function get_doc_adresse($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->of_adresse, 'tag' => "of:$function_name", 'desc' => __("Adresse de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_code_postal($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->of_code_postal, 'tag' => "of:$function_name", 'desc' => __("Code postal de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_datadock($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->of_datadock, 'tag' => "of:$function_name", 'desc' => __("Numéro Datadock de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_qualiopi($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->of_qualiopi, 'tag' => "of:$function_name", 'desc' => __("Numéro Qualiopi de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_description($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->of_description, 'tag' => "of:$function_name", 'desc' => __("Description de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_logo($arg = 'valeur', $dimensions = "")
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $style = get_style_dimensions($dimensions);
        
        $logo = '<img src="'.str_replace('https', 'http', wp_get_attachment_url($this->of_logo)).'" '.$style.' />';
        $result = array('valeur' => $logo, 'tag' => "of:$function_name", 'desc' => __("Logo de l’organisme de formation (dimensions à ajouter sous la forme {of:logo:largxhaut} unité(s) en mm obligatoirement ; ex. {of:logo:x10} pour une hauteur de 10mm et largeur automatique)"));
        return $result[$arg];
    }

    public function get_doc_no_of($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->of_noof, 'tag' => "of:$function_name", 'desc' => __("Numéro de déclaration d'activité de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_siret($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->of_siret, 'tag' => "of:$function_name", 'desc' => __("Siret de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_ape($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->of_ape, 'tag' => "of:$function_name", 'desc' => __("Code APE de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_rcs($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->of_rcs, 'tag' => "of:$function_name", 'desc' => __("RCS de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_tva_intracom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->of_tva_intracom, 'tag' => "of:$function_name", 'desc' => __("Numéro de TVA intra-communautaire"));
        return $result[$arg];
    }

    public function get_doc_url($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->of_url, 'tag' => "of:$function_name", 'desc' => __("Lien Web vers lequel renvoyer les clients"));
        return $result[$arg];
    }

    public function get_doc_telephone($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->of_telephone, 'tag' => "of:$function_name", 'desc' => __("Numéro de téléphone de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_ville($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->of_ville, 'tag' => "of:$function_name", 'desc' => __("Ville de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_email($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->of_email, 'tag' => "of:$function_name", 'desc' => __("Adresse électronique de l’organisme de formation"));
        return $result[$arg];
    }

    public function get_doc_signataire($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $resp_nom = "";
        if ($arg == 'valeur')
        {
            $signature_pouvoir = is_signataire();
            $fonction = get_fonction_responsable();
            if (!empty($fonction))
                $fonction = ', '.$fonction;
            
            if ($signature_pouvoir !== false)
            {
                $resp = new Formateur(get_current_user_id());
                if ($resp)
                {
                    $resp_nom = $resp->get_displayname();
                    if ($signature_pouvoir == "signature_po")
                        $resp_nom .= " (P/O ".signataire_pour_nom().$fonction.")";
                    else
                        $resp_nom .= $fonction;
                }
                else
                    $resp_nom = '<span class="erreur">'.__("Vous n'êtes pas autorisé.e à signer, ce message ne devrait jamais s'afficher...").'</span>';
            }
            else
                $resp_nom = signataire_pour_nom().$fonction;
        }
        
        $result = array('valeur' => $resp_nom, 'tag' => "of:$function_name", 'desc' => __("Signataire (officiel ou sur ordre) des documents et fonction du/de la signataire officielle"));
        return $result[$arg];
    }

    public function get_doc_responsable_nom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $resp_nom = "";
        if ($arg == 'valeur')
            $resp_nom = signataire_pour_nom();
        
        $result = array('valeur' => $resp_nom, 'tag' => "of:$function_name", 'desc' => __("Responsable de formation signataire des documents"));
        return $result[$arg];
    }

    public function get_doc_responsable_fonction($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $resp_fonction = "";
        if ($arg == 'valeur')
            $resp_fonction = get_fonction_responsable();
        $result = array('valeur' => $resp_fonction, 'tag' => "of:$function_name", 'desc' => __("Fonction du/de la responsable de formation"));
        return $result[$arg];
    }
    
    public function get_doc_tauxtva($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->of_tauxtva, 'tag' => "of:$function_name", 'desc' => __("Taux de la TVA"));
        return $result[$arg];
    }
    
    public function get_doc_today($arg = 'valeur')
    {
        $date = '';
        if ($arg == 'valeur')
        {
            global $document_actuel;
            if (champ_additionnel('periode_validite') && $document_actuel->date_debut != "")
                $date = pretty_print_dates($document_actuel->date_debut);
            else
                $date = pretty_print_dates(date("d/m/Y", time()));
        }
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $date, 'tag' => "of:$function_name", 'desc' => __("Date du jour"));
        return $result[$arg];
    }
}
