<?php
/*
 * wpof-mise-a-jour.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

add_action('wp_ajax_maj_supp_user_stagiaire', 'maj_supp_user_stagiaire');
function maj_supp_user_stagiaire()
{
    global $wpdb, $wpof, $SessionFormation, $SessionStagiaire, $suffix_session_stagiaire, $suffix_documents;
    
    $SessionFormation = get_formation_sessions();
    $reponse = array();
    
    ob_start();
    
    $erreur = 0;
    
    foreach($SessionFormation as $session)
    {
        foreach($session->inscrits as $uid)
        {
            $stagiaire = get_stagiaire_by_id($session->id, $uid);
            if ($stagiaire)
            {
                $stagiaire->update_meta("prenom");
                $stagiaire->update_meta("username");
                $stagiaire->update_meta("email");
                $stagiaire->update_meta("nom");
                echo $stagiaire->prenom." ".$stagiaire->nom." ".$stagiaire->username." ".$stagiaire->email."<br />";
            }
            else
            {
                $client = $session->get_client_by_stagiaire($uid);
                if ($client)
                {
                    if ($client->financement == "opac")
                    {
                        unset($session->inscrits[array_search($uid, $session->inscrits)]);
                    }
                    else
                    {
                        echo $session->get_id_link()." stagiaire introuvable ".$uid." pour client {$client->nom}/{$client->id} ".join(",", $client->stagiaires)."<br />";
                        $erreur++;
                    }
                }
                else
                {
                    echo $session->get_id_link()." stagiaire sans client ".$uid."<br />";
                    unset($session->inscrits[array_search($uid, $session->inscrits)]);
                }
            }
            $session->update_meta("inscrits");
        }
    }
    
    $reponse = array('log' => ob_get_clean());
    if ($erreur > 0)
    {
        echo json_encode($reponse);
        die();
    }
    
    $table = $wpdb->prefix.$suffix_documents;
    echo "<p>Table documents : visible_stagiaire → visible</p>";
    echo $wpdb->query("UPDATE $table SET meta_key = 'visible' WHERE meta_key = 'visible_stagiaire';");
    echo "</p>";
    
    $table = $wpdb->prefix.$suffix_session_stagiaire;
    $create_table = $wpdb->get_results("show create table $table;", ARRAY_A);
    $match = array();
    preg_match('/UNIQUE KEY `([^`]*)/', $create_table[0]["Create Table"], $match);
    
    echo "<p>Table stagiaires : supprimer la contrainte unique : ";
    echo $wpdb->query("ALTER TABLE $table DROP INDEX {$match[1]};");
    echo "</p>";
    
    echo "<p>renommer colonne user_id en stagiaire_id : ";
    echo $wpdb->query("ALTER TABLE $table CHANGE user_id stagiaire_id bigint(20) unsigned null;");
    echo "</p>";

    echo "<p>ajouter la nouvelle contrainte unique : ";
    echo $wpdb->query("ALTER TABLE $table ADD CONSTRAINT UNIQUE (session_id,stagiaire_id,meta_key);");
    echo "</p>";
    
    $stagiaires = get_all_stagiaires();
    
    $new_id = (integer) $wpdb->get_var("SELECT MAX(`stagiaire_id`) FROM $table;") + 1;
    foreach($stagiaires as $s)
    {
        if (isset($SessionStagiaire[$s->id]))
        {
            $session_id = $s->session_formation_id;
            $query = $wpdb->prepare("UPDATE $table SET stagiaire_id = '%d' WHERE stagiaire_id = '%d' AND session_id = '%d';", $new_id, $s->id, $session_id);
            ;
            echo "<p>$query <strong>".$wpdb->query($query)."</strong></p>";
            $session = get_session_by_id($session_id);
            $client = get_client_by_id($session_id, $s->client_id);
            $key = array_search($s->id, $session->inscrits);
            if ($key !== false)
            {
                echo "<p>Inscrits à la session $session_id</p>";
                echo "<p>".var_export($session->inscrits, true)."</p>";
                $session->inscrits[$key] = $new_id;
                echo "<p>".var_export($session->inscrits, true)."</p>";
                $session->update_meta("inscrits");
            }
            $key = array_search($s->id, $client->stagiaires);
            if ($key !== false)
            {
                echo "<p>Stagiaires du client {$client->id}</p>";
                echo "<p>".var_export($client->stagiaires)."</p>";
                $client->stagiaires[$key] = $new_id;
                echo "<p>".var_export($client->stagiaires)."</p>";
                $client->update_meta("stagiaires");
                $client->update_meta("stagiaires");
            }
                        
            $s->id = $new_id;
            $new_id++; 
        }
        $SessionStagiaire[$s->id] = $s;
        $s->update_meta("prenom");
        $s->update_meta("username");
        $s->update_meta("email");
        $s->update_meta("nom");
        echo "<p>".$s->prenom." ".$s->nom." ".$s->username." ".$s->email."</p>";
    }
    
    $reponse['log'] .= ob_get_clean();
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_maj_clean_db', 'maj_clean_db');
function maj_clean_db()
{
    global $wpdb, $wpof, $SessionFormation;
    
    $session_delete_keys = array
    (
        'entreprise_nom',
        'entreprise_adresse',
        'entreprise_cp_ville',
        'entreprise_telephone',
        'contact',
        'financement',
        'financement_complement',
        'nature_formation',
        'autres_frais',
        'tarif_total_autres_chiffre',
        'tarif_total_autres_lettre',
        'commanditaire_nom',
        'commanditaire_contact',
        'commanditaire_contact_email',
        'commanditaire_num_of',
        'st_nb_stagiaire',
        'tarif_base_total',
        'numero',
    );
    ob_start();
    
    $query = "DELETE FROM {$wpdb->prefix}postmeta WHERE meta_key IN (".join(",", $session_delete_keys).");";
    echo "<p>$query → ".$wpdb->query($query)."</p>";
    
    $stagiaire_delete_keys = array
    (
        'nb_jour',
        'tarif_jour',
        'tarif_total_chiffre',
        'tarif_total_lettre',
        'date',
        'entreprise',
        'entreprise_adresse',
        'entreprise_cp_ville',
        'entreprise_fonction',
        'entreprise_responsable',
        'entreprise_telephone',
        'exe_comptable',
        'financement',
        'financement_complement',
        'has_employeur',
        'nature_formation',
        'adresse',
        'cp_ville',
        'date_inscription',
        'statut_complement',
        'telephone',
        'tarif_base_total',
        'etat-session',
        'tarif_heure',
    );
    
    $query = "DELETE FROM {$wpdb->prefix}wpof_session_stagiaire WHERE meta_key IN (".join(",", $stagiaire_delete_keys).");";
    echo "<p>$query → ".$wpdb->query($query)."</p>";
    
    $stagiaires = get_all_stagiaires();
    
    echo "<p class='alerte'>Nb de stagiaires ".count($stagiaires)."</p>";
    
    foreach($stagiaires as $stag)
    {
        if (isset($stag->emarge) && $stag->emarge == 0)
            $stag->update_meta("confirme", 0);
        else
            $stag->update_meta("confirme", 1);
    }
    
    $query = "DELETE FROM {$wpdb->prefix}wpof_session_stagiaire WHERE meta_key = 'emarge';";
    echo "<p>$query → ".$wpdb->query($query)."</p>";
    
    $plage = array
    (
        'date_debut' => '01/01/'.$wpof->annee1,
        'date_fin' => date("d/m/Y", time() + (365 * 24 * 60 * 60)),
    );
    
    if (isset($_POST['plage']))
        $plage = $_POST['plage'];
    
    select_session_by_plage($plage);
    
    foreach($SessionFormation as $session)
    {
        foreach($session->clients as $cid)
        {
            $client = new Client($session->id, $cid);
            $cp_ville = explode(" ", $client->cp_ville, 2);
            if (is_numeric($cp_ville[0]))
            {
                $client->update_meta("code_postal", $cp_ville[0]);
                $client->update_meta("ville", isset($cp_ville[1]) ? $cp_ville[1] : "");
                echo "<p>".$client->get_nom()." {$client->cp_ville} → CP {$client->code_postal} / Ville {$client->ville}</p>";
            }
            else
            {
                $client->update_meta("code_postal", "");
                $client->update_meta("ville", $client->cp_ville);
                echo "<p>".$client->get_nom()." [{$client->cp_ville}] → <strong>CP non numérique {$cp_ville[0]}</strong></p>";
            }
        }
    }
    
    $query = "DELETE FROM {$wpdb->prefix}wpof_client WHERE meta_key = 'cp_ville';";
    echo "<p>$query → ".$wpdb->query($query)."</p>";
    
    $reponse = array('log' => ob_get_clean());
    echo json_encode($reponse);
    
    die();
}

add_action('wp_ajax_maj_db_client', 'maj_db_client');
function maj_db_client()
{
    ob_start();
    global $SessionFormation;
    
    global $wpdb;
    $table_client = $wpdb->prefix."wpof_client";
    
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    
    $charset_collate = $wpdb->get_charset_collate();
    $wpof_sql = "CREATE TABLE IF NOT EXISTS $table_client
    (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        session_id bigint(20) unsigned DEFAULT NULL,
        client_id bigint(20) unsigned DEFAULT NULL,
        meta_key varchar(128) DEFAULT NULL,
        meta_value longtext DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE (session_id,client_id,meta_key)
    ) $charset_collate;";

    $res = dbDelta($wpof_sql);
    echo "$wpof_sql\n".var_dump($res)."\n";
    
    $SessionFormation = get_formation_sessions();
    $clients = array();
    
    foreach($SessionFormation as $session)
    {
        $session->init_stagiaires();
        
        echo "<p><span class='succes'>[{$session->id}] {$session->titre_session} ({$session->type_texte})</span></p>";
        
        switch ($session->type_index)
        {
            case "inter":
                foreach ($session->stagiaires as $stagiaire)
                {
                    $client = new Client($session->id);
                    $client->update_meta("session_formation_id", $session->id);
                    $client->update_meta("stagiaires", array($stagiaire->user_id));
                    $client->update_meta("adresse", $stagiaire->adresse);
                    $client->update_meta("cp_ville", $stagiaire->cp_ville);
                    $client->update_meta("telephone", $stagiaire->telephone);
                    $client->update_meta("tarif_total_chiffre", $stagiaire->tarif_total_chiffre);
                    $client->update_meta("exe_comptable", $stagiaire->exe_comptable);
                    $client->update_meta("nature_formation", $stagiaire->nature_formation);
                    $client->update_meta("numero_contrat", $session->numero);
                    
                    if (!$stagiaire->has_employeur || $stagiaire->financement == "part" || $stagiaire->statut_stagiaire == "particulier")
                    {
                        $client->update_meta("nom", $stagiaire->get_displayname());
                        $client->update_meta("financement", "part");
                    }
                    else
                    {
                        $client->update_meta("nom", $stagiaire->entreprise);
                        $client->update_meta("financement", $stagiaire->financement);
                        $client->update_meta("financement_complement", $stagiaire->financement_complement);
                    }
                    $session->clients[] = $client->id;
                    
                    echo "<p>".var_export($client, true)."</p>";
                }
                $session->clients = array_unique($session->clients);
                $session->update_meta("clients", $session->clients);
                break;
            case "intra":
                $client = new Client($session->id);
                $client->update_meta("session_formation_id", $session->id);
                $client->update_meta("nom", $session->entreprise_nom);
                $client->update_meta("adresse", $session->entreprise_adresse);
                $client->update_meta("cp_ville", $session->entreprise_cp_ville);
                $client->update_meta("telephone", $session->entreprise_telephone);
                $client->update_meta("contact_id", $session->entreprise_contact_id);
                $client->update_meta("nature_formation", $session->nature_formation);
                $client->update_meta("financement", $session->financement);
                $client->update_meta("financement_complement", $session->financement_complement);
                $client->update_meta("tarif_total_chiffre", $session->tarif_total_chiffre);
                $client->update_meta("tarif_total_autres_chiffre", $session->tarif_total_autres_chiffre);
                $client->update_meta("autres_frais", $session->autres_frais);
                $client->update_meta("exe_comptable", $session->exe_comptable);
                $client->update_meta("numero_contrat", $session->numero);
                
                foreach ($session->stagiaires as $stagiaire)
                    $client->stagiaires[] = $stagiaire->user_id;
                $client->update_meta("stagiaires", $client->stagiaires);
                
                $session->update_meta("clients", array($client->id));
                echo "<p>".var_export($client, true)."</p>";
                break;
            case "sous_traitance":
                $client = new Client($session->id);
                $client->update_meta("session_formation_id", $session->id);
                $client->update_meta("nom", $session->commanditaire_nom);
                $client->update_meta("contact", $session->commanditaire_contact);
                $client->update_meta("contact_email", $session->commanditaire_contact_email);
                $client->update_meta("num_of", $session->commanditaire_num_of);
                $client->update_meta("financement", "opac");
                $client->update_meta("tarif_total_chiffre", $session->tarif_total_chiffre);
                $client->update_meta("tarif_total_autres_chiffre", $session->tarif_total_autres_chiffre);
                $client->update_meta("autres_frais", $session->autres_frais);
                $client->update_meta("exe_comptable", $session->exe_comptable);
                $client->update_meta("nb_stagiaires", $session->st_nb_stagiaire);
                $session->update_meta("clients", array($client->id));
                $client->update_meta("numero_contrat", $session->numero);
                echo "<p>".var_export($client, true)."</p>";
                break;
            default:
                echo "<span class='erreur'>Session mal saisie ! [".$session->id."]</span>";
                break;
        }
    }
    $reponse = array('log' => ob_get_clean());
    echo json_encode($reponse);
    
    die();
}

/*
 * sessions meta_key
 */
$session_tiret_meta_key = array
(
    'pre-requis',
    'horaire-am-debut',
    'horaire-am-fin',
    'horaire-pm-debut',
    'horaire-pm-fin',
    'doc-info',
    'nature-formation',
    'tarif-jour',
    'nb-jour',
    'nb-heure',
    'tarif-total-chiffre',
    'tarif-total-lettre',
    'autres-frais',
    'tarif-total-autres-chiffre',
    'tarif-total-autres-lettre',
    'session-unique-titre',
);

/*
 * session_stagiaire meta_key
 */
$session_stagiaire_tiret_meta_key = array
(
    'etat-session',
);

/*
 * options
 */
$wpof_option_tiret_meta_key = array
(
    'wpof_eval-form',
    'wpof_hidden-menus',
    'wpof_monnaie-symbole',
    'wpof_nature-formation',
    'wpof_options-list',
    'wpof_pdf-css',
    'wpof_pdf-footer',
    'wpof_pdf-hauteur-footer',
    'wpof_pdf-hauteur-header',
    'wpof_pdf-header',
    'wpof_pdf-marge-bas',
    'wpof_pdf-marge-droite',
    'wpof_pdf-marge-gauche',
    'wpof_pdf-marge-haut',
    'wpof_respform-admin',
    'wpof_respform-fonction',
    'wpof_respform-id',
    'wpof_session-login-form',
    'wpof_statut-stagiaire',
    'wpof_tarif-inter',
);

add_action('wp_ajax_maj_db_documents', 'maj_db_documents');
function maj_db_documents()
{
    global $wpof;
    global $wpdb;
    
    ob_start();
    $table = $wpdb->prefix."wpof_documents";
    
    echo "<p>supprimer la contrainte unique : ";
    echo $wpdb->query("ALTER TABLE $table DROP INDEX session_id;");
    echo "</p>";
    
    echo "<p>renommer colonne user_id en contexte_id : ";
    echo $wpdb->query("ALTER TABLE $table CHANGE user_id contexte_id bigint(20) unsigned null;");
    echo "</p>";
    
    echo "<p>ajouter la colonne contexte après la colonne session_id : ";
    echo $wpdb->query("ALTER TABLE $table ADD COLUMN contexte smallint unsigned AFTER session_id;");
    echo "</p>";
    
    echo "<p>ajouter la nouvelle contrainte unique : ";
    echo $wpdb->query("ALTER TABLE $table ADD CONSTRAINT UNIQUE (session_id, contexte, contexte_id, document, meta_key);");
    echo "</p>";
    
    $all_docs = $wpdb->get_results("SELECT * FROM $table;");
    
    foreach($all_docs as $row)
    {
        echo "<p><strong><a href='".get_permalink($row->session_id)."'>Session ID ".$row->session_id."</a></strong> ".get_post_meta($row->session_id, "first_date", true)."</p>";
        $row->document = str_replace('-', '_', $row->document);
        if ($row->document == "undefined")
        {
            $row->document = "scan";
            $row->contexte_id = $row->session_id;
        }
        elseif (isset($wpof->documents->term[$row->document]))
        {
            $row->contexte = $wpof->documents->term[$row->document]->contexte;
            if ($row->contexte & $wpof->doc_context->session)
                $row->contexte_id = $row->session_id;
            
            if ($row->contexte & $wpof->doc_context->client)
            {
                $clients = get_post_meta($row->session_id, "clients", true);
                if (!is_array($clients))
                {
                    echo "<span style='color: red;'>Pas de clients !</span>";
                    $clients = array();
                }
                elseif ($row->contexte_id == 0 && get_post_meta($row->session_id, "type_formation", true) == "intra")
                    $row->contexte_id = $clients[0];

                foreach($clients as $cid)
                {
                    $stagiaires = unserialize(get_client_meta($cid, "stagiaires"));
                    if (!empty($stagiaires))
                    {
                        if (in_array($row->contexte_id, $stagiaires))
                        {
                            $row->contexte_id = $cid;
                        }
                    }
                    else
                    {
                        echo "<p>Client ".get_client_meta($cid, "nom")." ($cid / Session {$row->session_id}) n'a pas de stagiaires.</p>";
                    }
                }
            }
            $class = ($row->contexte_id == 0) ? 'class="erreur"' : '';
            $query = $wpdb->prepare("UPDATE $table SET contexte = '%d', contexte_id = '%d', document = '%s' WHERE id = '%d'", $row->contexte, $row->contexte_id, $row->document, $row->id);
            //echo "<p $class>$query pour session {$row->session_id} ".get_post_meta($row->session_id, "type_formation", true)."</p>";
            echo "<p>$query : ".$wpdb->query($query)."</p>";
        }
        else
        {
            $query = $wpdb->prepare("DELETE FROM $table WHERE id = '%d'", $row->id);
            //echo "<p class='alerte'>{$row->document} : $query</p>";
            echo "<p class='alerte'>{$row->document} : $query : ".$wpdb->query($query)."</p>";
        }
    }
    
    $reponse = array('log' => ob_get_clean());
    echo json_encode($reponse);
    
    
    die();
}


add_action('wp_ajax_maj_db_tirets', 'maj_db_tirets');
function maj_db_tirets()
{
    global $wpof;
    global $wpdb;
    global $session_tiret_meta_key;
    global $wpof_option_tiret_meta_key;
    
    ob_start();
    
    $table_session_stagiaire = $wpdb->prefix."wpof_session_stagiaire";
    $table_postmeta = $wpdb->prefix."postmeta";
    $table_options = $wpdb->prefix."options";
    
    foreach($session_tiret_meta_key as $k)
    {
        $query = $wpdb->prepare
        (
            "UPDATE ".$table_postmeta." SET meta_key = '%s' WHERE meta_key = '%s';",
            str_replace("-", "_", $k),
            $k
        );
        echo "<p>[$k] $query : ";
        echo $wpdb->query($query);
        echo "</p>";
        
        // suppression des doublons (on garde celui qui a l'ID le plus élevé, donc, le plus récent)
        $query = "DELETE t1 FROM $table_postmeta AS t1, $table_postmeta AS t2 WHERE t1.meta_id < t2.meta_id AND t1.meta_key = t2.meta_key AND t1.post_id = t2.post_id;";
        echo "<p>Doublons : ".$wpdb->query($query)."</p>";
    }
    
    foreach($wpof_option_tiret_meta_key as $k)
    {
        $query = $wpdb->prepare
        (
            "UPDATE ".$table_options." SET option_name = '%s' WHERE option_name = '%s';",
            str_replace("-", "_", $k),
            $k
        );
        echo "<p>[<strong>$k</strong>] $query";
        echo $wpdb->query($query);
        echo "</p>";
        
        // suppression des doublons (on garde celui qui a l'ID le plus élevé, donc, le plus récent)
        $query = "DELETE t1 FROM $table_options AS t1, $table_options AS t2 WHERE t1.option_id < t2.option_id AND t1.option_name = t2.option_name";
        echo "<p>Doublons : ".$wpdb->query($query)."</p>";
    }
    
    // statut -> statut_stagiaire
    $query = "UPDATE ".$table_session_stagiaire." SET meta_key = 'statut_stagiaire' WHERE meta_key = 'statut';";
    echo "statut → statut_stagiaire : ".$wpdb->query($query);
    
    $reponse = array('log' => ob_get_clean());
    echo json_encode($reponse);
    
    die();
}

/*
 * Mises à jour entre versions
 */

if (WPOF_VERSION > 0.2)
{
    // ajout de deux tables dans la base de données
    // {$prefix}wpof_session_stagiaire
    // {$prefix}wpof_documents
    
    global $wpdb;
    $table_session_stagiaire = $wpdb->prefix."wpof_session_stagiaire";
    $table_documents = $wpdb->prefix."wpof_documents";
    $table_quiz = $wpdb->prefix."wpof_quiz";
    $table_creneaux = $wpdb->prefix."wpof_creneaux";
    
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    
    $charset_collate = $wpdb->get_charset_collate();
    $wpof_sql = "CREATE TABLE IF NOT EXISTS $table_creneaux
    (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        session_id bigint(20) unsigned NOT NULL,
        activite_id bigint(20) DEFAULT NULL,
        module_id bigint(20) DEFAULT NULL,
        date_debut datetime,
        date_fin datetime,
        type varchar(20) DEFAULT NULL,
        lieu_id bigint(20) DEFAULT NULL,
        salle_id bigint(20) DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE (date_debut, session_id, lieu_id, salle_id)
    ) $charset_collate;";
    
    $res = dbDelta($wpof_sql);
    
    $wpof_sql = "CREATE TABLE IF NOT EXISTS $table_documents
    (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        session_id bigint(20) unsigned DEFAULT NULL,
        contexte smallint unsigned DEFAULT NULL,
        contexte_id bigint(20) unsigned DEFAULT NULL,
        document varchar(64) DEFAULT NULL,
        meta_key varchar(255) DEFAULT NULL,
        meta_value longtext DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE (session_id,contexte,contexte_id,document,meta_key)
    ) $charset_collate;";
    
    $res = dbDelta($wpof_sql);
    
    /*
    $existing_columns = $wpdb->get_col("DESC {$table_documents}", 0);
    if (!in_array("scan", $existing_columns))
    {
        $wpdb->query("ALTER TABLE $table_documents ADD COLUMN scan tinyint DEFAULT 0 AFTER document;");
    }
    */
    
    //debug_info($res, "SQL1");
    
    // Note : je ne sais pas pourquoi, l'envoi des deux requêtes de création de table en une fois à dbDelta ne fonctionne pas, seule la seconde est créée...
    $wpof_sql = "CREATE TABLE IF NOT EXISTS $table_session_stagiaire
    (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        session_id bigint(20) unsigned DEFAULT NULL,
        user_id bigint(20) unsigned DEFAULT NULL,
        meta_key varchar(255) DEFAULT NULL,
        meta_value longtext DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE (session_id,user_id,meta_key)
    ) $charset_collate;";

    $res = dbDelta($wpof_sql);
    //debug_info($res, "SQL2");
    
    $wpof_sql = "CREATE TABLE IF NOT EXISTS $table_quiz
    (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        quiz_id bigint(20) unsigned DEFAULT NULL,
        parent_id bigint(20) unsigned DEFAULT NULL,
        subject varchar(64) DEFAULT NULL,
        title varchar(255) DEFAULT NULL,
        meta_key smallint unsigned DEFAULT NULL,
        meta_value longtext DEFAULT NULL,
        PRIMARY KEY (id),
        UNIQUE (quiz_id,parent_id,subject,occurrence,meta_key)
    ) $charset_collate;";

    $res = dbDelta($wpof_sql);
}
