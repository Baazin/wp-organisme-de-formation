REPLACE INTO {table_prefix}options (option_name, option_value) VALUES
('wpof_monnaie', 'euro'),
('wpof_monnaie_symbole', '€'),
('wpof_exotva', '1'),
('wpof_tarif_inter', '55'),
('wpof_respform_admin', ''),
('wpof_hidden_menus', 'a:3:{i:2;s:1:\"1\";i:5;s:1:\"1\";i:25;s:1:\"1\";}'),
('wpof_eval_form', 'h | Organisation générale de la formation
r | Qualité de l’information en amont
r | Durée
r | Rythme de la formation
r | Horaires
r | Composition du groupe
ta | Commentaires :

h | Contenu de la formation
r | Réalisation du programme pédagogique prévu
r | Réponse à vos attentes
r | Équilibre entre la théorie et la pratique
r | Utilité de cette formation pour votre activité
r | Supports pédagogiques
ta | Commentaires :

h | Animation de la formation
r | Qualité des échanges entre participants
r | Démarche pédagogique et qualité de l’animation
r | Clarté des explications
ta | Commentaires :

h | Organisation matérielle de la formation
r | La qualité de l’accueil
r | Les locaux (espace, accessibilité)
r | Le matériel et les équipements utilisés
ta | Commentaires :

h | Appréciation générale
r | Êtes-vous satisfait de la formation :
t | Ce qui vous a le plus intéressé :
t | Ce qui vous a le moins intéressé :
ta | Que retenez-vous de cette formation ?
ta | Quelle(s) autre(s) formation(s) aimeriez-vous suivre ?'),
('wpof_pdf_marge_haut', '10'),
('wpof_pdf_marge_bas', '10'),
('wpof_pdf_marge_gauche', '10'),
('wpof_pdf_marge_droite', '10'),
('wpof_pdf_hauteur_header', '15'),
('wpof_pdf_hauteur_footer', '15'),
('wpof_pdf_css', '#header, #footer
{
    position: fixed;
    left: 0;
    right: 0;
    color: #888;
    font-size: 0.9em;
}
#header
{
    top: 0;
}
#footer
{
    border-top: 0.1pt solid #aaa;
}
#footer p,
.page-number
{
    font-size: 0.7em;
    line-height: 0.9em;
    text-align: center;
}
body
{
    font-family: sans-serif;
    text-align: justify;
    font-size: 8pt;
}
div#attestation,
div#accord-comm
{
    font-size: 10pt;
}

div.reglement-interieur p
{
    margin: 2px 0;
}

.page-number:before
{
     content: \"Page \" counter(page);
}
h1
{
    background-color: cmyk(0, 0.15, 1, 0);
    text-align: center;
    page-break-before: always;
    break-before: always;
}
h1:nth-of-type(1)
{
    page-break-before: avoid;
    break-before: avoid;
}
h2, h3, h4, h5
{
    page-break-after: avoid;
    break-after: avoid;
}
h2:nth-of-type(1),
h3:nth-of-type(1),
h4:nth-of-type(1),
h5:nth-of-type(1)
{
    margin-top: 0px;
}
div.footnote
{
    position: absolute;
    bottom: 0;
}

table.signature,
table.cartouche { width: 100%; }

table.signature { table-layout: fixed; }

tr.signature { height: 50mm; }

.cartouche td { border: 0.1mm solid #CCC; text-align: center; }

img.pv-secu
{
    height: 245mm;
    width: 173mm;
    margin: auto;
}
img.photo-formateur
{
    width: 35mm;
    height: auto;
    margin: 0 0 5mm 5mm;
    float: right;
}

/* Proposition de formation */
.proposition p,
.proposition ul,
.proposition ol,
{
    margin: 0 0 2mm 0;
    font-size: 7pt;
    font-weight: normal;
}
.proposition td
{
    vertical-align: top;
    border-bottom: 0.1mm solid #CCC;
    font-size: 7pt;
    padding: 1mm 0 3mm 4mm;
}
.proposition tr > td:nth-of-type(1)
{
    width: 30mm;
    padding-right: 4mm;
    padding-left: 0;
    font-weight: bold;
}
.proposition td p:first-child,
.proposition td ol:first-child,
.proposition td ul:first-child
{
    margin-top: 0mm;
}
.proposition td p:last-child,
.proposition td ul:last-child,
.proposition td ol:last-child
{
    margin-bottom: 0mm;
}
table.proposition
{
    margin-bottom: 6mm;
}

table.eval { width: 100%; }
td.td-titre { font-size: 12pt; font-weight: bold; }
td.td-value { width: 10mm; border: 0.1mm solid #AAA; color: #AAA; text-align: center; }
td.td-plusmoins { text-align: center; font-weight: bold; }
td.td-reponse-t { border-bottom: 0.1mm solid #CCC; }
td.td-reponse-ta { height: 15mm; border-bottom: 0.1mm solid #CCC; }

/* feuille d\'émargement */
.emargement h2
{
    background-color: cmyk(0, 0.15, 1, 0);
    text-align: center;
}
table.tableau-dates,
table.tableau-stagiaires
{
    width: 100%;
    border-collapse: collapse;
}
table.tableau-dates td,
table.tableau-dates th,
table.tableau-stagiaires td,
table.tableau-stagiaires th
{
    height: 13mm;
    border: 0.2mm solid #AAA;
    text-align: center;
}
table.tableau-dates th.date,
table.tableau-stagiaires th.stagiaire
{
    width: 50%;
}
table.tableau-dates th.plage,
table.tableau-stagiaires th.plage
{
    width: 20%;
}
td.stagiaire-absent
{
    background-color: #AAA;
}

div.saut-de-page { page-break-after: always; }'),
('wpof_options_list', ''),
('wpof_terms_exo_tva', 'Cette prestation de formation est exonérée de TVA conformément à l\'article 261 du CGI'),
('wpof_terms_no_session', 'Pas de session programmée pour l\'instant'),
('wpof_terms_tarif_inter', 'Tarif inter-entreprises par stagiaire'),
('wpof_terms_tarif_groupe', '<a href=\'/contact/\'>Contactez-nous</a> pour un tarif de groupe'),
('wpof_terms_connexion_session', 'Vous devez vous connecter au site pour vous inscrire à cette session ou créer un compte si vous ne l\'avez pas encore fait.'),
('wpof_terms_msg_debut_pre_inscription', 'Merci de remplir ce formulaire. Il s\'agit de la première étape. Nous prendrons ensuite contact avec vous (e-mail ou téléphone) pour affiner votre inscription et établir la convention de formation.'),
('wpof_terms_msg_validation_pre_inscription', 'Vous pouvez valider votre demande de pré-inscription même si vous n\'avez pas rempli tout le formulaire. Vous pourrez le compléter par la suite.'),
('wpof_terms_question_employeur', 'Vous vous inscrivez dans le cadre de votre activité professionnelle'),
('wpof_terms_autres_frais', 'frais de déplacement, hébergement et restauration'),
('wpof_doc_last_modif', 'Dernière modification'),
('wpof_doc_alerte_creation', 'Attention ! Document déjà signé par votre responsable !'),
('wpof_doc_pas_de_docs', 'Pas de document pour l\'instant. vous serez averti lorsqu\'ils seront prêts.'),
('wpof_doc_diffuser', 'Rendez ce document disponible au stagiaire'),
('wpof_doc_supprimer', 'Supprimer le document (il faudra tout recommencer)'),
('wpof_doc_emarge_recu_ri', 'J\'approuve le règlement intérieur'),
('wpof_formateur_gest', ''),
('wpof_formateur_marque', '1'),
('wpof_doc_scan', 'Déposez un scan correspondant à ce document'),
('wpof_url_bpf', 'bpf'),
('wpof_title_bpf', 'Bilan pédagogique et financier'),
('wpof_url_pilote', 'pilote'),
('wpof_title_pilote', 'Pilote des sessions'),
('wpof_champs_additionnels', 'a:2:{s:14:\"numero_contrat\";s:1:\"1\";s:16:\"formateur_marque\";s:1:\"1\";}'),
('wpof_special_page', 'single'),
('wpof_url_aide', 'aide'),
('wpof_title_aide', 'Aide en ligne')
;