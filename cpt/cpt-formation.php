<?php
/*
 * wpof-admin.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-quiz.php");
require_once(wpof_path . "/class/class-aide.php");

/**
 * Add post type formation
 */
function register_cpt_formation() {

	/**
	 * Post Type: Formations.
	 */

	$labels = array(
		"name" => __( "Formations", "generic" ),
		"singular_name" => __( "Formation", "generic" ),
		"all_items" => __( "Toutes les formations", "generic" ),
		"add_new" => __( "Ajouter une nouvelle", "generic" ),
		"add_new_item" => __("Ajouter une nouvelle formation"),
		"view_item" => __("Voir la formation"),
		"edit_item" => __("Modifier la formation"),
		"update_item" => __("Mettre à jour la formation"),
	);

	$args = array(
		"label" => __( "Formations", "generic" ),
		"labels" => $labels,
		"description" => "Fiche-programme de formation",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => 'formations',
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "formation", "with_front" => true ),
		"query_var" => true,
		"menu_position" => 4,
		"menu_icon" => "dashicons-welcome-learn-more",
		"supports" => array( "title", "thumbnail" ),
		"taxonomies" => array( "category", "post_tag" ),
	);

	register_post_type( "formation", $args );
}

add_action( 'init', 'register_cpt_formation' );


// add meta box
add_action('add_meta_boxes','initialisation_formation_metaboxes');
function initialisation_formation_metaboxes()
{
    global $post;
    $formation = get_formation_by_id($post->ID);
    $user_id = get_current_user_id();
    $role = wpof_get_role($user_id);
    
    if ($role != "um_formateur-trice" || (!empty($formation->formateur) && in_array($user_id, $formation->formateur)) || empty($formation->formateur))
    {
        add_meta_box('formation-formateur', __("Formateur.trice"), 'formation_formateur_meta_box', 'formation', 'side', 'high');
        add_meta_box('formation-divers', __("Divers"), 'formation_divers_meta_box', 'formation', 'side', 'high');
        add_meta_box('formation-data', __('Caractéristiques de la formation'), 'formation_data_meta_box', 'formation', 'normal', 'high');
        add_meta_box('formation-quiz', __('Évaluation des compétences'), 'formation_quiz_meta_box', 'formation', 'normal', 'high');
    }
    else
    {
        remove_meta_box('submitdiv', 'formation', 'side');
        remove_meta_box('categorydiv', 'formation', 'side');
        remove_meta_box('tagsdiv-post_tag', 'formation', 'side');
        remove_meta_box('postimagediv', 'formation', 'side');
        add_meta_box('interdit', __("Droit d'accès"), 'formation_interdit_meta_box', 'formation', 'normal', 'high');
    }
}

function formation_interdit_meta_box($post)
{
    $formation = get_formation_by_id($post->ID);
    ?>
    <p class="erreur"><?php _e("Vous n'avez pas les droits nécessaires pour modifier cette formation"); ?></p>
    <p><?php _e("Veuillez contacter l'équipe pédagogique de cette fiche :"); ?></p>
    <?php
    the_liste_formateur(array('only' => $formation->formateur));
}
function formation_formateur_meta_box($post)
{
    $formateurs = get_post_meta($post->ID, "formateur", true);
    if (empty($formateurs))
        $formateurs = array(get_current_user_id());
    echo select_user(array('role__in' => array('um_formateur-trice', 'um_responsable')), 'formateur[]', $formateurs, null, true).get_icone_aide("formation_formateur");
}

function formation_divers_meta_box($post)
{
    global $wpof;
        $data = get_post_meta($post->ID, "duree", true);
        echo '<label for="duree"><h3>'.__('Durée').'</h3></label>'.get_icone_aide("formation_duree");
        echo '<p>'.__('Durée prévue de la formation en heures').'</p>';
        echo '<input style="width: 100%;" id="duree" name="duree" step="0.25" type="number" value="'.$data.'" />';
        
        if (champ_additionnel('duree_jour'))
        {
            $data = get_post_meta($post->ID, "nb_jour", true);
            echo '<p>'.__("Indiquez éventuellement une durée en jours ou demies-journées (précisez l'unité)").'</p>';
            echo '<label for="nb_jour">'.__('Nombre de jours').'</label>'.get_icone_aide("sessionformation_nb_jour");
            echo '<input style="width: 100%;" id="nb_jour" name="nb_jour" type="text" value="'.$data.'" />';
        }
        
        $data = get_post_meta($post->ID, "tarif", true);
        echo '<label for="tarif"><h3>Tarif inter</h3></label>'.get_icone_aide("formation_tarif");
        echo '<p>'.__('Tarif par défaut pour une session inter (par heure et par stagiaire).');
        $tarif_base = $wpof->tarif_inter;
        if ($tarif_base > 0)
            printf(__('<br />Laissez vide pour appliquer le tarif de base de %d %s.'), $tarif_base, $wpof->monnaie_symbole);
        echo '<input style="width: 100%;" id="tarif" name="tarif" step="0.001" type="number" value="'.$data.'" />';
}

function formation_data_meta_box($post)
{
    global $tinymce_wpof_settings;
    global $wpof;
    
    $formation = get_formation_by_id($post->ID);
    
    echo "<div><strong>Accès</strong> ".get_icone_aide("formation_acces_public");
    $data = get_post_meta($post->ID, "acces_public", true);
    echo "<input type='checkbox' name='acces_public' id='acces_public' value='1' ".checked(1, $data, false)." />";
    echo "<label class='inline' for='acces_public'>".__("Cette fiche formation est publique")."</label></div>";
    
    echo "<div><strong>Spécialité</strong> ".get_icone_aide("sessionformation_specialite");
    $data = get_post_meta($post->ID, "specialite", true);
    echo $wpof->specialite->get_select_list($data, "", __("Choisissez une spécialité, la plus précise possible"));
    echo "</div>";
    
    foreach(array('proposition' => __("Proposition de formation"), 'convention' => __("Convention de formation")) as $groupe => $titre)
    {
        echo "<hr /><h3>$titre</h3>";
        foreach($wpof->desc_formation->get_group($groupe)->term as $k => $t)
        {
            echo "<h4>{$t->text} ".get_icone_aide("sessionformation_".$k)."</h4>";
            switch ($wpof->{"formation_".$k."_mode"})
            {
                case 'force':
                    ?>
                    <p><em><?php _e("Ce paramètre est fixé par votre responsable de formation et ne peut être modifié."); ?></em></p>
                    <p><?php echo $wpof->{"formation_".$k."_text"}; ?></p>
                    <?php
                    break;
                case 'propose':
                    if ($formation->$k == "")
                        $formation->$k = "<p><span class='brouillon'>Texte d'exemple, adaptez-le !</span></p>".$wpof->{"formation_".$k."_text"};
                default:
                    wp_editor($formation->$k, $k, array_merge($tinymce_wpof_settings, array('textarea_rows' => 8)));
                    break;
            }
        }
    }
}        

function formation_quiz_meta_box($post)
{
    global $tinymce_wpof_settings;
    
    _e("<p>L'évaluation des compétences doit avoir lieu avant la session pour les pré-requis <strong>et</strong> les objectifs. Elle doit permettre de :</p>
    <ul class='classic'>
<li>valider les pré-requis</li>
<li>proposer éventuellement un allégement si le stagiaire maîtrise déjà certaines compétences</li>
</ul>

<p>L'évaluation des compétences doit <strong>également</strong> avoir lieu après la formation pour vérifier que le stagiaire a bien acquis des compétences à la suite de la session.</p>

</ul>
<p>Attention à la rédaction du quiz : tout paragraphe devient une question. Vous pouvez intercaler des titres (format Titre), insérer des images et des vidéos dans les paragraphes avec le texte de la question. Ne laissez pas de paragraphe vide ou avec une image seule !</p>

<p>Les questions portent sur des compétences à avoir ou acquérir. Par exemple « créer des dossiers et naviguer dans votre ordinateur ». Chaque compétence est évaluée de 1 à 5.</p>");

    echo "<h3>".__("Évaluation des pré-requis")."</h3>";
    if ($quizpr_id = get_post_meta($post->ID, "quizpr_id", true))
    {
        $quizpr = new Quiz();
        $quizpr->set_identite("prerequis", $post->ID);
        $quizpr->init_questions($quizpr_id);
        $data = $quizpr->get_html_questions();
    }
    else
        $data = get_post_meta($post->ID, "quizpr", true);
    wp_editor($data, "quizpr", array_merge($tinymce_wpof_settings, array('textarea_rows' => 10)));
    
    echo "<h3>".__("Évaluation des objectifs")."</h3>";
    if ($quizobj_id = get_post_meta($post->ID, "quizobj_id", true))
    {
        $quizobj = new Quiz();
        $quizobj->set_identite("objectifs", $post->ID);
        $quizobj->init_questions($quizobj_id);
        $data = $quizobj->get_html_questions();
    }
    else
        $data = get_post_meta($post->ID, "quizobj", true);
    wp_editor($data, "quizobj", array_merge($tinymce_wpof_settings, array('textarea_rows' => 15)));
}

// save meta box with update
add_action('save_post','save_formation_metaboxes');
function save_formation_metaboxes($post_ID)
{
    global $wpof;
    if (get_post_type($post_ID) != "formation") return;
    
    $champs = array_merge
    (
        array_keys($wpof->desc_formation->term),
        array
        (
            'formateur',
            'specialite',
            'duree',
            'nb_jour',
            'tarif',
            'acces_public',
        )
    );
    foreach($champs as $c)
    {
        if(isset($_POST[$c]))
            update_post_meta($post_ID, $c, $_POST[$c]);
    }
    
    if (!isset($_POST['acces_public']))
        update_post_meta($post_ID, 'acces_public', 0);
    
    if (isset($_POST['quizpr']) && $_POST['quizpr'] != "")
    {
        $quiz = new Quiz();
        $quiz->set_identite("prerequis", $post_ID);
        $quiz->init_questions();
        $quiz->parse_text($_POST['quizpr']);
        update_post_meta($post_ID, "quizpr_id", $quiz->quiz_id);
    }
    if (isset($_POST['quizobj']) && $_POST['quizobj'] != "")
    {
        $quiz = new Quiz();
        $quiz->set_identite("objectifs", $post_ID);
        $quiz->init_questions();
        $quiz->parse_text($_POST['quizobj']);
        update_post_meta($post_ID, "quizobj_id", $quiz->quiz_id);
    }
}
