<?php
/*
 * wpof-admin.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

 
/**
 * Add post type session de formation
 */
function register_cpt_session_formation() {

	/**
	 * Post Type: Formations.
	 */

	$labels = array(
		"name" => __( "Sessions de formation", "generic" ),
		"singular_name" => __( "Session de formation", "generic" ),
		"all_items" => __( "Toutes les sessions", "generic" ),
		"add_new" => __( "Ajouter une nouvelle", "generic" ),
		"add_new_item" => __("Ajouter une nouvelle session"),
		"view_item" => __("Voir la session"),
		"edit_item" => __("Modifier la session"),
		"update_item" => __("Mettre à jour la session"),
	);

	$args = array(
		"label" => __( "Sessions de formation", "generic" ),
		"labels" => $labels,
		"description" => "Session de formation programmée avec dates et lieu",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
                'show_ui' => false, // Should the primary admin menu be displayed?
                'show_in_nav_menus' => false, // Should it show up in Appearance > Menus?
                'show_in_menu' => false, // This inherits from show_ui, and determines *where* it should be displayed in the admin
                'show_in_admin_bar' => false, // Should it show up in the toolbar when a user
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "session", "with_front" => true ),
		"query_var" => true,
		"menu_position" => 3.14,
		"menu_icon" => "dashicons-calendar-alt",
		"supports" => array( "title", "thumbnail" ),
		"taxonomies" => array(),
	);

	register_post_type( "session", $args );
}

add_action( 'init', 'register_cpt_session_formation' );


// add meta box
add_action('add_meta_boxes','initialisation_session_metaboxes');
function initialisation_session_metaboxes()
{
    wp_enqueue_script('jquery-ui-core');
    wp_enqueue_script( 'wpof_cpt', wpof_url."cpt/cpt.js", array('jquery') );
    wp_enqueue_media();
    
    add_meta_box('session-formation', __("Formation"), 'session_formateur_meta_box', 'session', 'normal', 'high');
    add_meta_box('session-creneaux', __("Dates et créneaux"), 'session_creneaux_meta_box', 'session', 'normal', 'high');
    add_meta_box('session-lieu', __("Lieu"), 'session_lieu_meta_box', 'session', 'normal', 'high');
    add_meta_box('session-data', __('Caractéristiques de la session'), 'session_data_meta_box', 'session', 'normal', 'high');
    add_meta_box('session-quiz', __('Évaluation des compétences'), 'session_quiz_meta_box', 'session', 'normal', 'high');
    add_meta_box('session-ressources', __('Ressources'), 'session_ressources_meta_box', 'session', 'normal', 'high');
}

function session_formateur_meta_box($post)
{
    global $wpof;
    global $SessionFormation;
    if (!isset($SessionFormation[$post->ID]))
        $SessionFormation[$post->ID] = new SessionFormation($post->ID);
    $session_formation = $SessionFormation[$post->ID];
    
    $id_formation = ($session_formation->formation_id == "") ? -1 : $session_formation->formation_id;
    // pré-affichage en fonction de l'ID de la formation sélectionnée
    // si ID == -1, alors session unique, pas une formation du catalogue
    // attention, logique inversée, on n'insère que du display none !
    $display_unique = ($id_formation > 0) ? "style='display: none;'" : "";
    $display_catalogue = ($id_formation < 0) ? "style='display: none;'" : "";
    
    // désactivation du champ titre : ce dernier ne doit pas être modifié à la main, il est généré en fonction de certains champs
    ?>
    
    <div class="flex-container <?php echo $session_formation->type_index ;?>">
    
        <div>        
        <h3><?php _e("Identifiant interne"); ?></h3>
        <input type="text" name="numero" value="<?php echo $session_formation->numero; ?>" />
        
        <?php
        echo "<h3>".__("Type de session")."</h3>";
        if ($session_formation->type_index == "")
            echo text_to_choices($wpof->type_session, 'type_formation', 'radio');
        else
            echo "<p>".$session_formation->type_texte."</p>";
        
        echo "<h3>".__("Visibilité")."</h3>";
        $data = get_post_meta($post->ID, "visibilite_session", true);
        echo text_to_choices($wpof->visibilite_session, 'visibilite_session', 'radio', $data);
        
        ?>
        </div>
        
        <div>
        <?php
        echo "<h3>".__("Formation du catalogue")."</h3>";
        echo "<div id='catalogue'>".select_post_by_type("formation", "formation", $id_formation, __("Session unique, sans lien avec le catalogue"))."</div>";
        
        echo "<div class='session-unique' id='session_unique_titre' $display_unique>";
        $data = get_post_meta($post->ID, "session_unique_titre", true);
        echo "<h3>".__("Intitulé de cette session unique")."</h3>";
        echo '<input name="session_unique_titre" type="text" size="60" value="'.$data.'" />';
            echo '<div class="non_sous_traitance">';
            echo "<h3>Spécialité</h3>";
            $data = get_post_meta($post->ID, "specialite", true);
            echo $wpof->specialite->get_select_list($data, "", __("Choisissez une spécialité, la plus précise possible"));
            echo "</div>";
        echo "</div>";
        
        echo "<h3>".__("Formateur⋅trice animant cette session")."</h3>";
        echo hidden_input("user_id", get_current_user_id());
        $selected_formateur = get_post_meta($post->ID, "formateur", true);
        if ($selected_formateur == "" || $selected_formateur == null)
            $selected_formateur = get_current_user_id();
        echo select_user(array('role__in' => array('um_formateur-trice', 'um_responsable')), 'formateur[]', $selected_formateur, null, true);
        
        ?>
        </div>
        
        <div class="non_sous_traitance">
        <h3><?php _e('Nombre de stagiaires'); ?></h3>
        <label for="stagiaires_min"><?php _e("minimum"); ?> </label>
        <input id="stagiaires_min" name="stagiaires_min" type="number" value="<?php echo $session_formation->stagiaires_min; ?>" /><br />
        
        <label for="stagiaires_max"><?php _e("maximum"); ?></label>
        <input id="stagiaires_max" name="stagiaires_max" type="number" value="<?php echo $session_formation->stagiaires_max; ?>" />
        </div>
        
        <div id="tarif_bloc" class="non_sous_traitance">
        <h3><?php _e('Tarif'); ?></h3>
        <?php 
        $tarif_base = get_post_meta($session_formation->formation_id, "tarif_inter", true);
        if ($tarif_base == "")
            $tarif_base = $wpof->tarif_inter;

        if ($tarif_base > 0 && empty($session_formation->tarif_heure))
            $session_formation->tarif_heure = $tarif_base;
        
        ?>
        <label for="id_tarif_heure"><?php _e("Horaire"); ?></label>
            <span data-input="id_tarif_heure" class="icone-bouton reset-input dashicons dashicons-dismiss"></span>
            <input id="id_tarif_heure" name="tarif_heure" type="number" min="0" step="0.01" value="<?php echo (!$session_formation->tarif_base_total) ? $session_formation->tarif_heure : ""; ?>" 
            <?php disabled($session_formation->tarif_base_total); ?> /> 
            <?php echo $wpof->monnaie_symbole ?>
        <label for="id_tarif_total"><?php _e("Total"); ?></label>
            <span data-input="id_tarif_total" class="icone-bouton reset-input dashicons dashicons-dismiss"></span>
            <input id="id_tarif_total" name="tarif_total_chiffre" type="number" min="0" step="0.01" value="<?php echo ($session_formation->tarif_base_total) ? $session_formation->tarif_total_chiffre : ""; ?>" 
            <?php disabled(!$session_formation->tarif_base_total); ?> /> 
            <?php echo $wpof->monnaie_symbole ?>
        </div>

        
        <div class="sous_traitance">
        <h3><?php _e("Organisme commanditaire"); ?></h3>
        <label for="commanditaire_nom"><?php _e("Nom de l'organisme de formation"); ?></label>
        <input type="text" name="commanditaire_nom" id="commanditaire_nom" value="<?php echo $session_formation->commanditaire_nom; ?>" />
        <label for="commanditaire_contact"><?php _e("Nom du contact"); ?></label>
        <input type="text" name="commanditaire_contact" id="commanditaire_contact" value="<?php echo $session_formation->commanditaire_contact; ?>" />
        <label for="commanditaire_contact_email"><?php _e("E-mail du contact"); ?></label>
        <input type="text" name="commanditaire_contact_email" id="commanditaire_contact_email" value="<?php echo $session_formation->commanditaire_contact_email; ?>" />
        <label for="commanditaire_num_of"><?php _e("Numéro de déclaration d'activité"); ?></label>
        <input type="text" name="commanditaire_num_of" id="commanditaire_num_of" value="<?php echo $session_formation->commanditaire_num_of; ?>" />
        </div>
        
        <div class="sous_traitance">
        <h3><?php _e("Bilan financier"); ?></h3>
        <label for="st_id_tarif_total"><?php _e("Tarif facturé à l'OF"); ?></label>
            <input id="st_id_tarif_total" name="st_tarif_total_chiffre" type="number" min="0" step="0.01" value="<?php echo ($session_formation->tarif_base_total) ? $session_formation->tarif_total_chiffre : ""; ?>" />
            <?php echo $wpof->monnaie_symbole ?>
        <label for="id_nb_heure"><?php _e("Nombre d'heures effectuées (en décimal)"); ?></label>
            <input id="id_nb_heure" name="nb_heure_decimal" type="number" min="0" step="0.01" value="<?php echo ($session_formation->nb_heure_decimal) ? $session_formation->nb_heure_decimal : ""; ?>" />
        <label for="id_nb_stagiaire"><?php _e("Nombre de stagiaires"); ?></label>
            <input id="id_nb_stagiaire" name="st_nb_stagiaire" type="number" min="1" step="1" value="<?php echo ($session_formation->st_nb_stagiaire) ? $session_formation->st_nb_stagiaire : ""; ?>" />
        </div>
        
    </div> <!-- fin div class=flex-container -->
    <?php
}

function get_lieu()
{
    $lieu_id = $_POST['lieu_id'];

    $lieu = get_post_meta($lieu_id);
    $data = array(
            "adresse" => (isset ($lieu['adresse'][0])) ? $lieu['adresse'][0] : "",
            "code_postal" => (isset ($lieu['code_postal'][0])) ? $lieu['code_postal'][0] : "",
            "ville" => (isset ($lieu['ville'][0])) ? $lieu['ville'][0] : "",
            "localisation" => (isset ($lieu['localisation'][0])) ? $lieu['localisation'][0] : "",
            );
    
    if (isset ($lieu['secu_erp'][0]) && $lieu['secu_erp'][0] != "")
    {
        $data["secu_erp"] = $lieu['secu_erp'][0];
        $data["secu_erp_name"] = get_the_title($lieu['secu_erp'][0]);
        $data["secu_erp_link"] = get_attachment_link($lieu['secu_erp'][0]);
    }
    echo json_encode($data);

    die();
}
add_action( 'wp_ajax_get_lieu', 'get_lieu' );

function get_formation_data()
{
    $formation = new Formation($_POST['formation_id']);
    
    echo json_encode($formation);
    
    die();
}
add_action( 'wp_ajax_get_formation_data', 'get_formation_data' );

function session_lieu_meta_box($post)
{
    global $SessionFormation;
    if (!isset($SessionFormation[$post->ID]))
        $SessionFormation[$post->ID] = new SessionFormation($post->ID);
    $session_formation = $SessionFormation[$post->ID];
    
    if ($session_formation->type_index != 'sous_traitance') :
    
    $lieu = $session_formation->lieu;
    if (!isset($lieu))
        $lieu = new Lieu();

    $id_lieu = $lieu->id;
    echo "<p id='lieu-existant'>".__("Lieu prédéfini")." ";
    echo select_post_by_type("lieu", "lieu", $id_lieu, __("Nouveau lieu, pas encore déterminé ou à usage unique"));
    echo "</p>";
    
    $display_unique = $disabled = $readonly = $secu_erp_name = $inactif = "";
    
    if ($id_lieu != -1 && $id_lieu != "")
    {
        $display_unique = "style='display: none;'";
        $disabled = "disabled='disabled'";
        $readonly = "readonly='readonly'";
        $inactif = "inactif";
    }

    $secu_erp_name = ($lieu->secu_erp != "" && $lieu->secu_erp > 0) ? get_the_title($lieu->secu_erp) : "";  

    ?>
    
    <div id="lieu-data">
    <p class='nouveau-lieu' id='nouveau-lieu' <?php echo $display_unique; ?>>
    <?php _e("Nom du lieu à créer") ?>
    <input name="lieu_nom" type="text" <?php echo $disabled; ?> value="<?php echo $lieu->nom; ?>" />
    </p>
    <p>
    <label for="adresse"><?php _e("Adresse"); ?> </label>
    <textarea id="adresse" name="lieu_adresse" cols="30" rows="2" <?php echo $disabled; ?>><?php echo $lieu->adresse; ?></textarea>
    </p>
    <p>
    <label for="code_postal"><?php _e("Code postal"); ?> </label>
    <input id="code_postal" name="lieu_code_postal" type="text" <?php echo $disabled; ?> value="<?php echo $lieu->code_postal; ?>" />
    </p>
    <p>
    <label for="ville"><?php _e("Ville"); ?> </label>
    <input id="ville" name="lieu_ville" type="text" <?php echo $disabled; ?> value="<?php echo $lieu->ville; ?>" />
    (<?php _e("Indiquez au moins cette information si vous n'avez pas tous les détails pour le lieu"); ?>)
    </p>
    <p>
    <label for="localisation"><?php _e("Localisation"); ?></label><br />
    <em><?php _e("Insertion de code pour embarquer une carte avec pointeur (Openstreetmap par exemple)"); ?></em><br />
    <textarea id="localisation" name="lieu_localisation" rows="5" cols="60" <?php echo $readonly; ?>><?php echo $lieu->localisation; ?></textarea>
    
    </p>
    <p>
    <label for="secu_erp"><?php _e("PV de commission de sécurité ERP"); ?></label><br />
    <input type="hidden" id="secu_erp" name="lieu_secu_erp" value="<?php echo $lieu->secu_erp; ?>" />
    <a href="#" class="clickButton <?php echo $inactif; ?> button-add-media" data-valueid="secu_erp" data-linkmedia="erp-nom-image" id="erp-add-image"><?php _e("Téléversez une image"); ?></a>
    <a id="erp-nom-image" target="_blank" href="<?php echo get_attachment_link($liue->secu_erp); ?>"><?php echo $secu_erp_name; ?></a>
    
    </p>
    
    <p id="add-nouveau-lieu" class="center" <?php echo $display_unique ?>>
    <a href="#" class="clickButton"><?php _e("Enregistrez ce nouveau lieu"); ?></a>
    </p>
    
    </div>
    <?php
    
    endif; // type_formation != sous_traitance
}

function session_creneaux_meta_box($post)
{
    global $wpof;
    global $type_creneau;
    global $SessionFormation;
    if (!isset($SessionFormation[$post->ID]))
        $SessionFormation[$post->ID] = new SessionFormation($post->ID);
    $session_formation = $SessionFormation[$post->ID];
    
    $dates = $session_formation->dates_array;
    
    if (!is_array($dates)) // TODO : vérifier l'utilité de ces deux lignes
        $dates = preg_split("/[\s]+/", $dates);
        
    // Explications type de créneaux
    if ($session_formation->type_index != 'sous_traitance')
    {
        echo "<p>".__("Types de créneaux : ");
        foreach($type_creneau as $type => $text)
            echo "<span class='faux-bouton creneau $type'>$text</span>";
        echo "</p>";
    }
    
    echo $session_formation->get_html_creneaux(true);
}

function session_data_meta_box($post)
{
    global $SessionFormation;
    if (!isset($SessionFormation[$post->ID]))
        $SessionFormation[$post->ID] = new SessionFormation($post->ID);
    $session_formation = $SessionFormation[$post->ID];
    
    if ($session_formation->type_index != 'sous_traitance') :

    global $tinymce_wpof_settings;
    
    $id_formation = ($session_formation->formation_id == "") ? -1 : $session_formation->formation_id;
    // pré-affichage en fonction de l'ID de la formation sélectionnée
    // si ID == -1, alors session unique, pas une formation du catalogue
    // attention, logique inversée, on n'insère que du display none !
    $display_unique = ($id_formation > 0) ? "style='display: none;'" : "";
    $display_catalogue = ($id_formation < 0) ? "style='display: none;'" : "";
    
    echo "<div class='infobox'>";
    echo "<p class='session-unique' $display_unique>".__("Vous devez remplir tout le formulaire !")."</p>";
    echo "<p class='session-catalogue' $display_catalogue>".__("Si vous laissez ces champs vides, les informations seront récupérées depuis la fiche formation. Ne remplissez que les informations qui diffèrent de la version catalogue")."</p>";
    echo "</div>";
    
    echo "<h3>Présentation</h3>";
    $data = get_post_meta($post->ID, "presentation", true);
    wp_editor($data, "presentation", array_merge($tinymce_wpof_settings, array('textarea_rows' => 10)));

    echo "<h3>Objectifs</h3>";
    echo '<p>'.__("Obligatoire en formation professionnelle continue. Précisez ici les connaissances et capacités que doit acquérir le stagiaire pendant la formation. La formulation doit débuter par des verbes tels que « être capable de », « connaître », « maîtriser », etc.").'</p>';
    $data = get_post_meta($post->ID, "objectifs", true);
    wp_editor($data, "objectifs", array_merge($tinymce_wpof_settings, array('textarea_rows' => 5)));

    echo "<h3>Pré-requis</h3>";
    echo '<p>'.__('Obligatoire en formation professionnelle continue. Précisez ici les connaissances et capacités que doit posséder le stagiaire avant de suivre la formation. La formulation doit débuter par des verbes tels que « être capable de », « connaître », « maîtriser », etc.').'</p>';
    $data = get_post_meta($post->ID, "pre_requis", true);
    wp_editor($data, "pre_requis", array_merge($tinymce_wpof_settings, array('textarea_rows' => 5)));

    echo "<h3>Programme</h3>";
    $data = get_post_meta($post->ID, "programme", true);
    wp_editor($data, "programme", array_merge($tinymce_wpof_settings, array('textarea_rows' => 15)));

    $data = get_post_meta($post->ID, "public", true);
    echo '<label for="public"><h3>Public</h3></label>';
    echo '<input style="width: 100%;" id="public" name="public" type="text" value="'.$data.'" />';

    echo "<h3>Matériel pédagogique</h3>";
    $data = get_post_meta($post->ID, "materiel_pedagogique", true);
    wp_editor($data, "materiel_pedagogique", array_merge($tinymce_wpof_settings, array('textarea_rows' => 5)));
    
    endif;
}        

function session_quiz_meta_box($post)
{
    global $SessionFormation;
    if (!isset($SessionFormation[$post->ID]))
        $SessionFormation[$post->ID] = new SessionFormation($post->ID);
    $session_formation = $SessionFormation[$post->ID];
    
    if ($session_formation->type_index != 'sous_traitance') :
    
    global $tinymce_wpof_quiz_settings;
    
    $id_formation = get_post_meta($post->ID, "formation", true);
    $id_formation = ($id_formation == "") ? -1 : $id_formation;
    // pré-affichage en fonction de l'ID de la formation sélectionnée
    // si ID == -1, alors session unique, pas une formation du catalogue
    // attention, logique inversée, on n'insère que du display none !
    $display_unique = ($id_formation > 0) ? "style='display: none;'" : "";
    $display_catalogue = ($id_formation < 0) ? "style='display: none;'" : "";
    
    echo "<p class='session-unique' $display_unique>".__("Vous devez remplir tout le formulaire !")."</p>";
    echo "<p class='session-catalogue' $display_catalogue>".__("Ne remplissez que les informations qui diffèrent de la version catalogue")."</p>";
    
    _e("<p>L'évaluation des compétences doit avoir lieu avant la session pour les pré-requis <strong>et</strong> les objectifs. Elle doit permettre de :</p>
    <ul class='classic'>
<li>valider les pré-requis</li>
<li>proposer éventuellement un allégement si le stagiaire maîtrise déjà certaines compétences</li>
</ul>

<p>L'évaluation des compétences doit <strong>également</strong> avoir lieu après la formation pour vérifier que le stagiaire a bien acquis des compétences à la suite de la session.</p>

<p>Attention à la rédaction du quiz : tout paragraphe devient une question. Vous pouvez intercaler des titres (format Titre), insérer des images et des vidéos dans les paragraphes avec le texte de la question. Ne laissez pas de paragraphe vide ou avec une image seule !</p>

<p>Les questions portent sur des compétences à avoir ou acquérir. Par exemple « créer des dossiers et naviguer dans votre ordinateur ». Chaque compétence est évaluée de 1 à 5.</p>");

    echo "<h3>".__("Évaluation des pré-requis")."</h3>";
    if ($quizpr_id = get_post_meta($post->ID, "quizpr_id", true))
    {
        $quizpr = new Quiz($quizpr_id);
        $quizpr->init_questions($quizpr_id);
        $data = $quizpr->get_html_questions();
    }
    else
        $data = get_post_meta($post->ID, "quizpr", true);
    wp_editor($data, "quizpr", array_merge($tinymce_wpof_quiz_settings, array('textarea_rows' => 10)));
        
    echo "<h3>".__("Évaluation des objectifs")."</h3>";
    if ($quizobj_id = get_post_meta($post->ID, "quizobj_id", true))
    {
        $quizobj = new Quiz($quizobj_id);
        $quizobj->init_questions($quizobj_id);
        $data = $quizobj->get_html_questions();
    }
    else
        $data = get_post_meta($post->ID, "quizobj", true);
    wp_editor($data, "quizobj", array_merge($tinymce_wpof_quiz_settings, array('textarea_rows' => 15)));
    
    endif;
}

function session_ressources_meta_box($post)
{
    global $tinymce_wpof_settings;
    
    $data = get_post_meta($post->ID, "ressources", true);
    wp_editor($data, "ressources", array_merge($tinymce_wpof_settings, array('textarea_rows' => 10)));
}

// save meta box with update
//add_action('save_post','save_session_metaboxes');
function save_session_metaboxes($post_ID)
{
    global $wpof;
    
    if (get_post_type($post_ID) != "session") return;
    
    $champs = array
    (
        'numero',
        'formateur',
        'formation',
        'session_unique_titre',
        'presentation',
        'public',
        'objectifs',
        'pre_requis',
        'programme',
        'ressources',
        'lieu',
        'type_formation',
        'visibilite_session',
        'stagiaires_min',
        'stagiaires_max',
        'materiel_pedagogique',
        'commanditaire_nom',
        'commanditaire_contact',
        'commanditaire_contact_email',
        'commanditaire_num_of',
        'nb_heure_decimal',
        'st_nb_stagiaire',
    );
    foreach($champs as $c)
    {
        if(isset($_POST[$c]))
            update_post_meta($post_ID, $c, $_POST[$c]);
    }
    
    if (isset($_POST['lieu']) && $_POST['lieu'] < 1)
    {
        foreach(array("nom", "adresse", "code_postal", "ville", "localisation", "secu_erp") as $c)
        {
            if(isset($_POST["lieu_".$c]))
                update_post_meta($post_ID, "lieu_".$c, $_POST["lieu_".$c]);
        }
    }        
    
    // Choix du tarif
    $tarif_heure = 0;
    if (empty($_POST['tarif_total_chiffre']) && empty($_POST['tarif_heure']))
        $tarif_heure = $wpof->tarif_inter;
        
    if (!empty($_POST['st_tarif_total_chiffre']))
    {
        update_post_meta($post_ID, 'tarif_total_chiffre', $_POST['st_tarif_total_chiffre']);
        update_post_meta($post_ID, 'tarif_base_total', 1);
        delete_post_meta($post_ID, 'tarif_heure');
    }
    elseif (!empty($_POST['tarif_heure']) || $tarif_heure > 0)
    {
        update_post_meta($post_ID, 'tarif_heure', ($tarif_heure > 0) ? $tarif_heure : $_POST['tarif_heure']);
        update_post_meta($post_ID, 'tarif_base_total', 0);
        delete_post_meta($post_ID, 'tarif_total_chiffre');
    }
    elseif (!empty($_POST['tarif_total_chiffre']) || !empty($_POST['st_tarif_total_chiffre']))
    {
        update_post_meta($post_ID, 'tarif_total_chiffre', $_POST['tarif_total_chiffre']);
        update_post_meta($post_ID, 'tarif_base_total', 1);
        delete_post_meta($post_ID, 'tarif_heure');
    }
    
    // création du titre en parallèle pour mise à jour
    $partie_titre = array("", "", "");

    $formation_id = -1;
    if (isset($_POST['formation']) && $_POST['formation'] > 0)
    {
        $partie_titre[0] = get_the_title($_POST['formation']);
        $formation_id = $_POST['formation'];
/*        
        // si les quiz ne sont pas définis dans la session, on va les chercher dans la formation
        if (!isset($quizpr))
        {
        }
        if (!isset($quizobj))
        {
            $quizobj_id = get_post_meta($_POST['formation'], "quizobj_id", true);
            update_post_meta($post_ID, "quizobj_id", $quizobj_id);
        }*/
    }
    elseif (isset($_POST['session_unique_titre']))
    {
        $partie_titre[0] = $_POST['session_unique_titre'];
        if (isset($_POST['specialite']))
            update_post_meta($post_ID, 'specialite', $_POST['specialite']);
    }
    
    
    if (isset($_POST['lieu']) && $_POST['lieu'] > 0)
        $partie_titre[2] = get_post_meta($_POST['lieu'], "ville", true);
    else if (isset($_POST['lieu_ville']))
        $partie_titre[2] = $_POST['lieu_ville'];
    
    // récupération des quiz définis dans la session (s'ils existent)
    // ou assignation aux quiz de la formation si elle existe
    if (isset($_POST['quizpr']) && $_POST['quizpr'] != "")
    {
        $quizpr = new Quiz();
        $quizpr->set_identite("prerequis", $post_ID);
        $quizpr->init_questions();
        $quizpr->parse_text($_POST['quizpr']);
        update_post_meta($post_ID, "quizpr_id", $quizpr->quiz_id);
        update_post_meta($post_ID, "quizpr_parent_id", $post_ID);
    }
    elseif ($formation_id > 0)
    {
        $quizpr_id = get_post_meta($formation_id, "quizpr_id", true);
        update_post_meta($post_ID, "quizpr_id", $quizpr_id);
        update_post_meta($post_ID, "quizpr_parent_id", $formation_id);
    }
    if (isset($_POST['quizobj']) && $_POST['quizobj'] != "")
    {
        $quizobj = new Quiz();
        $quizobj->set_identite("objectifs", $post_ID);
        $quizobj->init_questions();
        $quizobj->parse_text($_POST['quizobj']);
        update_post_meta($post_ID, "quizobj_id", $quizobj->quiz_id);
        update_post_meta($post_ID, "quizobj_parent_id", $post_ID);
    }
    elseif ($formation_id > 0)
    {
        $quizobj_id = get_post_meta($formation_id, "quizobj_id", true);
        update_post_meta($post_ID, "quizobj_id", $quizobj_id);
        update_post_meta($post_ID, "quizobj_parent_id", $formation_id);
    }
    
    // champs qui nécessitent un traitement particulier
    // enregistrement des dates
    if (isset($_POST['dates']) && !empty($_POST['dates'][0]))
    {
        $creneaux = sort_dates($_POST['dates']);

        update_post_meta($post_ID, "first_date", $creneaux[0]);
        $first_date = date_create_from_format("d/m/Y", $creneaux[0]);
        if ($first_date)
        {
            $ts = $first_date->getTimestamp();
            update_post_meta($post_ID, "first_date_timestamp", $ts);
            $partie_titre[1] = date_i18n("j F Y", $ts);
        }
        $partie_titre[1] = pretty_print_dates($creneaux[0]);

        // on déplace les dates en tant que clés du tableau $creneaux
        $creneaux = array_fill_keys($creneaux, null);
        
        // enregistrement des créneaux
        if (isset($_POST['creno_id']))
        {
            //debug_info($_POST, "POST");
            foreach($_POST['creno_id'] as $id)
            {
                $data = array();
                foreach(array('date', 'heure_debut', 'heure_fin', 'type', 'module_id', 'activite_id', 'lieu_id', 'salle_id') as $d)
                {
                    if (isset($_POST[$id."_".$d]))
                        $data[$d] = $_POST[$id."_".$d];
                }
                
                // Protection contre les créneaux vides
                if ($data['date'] == "" || $data['heure_debut'] == "" || $data['heure_fin'] == "")
                    continue;
                $creno = new Creneau();
                $creno->init_from_form($data);
                $creno->session_id = $post_ID;
                if (strpos($id, "tmp") === false)
                    $creno->id = $id;
                $creno_id = $creno->update();
                if ($creno_id !== false)
                    $creneaux[$data['date']][] = $creno_id;
            }
        }
        
        // on enregistre les créneaux, même s'il n'y a que des dates sans créneaux
        update_post_meta($post_ID, "creneaux", $creneaux);
    }
    
    // $post_title = join(" - ", $partie_titre);
    $post_title = $partie_titre[0];
    if (!empty($partie_titre[1]))
        $post_title .= " – ".$partie_titre[1];
    if (!empty($partie_titre[2]))
        $post_title .= " – ".$partie_titre[2];

    // mise à jour du titre et du slug
    if ( ! wp_is_post_revision( $post_ID ) )
    {
        // unhook this function so it doesn't loop infinitely
        remove_action('save_post', 'save_session_metaboxes');
     
        $args = array
        (
            'ID' => $post_ID,
            'post_title' => $post_title,
            'post_name' => sanitize_title($post_title),
        );
        // update the post, which calls save_post again
        wp_update_post( $args );
 
        // re-hook this function
        add_action('save_post', 'save_session_metaboxes');
    }
}

add_action( 'wp_ajax_save_nouveau_lieu', 'save_nouveau_lieu' );
function save_nouveau_lieu()
{
    if (isset($_POST['nouveau-lieu-nom']))
    {
        $lieu_id = wp_insert_post(array('post_title' => $_POST['lieu_nom'], 'post_type' => 'lieu', 'post_status' => 'publish', 'post_author' => 1), true);
        if (isset($_POST['adresse'])) update_post_meta($lieu_id, 'adresse', $_POST['adresse']);
        if (isset($_POST['code_postal'])) update_post_meta($lieu_id, 'code_postal', $_POST['code_postal']);
        if (isset($_POST['ville'])) update_post_meta($lieu_id, 'ville', $_POST['ville']);
        if (isset($_POST['localisation'])) update_post_meta($lieu_id, 'localisation', $_POST['localisation']);
        if (isset($_POST['secu_erp'])) update_post_meta($lieu_id, 'secu_erp', $_POST['secu_erp']);
        
        echo json_encode(array('id' => $lieu_id, 'nom' => $_POST['lieu_nom']));
    }
    
    die();
}
