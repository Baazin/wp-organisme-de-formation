jQuery(document).ready(function($)
{
    var nepasmodifier = "Le titre est créé automatiquement !";
    $("body.post-type-session #title-prompt-text").text("");
    $("body.post-type-session input#title").attr('disabled','disabled');
    $("body.post-type-session #titlewrap").append('<p>' + nepasmodifier + '</p>');
    
    var month = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];

    $("span.valide-creneau").click(valide_creneau);
    function valide_creneau(e)
    {
        e.preventDefault();
        var bloc_details = $(this).parent().parent();
        creno = bloc_details.parent();
        id = bloc_details.children("input[name='creno_id[]']").val();
        var fields = new Object();
        
        // on parcourt les valeurs pour recréer l'objet et son étiquette
        bloc_details.children("label").children().each(function()
        {
            fields[$(this).attr('name').replace(id + "_", "")] = $(this).val();
        });
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'update_creneau',
                'data': fields,
            },
            function(response)
            {
                bloc_creno = $(response);
                bloc_creno.on('click', '.edit-creneau', edit_creneau);
                bloc_creno.on('click', '.del-creneau', function() { $(this).parent().remove(); } );
                bloc_creno.on('click', '.valide-creneau', valide_creneau);
                bloc_creno.on('click', '.annule-creneau', annule_creneau);
                creno.replaceWith(bloc_creno);
            },
        );
        bloc_details.hide();
        bloc_details.parent().children(".masque").hide();
    }
    
    $("div.masque").click(function(e)
    {
        parent_id = "#" + $(this).parent().attr("id");
        $(parent_id + " span.annule-creneau").trigger("click");
    });
    
    $("span.annule-creneau").click(annule_creneau);
    function annule_creneau(e)
    {
        e.preventDefault();
        bloc_details = $(this).parent().parent();
        bloc_details.children("label").children().each(function()
        {
            if (undefined != bloc_details.attr('data-' + $(this).attr('name')))
            {
                $(this).val(bloc_details.attr('data-' + $(this).attr('name')));
            }
        });
        bloc_details.hide();
        bloc_details.parent().children(".masque").hide();
    }
    
    
    $("#catalogue select").on("change", function()
    {
        titre = $("#title").val();
        var parties = titre.split(" - ");
        user_id = $("input[name='user_id']").val();
        
        if ($(this).val() == -1)
        {
            $(".session-unique").show();
            $(".session-catalogue").hide();
            parties[0] = $("#session_unique_titre input").val();
            $("button.add-content-formation").prop('disabled', true);
            $("select[name='formateur[]']").val(user_id);
        }
        else
        {
            $(".session-unique").hide();
            $(".session-catalogue").show();
            parties[0] = $("#catalogue select option:selected").text();
            $("button.add-content-formation").prop('disabled', false);
            jQuery.post
            ({
                url: ajaxurl,
                data: {
                    'action': 'get_formation_data',
                    'formation_id': $(this).val(),
                },
                success: function(response)
                {
                    formation = JSON.parse(response);
                    $("select[name='formateur[]']").val(formation.formateur);
                    $("input[name='tarif_heure']").val(formation.tarif);
                },
            });
        }
        
        $("#title").val(parties.join(" - "));
    });
    
    $("#session_unique_titre input").blur(function()
    {
        titre = $("#title").val();
        var parties = titre.split(" - ");
        parties[0] = $("#session_unique_titre input").val();
        $("#title").val(parties.join(" - "));
    });
    
    
    $("#lieu-existant select").on("change", function()
    {
        titre = $("#title").val();
        var parties = titre.split(" - ");
        
        if ($(this).val() == -1)
        {
            $("#nouveau-lieu").show();
            $("#add-nouveau-lieu").show();
            $("#lieu-data input").removeAttr("disabled");
            $("#lieu-data textarea").removeAttr("readonly");
            $("#adresse").val("");
            $("#code_postal").val("");
            $("#ville").val("");
            $("#localisation").val("");
            $("#secu_erp").val("");
            $("#erp-nom-image").attr("href", "#");
            $("#erp-nom-image").text("");
            $("#erp-add-image").removeClass("inactif");
            parties[2] = $("#ville").val();
            $("#title").val(parties.join(" - "));
        }
        else
        {
            $("#nouveau-lieu").hide();
            $("#add-nouveau-lieu").hide();
            $("#lieu-data input").attr("disabled", "disabled");
            $("#lieu-data textarea").attr("readonly", "readonly");
            
            jQuery.post
            (
                ajaxurl,
                {
                    'action': 'get_lieu',
                    'lieu_id': $(this).val(),
                },
                function(response)
                {
                    lieu = JSON.parse(response);
                    $("#adresse").val(lieu.adresse);
                    $("#code_postal").val(lieu.code_postal);
                    $("#ville").val(lieu.ville);
                    $("#localisation").val(lieu.localisation);
                    $("#secu_erp").val(lieu.secu_erp);
                    $("#erp-nom-image").attr("href", lieu.secu_erp_link);
                    $("#erp-nom-image").text(lieu.secu_erp_name);
                    $("#erp-add-image").addClass("inactif");
                    parties[2] = lieu.ville;
                    $("#title").val(parties.join(" - "));
                },
            );
            
        }
    });
    
    $("#ville").blur(function()
    {
        titre = $("#title").val();
        var parties = titre.split(" - ");
        parties[2] = $("#ville").val();
        $("#title").val(parties.join(" - "));
    });
    /*
    $("#erp-add-image").click(function(e)
    {
        e.preventDefault();
        
        if (!$(this).hasClass("inactif"))
        {
            var uploader = wp.media
            ({
                title: "Envoyer un fichier",
                button: { text: "Téléverser" },
                multiple: false,
            })
            .on('select', function()
            {
                var selection = uploader.state().get('selection');
                var attachment = selection.first().toJSON();
                console.log(attachment);
                $("#erp-nom-image").attr("href", attachment.link);
                $("#erp-nom-image").text(attachment.title);
                $("#secu_erp").val(attachment.id);
            })
            .open();
        }
    });
 */
    
    $("#add-nouveau-lieu").click(function(e)
    {
        e.preventDefault();
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'save_nouveau_lieu',
                'nouveau-lieu-nom': $("#nouveau-lieu input").val(),
                'adresse': $("#adresse").val(),
                'code_postal': $("#code_postal").val(),
                'ville': $("#ville").val(),
                'localisation': $("#localisation").val(),
                'secu_erp': $("#secu_erp").val(),
            },
            function(response)
            {
                lieu = JSON.parse(response);
                $("#lieu-existant select").append(new Option(lieu.nom, lieu.id, true, true));
                $("#lieu-data input").attr("disabled", "disabled");
                $("#lieu-data textarea").attr("readonly", "readonly");
                $("#nouveau-lieu").hide();
                $("#add-nouveau-lieu").hide();
                
                // réceptionner l'id, le rajouter dans la liste, le sélectionner et griser tous les champs
            }
        );
    });
    
    // Ajoute le contenu du champ depuis la fiche formation
    $("button.add-content-formation").click(function(e)
    {
        e.preventDefault();
        editor_id = $(this).attr("data-content");
        formation_id = $(this).attr("data-formationid");
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'add_content_formation',
                'formation_id': formation_id,
                'content': editor_id,
            },
            function(response)
            {
                if ($('#wp-'+editor_id+'-wrap').hasClass('tmce-active') && tinyMCE.get(editor_id))
                {
                    content = tinyMCE.get(editor_id).getContent() + response;
                    tinyMCE.get(editor_id).setContent(content);
                    if ($(this).closest(".metadata.notif-modif") != undefined)
                        $(this).closest(".metadata.notif-modif").find(".input_jpost").addClass("modif");
                }
                else
                {
                    content = $('#'+textarea_id).val() + response;
                    $('#'+textarea_id).val(content);
                }
            }
        );
    });
    
    $("button.new-line").click(function(e)
    {
        e.preventDefault();
        editor_id = $(this).attr("data-content");
        
        if ($('#wp-'+editor_id+'-wrap').hasClass('tmce-active') && tinyMCE.get(editor_id))
        {
            tinyMCE.get(editor_id).setContent(tinyMCE.get(editor_id).getContent() + "<div class='flex'><p>1</p><p>2</p></div>");
            //tinyMCE.get(editor_id).setContent(tinyMCE.get(editor_id).getContent() + "<table><tr><td>1</td><td>2</td></tr></table>");
        }
        else
        {
            $('#'+textarea_id).val($('#'+textarea_id).val() + "<div class='flex'></div>");
        }
    });
    
    // Ajoute le contenu du champ depuis la proposition faite par l'organisme de formation
    $("button.add-content-of").click(function(e)
    {
        e.preventDefault();
        editor_id = $(this).attr("data-content");
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'add_content_of',
                'content': editor_id,
            },
            function(response)
            {
                if ($('#wp-'+editor_id+'-wrap').hasClass('tmce-active') && tinyMCE.get(editor_id))
                {
                    content = tinyMCE.get(editor_id).getContent() + response;
                    tinyMCE.get(editor_id).setContent(content);
                    if ($(this).closest(".metadata.notif-modif") != undefined)
                        $(this).closest(".metadata.notif-modif").find(".input_jpost").addClass("modif");
                }
                else
                {
                    content = $('#'+textarea_id).val() + response;
                    $('#'+textarea_id).val(content);
                }
            }
        );
    });
    
    // Efface le champ
    $("button.del-content").click(function(e)
    {
        e.preventDefault();
        editor_id = $(this).attr("data-content");
        
        if ($('#wp-'+editor_id+'-wrap').hasClass('tmce-active') && tinyMCE.get(editor_id))
        {
            tinyMCE.get(editor_id).setContent("");
            if ($(this).closest(".metadata.notif-modif") != undefined)
                $(this).closest(".metadata.notif-modif").find(".input_jpost").addClass("modif");
        }
        else
        {
            $('#'+textarea_id).val("");
        }
    });
    
    // TODO : à virer
    $("button.save-content").click(function(e)
    {
        e.preventDefault();
        local_parent = $(this).closest(".input_jpost");
        editor_id = $(this).attr("data-content");
        valid_icon = local_parent.find(".valid");
        display_valeur = local_parent.find(".valeur");
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'update_jpost_value',
                'object_class': $(this).attr('data-class'),
                'session_id': $(".id.session").attr('data-id'),
                'meta': editor_id,
                'value': tinyMCE.get(editor_id).getContent(),
                'nodeName': 'editor',
            },
            function(response)
            {
                param = JSON.parse(response);
                valid_icon.show().delay(1000).fadeOut(1000);
                display_valeur.html(param.valeur);
                local_parent.find(".save-content").hide();
            }
        );
    });
        
    
    
    $("#session-formation input[name='type_formation']").change(function(e)
    {
        e.preventDefault();
        if (!$("#session-formation input[name='visibilite_session']").prop("checked"))
        {
            if ($("#session-formation input[name='type_formation']").filter("[value='inter']").prop("checked"))
                $("#session-formation input[name='visibilite_session']").filter("[value='public']").prop("checked", true);
            else
                $("#session-formation input[name='visibilite_session']").filter("[value='invite']").prop("checked", true);
        }
        if ($("#session-formation input[name='type_formation']").filter("[value='sous_traitance']").prop("checked"))
        {
            $("#session-data").hide();
            $("#session-quiz").hide();
            $("#session-lieu").hide();
            $("#session-divers").hide();
            $(".postbox .flex-container").addClass("sous_traitance");
        }
        else
        {
            $("#session-data").show();
            $("#session-quiz").show();
            $("#session-lieu").show();
            $("#session-divers").show();
            $(".postbox .flex-container").removeClass("sous_traitance");
        }
    });
    
    // Efface la valeur de l'input dont l'id est passé par data-input
    // Supprime la propriété disabled de tous les input voisons
    $(".reset-input").click(function(e)
    {
        e.preventDefault();
        $("#" + $(this).attr("data-input")).val("");
        $(this).parent().children("input").prop("disabled", false);
    });
    
    // Désactive et efface la valeur de tous les inputs voisins, sauf celui qui est modifié
    $("#tarif_bloc input").change(function(e)
    {
        e.preventDefault();
        $(this).parent().children("input[name!='" + $(this).attr("name") + "']").prop("disabled", true);
        $(this).parent().children("input[name!='" + $(this).attr("name") + "']").val("");
    });
    
});
